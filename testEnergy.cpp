#include "Mesh/Mesh.h"
#include "Mesh/Energy.h"

USE_PRJ_NAMESPACE

int main()
{
  {
    boost::shared_ptr<Mesh> mesh(new Mesh(Mesh::createSheet(Vec2i(10,20),0.1f)));
    SpringEnergy spring(mesh,10);
    spring.debugGradient(0.01f);
    LinearSpringEnergy linearSpring(mesh,10);
    linearSpring.debugGradient(0.01f);
    StretchEnergy stretch(mesh,10,0.33f);
    stretch.debugGradient(0.01f);
    BendEnergy bend(mesh,10);
    bend.debugGradient(0.01f);
    KinematicEnergy kinematic(mesh,10);
    kinematic.debugGradient(0.01f);
    ConstantForceEnergy force(mesh,Vec3d::Random(),10);
    force.debugGradient(0.01f);
    SoftPointConstraintEnergy point(mesh,RandEngine::randI(0,mesh->nrVertex()-1),Vec3d::Random(),10);
    point.debugGradient(0.01f);
    SoftNormalConstraintEnergy normal(mesh,RandEngine::randI(0,mesh->nrTriangle()-1),Vec3d::Random(),10);
    normal.debugGradient(0.01f);
  }
  {
    boost::shared_ptr<Mesh> mesh(new MeshRibbon(0.1,0.05,10));
    BendRibbonEnergy bendRibbon(mesh,10);
    bendRibbon.debugGradient(0.01f);
    KinematicRibbonEnergy kinematicRibbon(mesh,10);
    kinematicRibbon.debugGradient(0.01f);
    ConstantForceRibbonEnergy forceRibbon(mesh,Vec3d::Random(),10);
    forceRibbon.debugGradient(0.01f);
    SoftPointConstraintEnergy point(mesh,RandEngine::randI(0,mesh->nrVertex()-1),Vec3d::Random(),10);
    point.debugGradient(0.01f);
    SoftNormalConstraintEnergy normal(mesh,RandEngine::randI(0,mesh->nrTriangle()-1),Vec3d::Random(),10);
    normal.debugGradient(0.01f);
  }
  return 0;
}
