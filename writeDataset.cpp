#include "Dataset/Dataset.h"
#include "Optimizer/Utils.h"

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
  ASSERT_MSG(argc==2,"Usage: writeDataset path")
  std::string path=argv[1];
  boost::shared_ptr<Mesh> mesh(new MeshRibbon(0.1f,0.05f,100));
  boost::shared_ptr<Simulator> sim(new Simulator(mesh,Simulator::NRZ_QP));
  sim->initRibbon(1/600.0f,1,Vec3d(0,-9.81f,0),M_PI/4,0.05f);
  boost::shared_ptr<Dataset> ds(new Dataset(sim,4));
  if(exists("Dataset.dat")) {
    ds->SerializableBase::read("Dataset.dat");
    if(endsWith(path,".txt"))
      ds->writePython(path);
    else ds->writeVTK(path);
  } else {
    INFO("Dataset.dat does not exist!")
  }
  return 0;
}
