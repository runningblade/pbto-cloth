#include "DebugObjective.h"
#include "SparseUtils.h"

USE_PRJ_NAMESPACE

//Quadratic
Quadratic::Quadratic(scalarD NX,scalarD NF)
{
  _f.setRandom(NF);
  _J.setRandom(NF,NX);
  _g.setRandom(NX);
  _H.setRandom(NX,NX);
  _H=(_H.transpose()*_H).eval();
}
int Quadratic::operator()(const Vec& x,Vec& fvec,Matd* fjac,bool modifiable)
{
  fvec=_f+_J*x;
  if(fjac)
    *fjac=_J;
  return 0;
}
scalarD Quadratic::operator()(const Vec& x,Vec* fgrad,Matd* fhess)
{
  if(fgrad)
    *fgrad=_H*x+_g;
  if(fhess)
    *fhess=_H;
  return x.dot(_H*x/2+_g);
}
int Quadratic::operator()(const Vec& x,Vec& fvec,SMat* fjac,bool modifiable)
{
  fvec=_f+_J*x;
  if(fjac)
    *fjac=toSparse<scalarD,Matd>(_J);
  return 0;
}
scalarD Quadratic::operator()(const Vec& x,Vec* fgrad,SMat* fhess)
{
  if(fgrad)
    *fgrad=_H*x+_g;
  if(fhess)
    *fhess=toSparse<scalarD,Matd>(_H);
  return x.dot(_H*x/2+_g);
}
int Quadratic::values() const
{
  return _J.rows();
}
int Quadratic::inputs() const
{
  return _J.cols();
}
void Quadratic::setNull(sizeType id)
{
  _H.row(id).setZero();
  _H.col(id).setZero();
  _g[id]=0;
}
//MassSpring
template <typename PARENT>
MassSpring<PARENT>::MassSpring(sizeType np,sizeType dim,scalarD l,sizeType nc,scalarD lc)
  :_soft(true),_np(np),_dim(dim),_l(l)
{
  for(sizeType x=0; x<np; x+=np-1)
    for(sizeType y=0; y<np; y+=np-1) {
      if((sizeType)_css.size() >= nc)
        break;
      Cold v=Cold::Zero(_dim);
      v[0]=(scalarD)x*l*lc;
      v[1]=(scalarD)y*l*lc;
      _css.push_back(v);
      _pss.push_back(GI(x,y));
    }
  if(dim == 3 && nc == 5) {
    Cold v=Cold::Zero(_dim);
    v[0]=(scalarD)(np/2)*l*lc;
    v[1]=(scalarD)(np/2)*l*lc;
    v[2]+=(np/2)*l*lc;
    _css.push_back(v);
    _pss.push_back(GI(np/2,np/2));
  }
}
template <typename PARENT>
Cold MassSpring<PARENT>::initX() const
{
  Cold x=Cold::Zero(inputs());
  for(sizeType r=0; r<_np; r++)
    for(sizeType c=0; c<_np; c++)
      x.segment<2>(GI(r,c)*_dim)=Vec2i(r,c).cast<scalarD>()*_l;
  return x;
}
template <typename PARENT>
void MassSpring<PARENT>::writeVTK(const Vec& x,const std::string& path) const
{
  std::vector<Vec3d,Eigen::aligned_allocator<Vec3d> > vss;
  std::vector<Vec3i,Eigen::aligned_allocator<Vec3i> > iss;
  for(sizeType c=0; c<_np; c++)
    for(sizeType r=0; r<_np; r++) {
      Vec3d v=Vec3d::Zero();
      for(sizeType d=0; d<_dim; d++)
        v[d]=x[GI(r,c)*_dim+d];
      vss.push_back(v);
      if(r<_np-1)
        iss.push_back(Vec3i(GI(r,c),GI(r+1,c),0));
      if(c<_np-1)
        iss.push_back(Vec3i(GI(r,c),GI(r,c+1),0));
    }
  VTKWriter<scalarD> os("grid",path,true);
  os.appendPoints(vss.begin(),vss.end());
  os.appendCells(iss.begin(),iss.end(),VTKWriter<scalarD>::LINE);
}
template <typename PARENT>
int MassSpring<PARENT>::operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable)
{
  fvec.setZero(values());
  //spring
  sizeType off=0;
  for(sizeType r=0; r<_np-1; r++)
    for(sizeType c=0; c<_np; c++) {
      Cold dir=x.segment(GI(r,c)*_dim,_dim)-x.segment(GI(r+1,c)*_dim,_dim);
      if(fjac)addBlock(*fjac,off,GI(r,c)*_dim,dir.transpose()/std::max<scalarD>(dir.norm(),1E-6f));
      if(fjac)addBlock(*fjac,off,GI(r+1,c)*_dim,-dir.transpose()/std::max<scalarD>(dir.norm(),1E-6f));
      fvec[off]=dir.norm()-_l;
      off++;
    }
  for(sizeType r=0; r<_np; r++)
    for(sizeType c=0; c<_np-1; c++) {
      Cold dir=x.segment(GI(r,c)*_dim,_dim)-x.segment(GI(r,c+1)*_dim,_dim);
      if(fjac)addBlock(*fjac,off,GI(r,c)*_dim,dir.transpose()/std::max<scalarD>(dir.norm(),1E-6f));
      if(fjac)addBlock(*fjac,off,GI(r,c+1)*_dim,-dir.transpose()/std::max<scalarD>(dir.norm(),1E-6f));
      fvec[off]=dir.norm()-_l;
      off++;
    }
  //fixing
  if(_soft)
    for(sizeType i=0; i<(sizeType)_css.size(); i++) {
      fvec.segment(off,_dim)=(x.segment(_pss[i]*_dim,_dim)-_css[i]);
      if(fjac)
        addBlockId<scalarD>(*fjac,off,_pss[i]*_dim,_dim,1);
      off+=_dim;
    }
  return 0;
}
template <typename PARENT>
sizeType MassSpring<PARENT>::GI(sizeType r,sizeType c) const
{
  return r+c*_np;
}
template <typename PARENT>
int MassSpring<PARENT>::values() const
{
  int nrC=_soft?(int)_css.size():0;
  return (_np-1)*_np*2+nrC*_dim;
}
template <typename PARENT>
int MassSpring<PARENT>::inputs() const
{
  return _np*_np*_dim;
}
template <typename PARENT>
void MassSpring<PARENT>::setSoft(bool soft)
{
  _soft=soft;
}
template <typename PARENT>
void MassSpring<PARENT>::setBound(LMInterface& lm)
{
  lm.setCB(Cold::Constant(inputs(),_l),Cold::Constant(inputs(),_l*_np));
}
template <typename PARENT>
void MassSpring<PARENT>::setSparse(LMInterface& lm,bool sparse,bool oneSide)
{
  SMat smat;
  STrips trips;
  Cold CI0=Cold::Zero(_pss.size()*_dim);
  Coli type=Coli::Zero(_pss.size()*_dim);
  smat.resize(_pss.size()*_dim,inputs());
  for(sizeType i=0; i<(sizeType)_pss.size(); i++)
    for(sizeType d=0; d<_dim; d++) {
      trips.push_back(STrip(i*_dim+d,_pss[i]*_dim+d,1));
      CI0[i*_dim+d]=_css[i][d];
    }
  smat.setFromTriplets(trips.begin(),trips.end());
  if(oneSide)
    type.segment(type.size()-_dim,_dim).setConstant(-1);
  if(sparse)
    lm.setCI(smat,CI0,type);
  else lm.setCI(smat.toDense(),CI0,type);
}
template <typename PARENT>
sizeType MassSpring<PARENT>::np() const
{
  return _np;
}
//MassSpringDynamics
MassSpringDynamics::MassSpringDynamics(sizeType np,sizeType dim,scalarD l,sizeType nc,scalarD lc)
  :MassSpring(np,dim,l,nc,lc),_dt(0.01f)
{
  initDynamics();
}
scalarD MassSpringDynamics::operator()(const Vec& x,Vec* fgrad,SMat* fhess)
{
  //gravity
  Cold g=Cold::Zero(x.size());
  for(sizeType i=0; i<inputs(); i+=_dim)
    g[i+_dim-1]=-9.81f;
  //build system
  scalarD coef=1/(_dt*_dt);
  if(fgrad)
    *fgrad=(x-2*_x+_xN)*coef-g;
  if(fhess) {
    fhess->resize(x.size(),x.size());
    for(sizeType i=0; i<x.size(); i++)
      fhess->coeffRef(i,i)=coef;
  }
  return (x-2*_x+_xN).squaredNorm()*coef/2-x.dot(g);
}
void MassSpringDynamics::updateDynamics(const Vec& x)
{
  _xN=_x;
  _x=x;
}
void MassSpringDynamics::initDynamics()
{
  _xN=_x=initX();
}
//MassSpringDynamics
MassSpringDynamicsWithBall::MassSpringDynamicsWithBall(sizeType np,sizeType dim,scalarD l,sizeType nc,scalarD lc)
  :MassSpringDynamics(np,dim,l,nc,lc)
{
  _rad=2.5;
  _ctr=Vec3d(5,5,-5);
}
void MassSpringDynamicsWithBall::evaluateConstraint(const Cold& x,sizeType& c,ConstraintInfo& cInfo,bool fjac)
{
  Vec3d dir=x.segment<3>(c*3)-_ctr;
  cInfo._value=dir.norm()-_rad;
  if(!fjac)
    return;
  cInfo._gradDense.setZero(x.size());
  cInfo._gradDense.segment<3>(c*3)=dir/std::max<scalarD>(dir.norm(),EPS);
  cInfo._gradSparse.resize(x.size());
  addBlock(cInfo._gradSparse,c*3,cInfo._gradDense.segment<3>(c*3));
}
void MassSpringDynamicsWithBall::getNewConstraint(const Cold& x,std::vector<sizeType>& newCons)
{
  if(_dim!=3)
    return;
  for(sizeType r=0; r<_np; r++)
    for(sizeType c=0; c<_np; c++)
      if((x.segment<3>(GI(r,c)*3)-_ctr).norm()<_rad)
        newCons.push_back(GI(r,c));
}
//instances
template class MassSpring<Objective<scalarD>>;
