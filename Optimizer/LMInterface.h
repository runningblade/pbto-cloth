#ifndef LM_INTERFACE_H
#define LM_INTERFACE_H

#include "EigenInterface.h"
#include <CommonFile/solvers/Objective.h>
#include <CommonFile/solvers/Callback.h>

PRJ_BEGIN

class QPInterface;
class LMInterface
{
public:
  typedef Objective<scalarD>::SMat SMat;
  typedef Eigen::DiagonalMatrix<scalarD,-1,-1> DMat;
  LMInterface();
  void setTolG(scalarD tolg);
  void setTolF(scalarD tolf);
  void setTolX(scalarD tolx);
  void setMaxIter(sizeType maxIter);
  void setMinMu(scalarD minMu);
  void clearCI();
  void setCI(const SMat& CI,const Cold& CI0,const Coli& type);
  void setCI(const Matd& CI,const Cold& CI0,const Coli& type);
  void setCB(const Cold& L,const Cold& U);
  void useCache(bool useCache);
  void useWoodbury(bool useWoodbury);
  void useInterface(boost::shared_ptr<LinearSolverInterface> sol);
  void setKrylov(scalar krylov);
  bool solveSparse(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb);
  bool solveDense(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb);
  bool solve(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb);
  void writeMuHistory(const std::string& path) const;
  //debug quadratic LM example
  void debugLMExampleQuadratic();
  //debug general nonlinear LM example
  void debugLMExampleNonlinear(bool soft,bool sparse,bool oneSide,bool bound=false);
  //debug constraints
  static void debugConstraint(bool sparse,int type,scalarD range);
  Cold solveSys(const Cold& p,const SMat& LHS,const Cold& RHS,scalarD mu,const SMat* J,bool sameA,bool& succ) const;
  Cold solveSys(const Cold& p,const Matd& LHS,const Cold& RHS,scalarD mu,const Matd* J,bool sameA,bool& succ) const;
  Cold solveSysQP(const Cold& p,const SMat& LHS,const Cold& RHS,scalarD mu,const SMat* J,bool sameA,bool& succ) const;
  Cold solveSysQP(const Cold& p,const Matd& LHS,const Cold& RHS,scalarD mu,const Matd* J,bool sameA,bool& succ) const;
  template <typename MAT>
  bool solveInternal(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb);
  void setTauMax(scalarD tauMax);
  void setTau(scalarD tau);
  scalarD tau() const;
  scalarD tauMax() const;
  sizeType nrIter() const;
protected:
  template <typename MAT>
  static typename MAT::Scalar maxDiagElem(const MAT& A,const MAT& J);
  //solver parameter
  sizeType _maxIter;
  scalarD _tau,_tauMax,_krylov;
  scalarD _tolg,_tolf,_tolx,_minMu;
  bool _useCache;
  bool _useWoodbury;
  //data
  boost::shared_ptr<QPInterface> _cons;
  boost::shared_ptr<LinearSolverInterface> _sol;
  //transient data
  std::vector<scalarD> _muHistory;
  sizeType _iter;
};

PRJ_END

#endif
