#include "BLEICInterface.h"
#include <alglib/optimization.h>

USE_PRJ_NAMESPACE

//helper
Cold fromAlglib(const alglib::real_1d_array& xAlglib)
{
  return Eigen::Map<const Eigen::VectorXd>(xAlglib.getcontent(),xAlglib.length()).cast<scalarD>();
}
void toAlglib(alglib::real_1d_array& ret,const Cold& x)
{
  if(ret.length() != x.size())
    ret.setlength(x.size());
  for(sizeType i=0; i<ret.length(); i++)
    ret[i]=x[i];
}
alglib::real_1d_array toAlglib(const Cold& x)
{
  alglib::real_1d_array ret;
  toAlglib(ret,x);
  return ret;
}
void toAlglib(alglib::real_2d_array& ret,const Matd& x)
{
  if(ret.rows() != x.rows() || ret.cols() != x.cols())
    ret.setlength(x.rows(),x.cols());
  //Matd xt=x.transpose();
  for(sizeType i=0; i<ret.rows(); i++)
    for(sizeType j=0; j<ret.cols(); j++)
      ret(i,j)=x(i,j);
}
alglib::real_2d_array toAlglib(const Matd& x)
{
  alglib::real_2d_array ret;
  toAlglib(ret,x);
  return ret;
}
void alglibRep(const alglib::real_1d_array& x,double func,void* ptr)
{
  BLEICInterface* bleic=(BLEICInterface*)ptr;
  Eigen::Map<const Eigen::VectorXd> xMap(x.getcontent(),x.length());
  bleic->_cb->operator()(xMap.cast<scalarD>(),xMap.cast<scalarD>(),func,xMap.norm(),0,0,0,0);
}
template <typename MAT>
void alglibGrad(const alglib::real_1d_array& x,double& func,alglib::real_1d_array& grad,void* ptr)
{
  BLEICInterface* bleic=(BLEICInterface*)ptr;
  Eigen::Map<const Eigen::VectorXd> xMap(x.getcontent(),x.length());
  Eigen::Map<Eigen::VectorXd> gradMap(grad.getcontent(),x.length());
  Objective<scalarD>& obj=*(bleic->_obj);

  scalarD funcTmp;
  MAT fjacTmp;
  Cold gradTmp=Cold::Zero(x.length()),fvecTmp;
  obj(xMap.cast<scalarD>(),fvecTmp,&fjacTmp,true);
  funcTmp=obj(xMap.cast<scalarD>(),&gradTmp,(MAT*)NULL);
  funcTmp+=fvecTmp.squaredNorm()/2;
  gradTmp+=fjacTmp.transpose()*fvecTmp;
  alglibRep(x,func,ptr);

  gradMap=gradTmp.cast<double>();
  func=funcTmp;
}
void alglibGradLBFGS(const alglib::real_1d_array& x,double& func,alglib::real_1d_array& grad,void* ptr)
{
  BLEICInterface* bleic=(BLEICInterface*)ptr;
  Eigen::Map<const Eigen::VectorXd> xMap(x.getcontent(),x.length());
  Eigen::Map<Eigen::VectorXd> gradMap(grad.getcontent(),x.length());
  Objective<scalarD>& obj=*(bleic->_obj);

  scalarD funcTmp;
  Cold gradTmp=Cold::Zero(x.length());
  obj(xMap.cast<scalarD>(),funcTmp,gradTmp,1,true);

  gradMap=gradTmp.cast<double>();
  func=funcTmp;
}
//BLEICInterface
BLEICInterface::BLEICInterface()
{
  _tolg=1E-9f;
  _tolf=1E-9f;
  _tolx=1E-9f;
  _maxIter=1E8;
}
void BLEICInterface::setTolG(scalarD tolg)
{
  _tolg=tolg;
}
void BLEICInterface::setTolF(scalarD tolf)
{
  _tolf=tolf;
}
void BLEICInterface::setTolX(scalarD tolx)
{
  _tolx=tolx;
}
void BLEICInterface::setMaxIter(sizeType maxIter)
{
  _maxIter=maxIter;
}
void BLEICInterface::setCI(const SMat& CI,const Cold& CI0,const Coli& type)
{
  setCI(CI.toDense(),CI0,type);
}
void BLEICInterface::setCI(const Matd& CI,const Cold& CI0,const Coli& type)
{
  _CI=CI;
  _CI0=CI0;
  _type=type;
}
void BLEICInterface::setCB(const Cold& L,const Cold& U)
{
  _L=L;
  _U=U;
}
template <typename MAT>
bool BLEICInterface::solveTpl(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb,bool lbfgs)
{
  //create
  alglib::minbleicstate state;
  alglib::minbleicreport rep;
  alglib::real_1d_array xAlglib=toAlglib(p);
  alglib::minbleiccreate(xAlglib,state);
  alglib::minbleicsetcond(state,_tolg,_tolf,_tolx,_maxIter);

  //bound constraint
  if(_L.size()==xAlglib.length() && _U.size()==xAlglib.length())
    alglib::minbleicsetbc(state,toAlglib(_L),toAlglib(_U));

  //linear constraint
  if(_CI.rows()==_CI0.size() && _CI.rows()==_type.size() && _CI.cols()==xAlglib.length()) {
    Matd CII0=Matd::Zero(_CI.rows(),_CI.cols()+1);
    CII0.block(0,0,_CI.rows(),_CI.cols())=_CI;
    CII0.col(_CI.cols())=-_CI0;

    alglib::integer_1d_array typeAlglib;
    typeAlglib.setcontent(_type.size(),_type.cast<alglib::ae_int_t>().eval().data());
    alglib::minbleicsetlc(state,toAlglib(CII0),typeAlglib);
  }

  //solve
  _obj=&obj;
  _cb=&cb;
  alglib::minbleicrestartfrom(state,xAlglib);
  if(lbfgs)
    alglib::minbleicoptimize(state,alglibGradLBFGS,alglibRep,this);
  else alglib::minbleicoptimize(state,alglibGrad<MAT>,alglibRep,this);
  alglib::minbleicresults(state,xAlglib,rep);
  p=fromAlglib(xAlglib);
  _ttype=rep.terminationtype;
  _iter=rep.iterationscount;
  _f=state.f;
  _obj=NULL;
  _cb=NULL;
  //INFOV("Termination type: %ld",_rep->terminationtype)
  return rep.terminationtype <= 7 && rep.terminationtype >= 1;
}
bool BLEICInterface::solveSparse(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb)
{
  return solveTpl<Objective<scalarD>::SMat>(p,obj,cb,false);
}
bool BLEICInterface::solveDense(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb)
{
  return solveTpl<Objective<scalarD>::DMat>(p,obj,cb,false);
}
bool BLEICInterface::solve(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb)
{
  return solveTpl<SMat>(p,obj,cb,true);
}
sizeType BLEICInterface::getTermination() const
{
  return _ttype;
}
scalarD BLEICInterface::getFunctionValue() const
{
  return _f;
}
sizeType BLEICInterface::nrIter() const
{
  return _iter;
}
