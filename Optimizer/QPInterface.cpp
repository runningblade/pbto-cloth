#include "QPInterface.h"
#include "SparseUtils.h"
#include "Utils.h"
#include <CommonFile/IO.h>
#ifdef EPS
#undef EPS
#endif
#include <qpOASES.hpp>

USE_PRJ_NAMESPACE

//QPInterface
void QPInterface::setCI(const SMat& CI,const Cold& CI0,const Coli& type) {
  _CIS.reset(new SMat(CI));
  _CID=NULL;
  _CIL.reset(new Cold(-CI0));
  _CIU.reset(new Cold(-CI0));
  for(sizeType i=0; i<type.size(); i++)
    if(type[i]>0)
      _CIU->coeffRef(i)=2e20;
    else if(type[i]<0)
      _CIL->coeffRef(i)=-2e20;
  _CIES=NULL;
  _CIED=NULL;
  _CI0=NULL;
  _invCICIT=NULL;
}
void QPInterface::setCI(const Matd& CI,const Cold& CI0,const Coli& type) {
  _CIS=NULL;
  _CID.reset(new Matd(CI));
  _CIL.reset(new Cold(-CI0));
  _CIU.reset(new Cold(-CI0));
  for(sizeType i=0; i<type.size(); i++)
    if(type[i]>0)
      _CIU->coeffRef(i)=2e20;
    else if(type[i]<0)
      _CIL->coeffRef(i)=-2e20;
  _CIES=NULL;
  _CIED=NULL;
  _CI0=NULL;
  _invCICIT=NULL;
}
void QPInterface::setCB(const Cold& L,const Cold& U) {
  _L.reset(new Cold(L));
  _U.reset(new Cold(U));
  _CIES=NULL;
  _CIED=NULL;
  _CI0=NULL;
  _invCICIT=NULL;
}
sizeType QPInterface::nrEquality() const {
  sizeType ret=0;
  if(_CIS||_CID)
    for(sizeType i=0; i<_CIL->rows(); i++)
      ret+=_CIL->coeff(i)==_CIU->coeff(i)?1:0;
  if(_L&&_U)
    for(sizeType i=0; i<_L->rows(); i++)
      ret+=_L->coeff(i)==_U->coeff(i)?1:0;
  return ret;
}
sizeType QPInterface::nrInequality() const {
  sizeType ret=(_CIL?_CIL->size():0)+(_L?_L->size():0);
  return ret-nrEquality();
}
sizeType QPInterface::nrGeneral() const {
  return _CIS?_CIS->rows():_CID?_CID->rows():0;
}
sizeType QPInterface::vars() const {
  return (_L&&_U)?_L->size():_CIS?_CIS->cols():_CID?_CID->cols():0;
}
sizeType QPInterface::cons() const {
  return _CIS?_CIS->rows():_CID?_CID->rows():0;
}
const Cold& QPInterface::L() const {
  return *_L;
}
const Cold& QPInterface::U() const {
  return *_U;
}
const QPInterface::SMat* QPInterface::CIS() const {
  if(!_CIES) {
    std::set<sizeType> IG,IE;
    if(_CIS||_CID)
      for(sizeType i=0; i<_CIL->rows(); i++)
        if(_CIL->coeff(i)==_CIU->coeff(i))
          IG.insert(i);
    if(_L||_U)
      for(sizeType i=0; i<_L->rows(); i++)
        if(_L->coeff(i)==_U->coeff(i))
          IE.insert(i);
    const_cast<boost::shared_ptr<SMat>&>(_CIES).reset(new SMat(createCIS(IG,IE)));
  }
  return _CIES.get();
}
const Matd* QPInterface::CID() const {
  if(!_CIED) {
    std::set<sizeType> IG,IE;
    if(_CIS||_CID)
      for(sizeType i=0; i<_CIL->rows(); i++)
        if(_CIL->coeff(i)==_CIU->coeff(i))
          IG.insert(i);
    if(_L||_U)
      for(sizeType i=0; i<_L->rows(); i++)
        if(_L->coeff(i)==_U->coeff(i))
          IE.insert(i);
    const_cast<boost::shared_ptr<Matd>&>(_CIED).reset(new Matd(createCID(IG,IE)));
  }
  return _CIED.get();
}
Cold QPInterface::enforceCons(const Cold& p) {
  if(nrInequality()>0) {
    bool succ=false;
    Cold ret=solveSysQP(p,NULL,NULL,-p,succ);
    ASSERT_MSG(succ,"Cannot enforceCons!")
    return ret;
  } else {
    prepareEnforceCons(p,NULL);
    Cold C=_CIES?Cold(*_CIES*p-*_CI0):Cold(*_CIED*p-*_CI0),invCICITC=_invCICIT->solve(C);
    return _CIES?Cold(p-_CIES->transpose()*invCICITC):Cold(p-_CIED->transpose()*invCICITC);
  }
}
Cold QPInterface::enforceConsZero(const Cold& p,const Cold& g) {
  if(nrInequality()>0)
    _invCICIT=NULL;
  prepareEnforceCons(p,&g);
  Cold C=_CIES?Cold(*_CIES*g):Cold(*_CIED*g);
  Cold invCICITC=C.size() == 0 ? Cold::Zero(0) : _invCICIT->solve(C);
  return _CIES?Cold(g-_CIES->transpose()*invCICITC):Cold(g-_CIED->transpose()*invCICITC);
}
Cold QPInterface::solveSysQP(const Cold& p,SMat* HS,Matd* HD,const Cold& G,bool& succ) const {
  using namespace qpOASES;
  Eigen::Matrix<real_t,-1,-1,Eigen::RowMajor> CIDTmp,HDTmp;
  Eigen::Matrix<real_t,-1,1> GQP=G.cast<real_t>(),x,CILQP,CIUQP,LQP,UQP,valH,valC;
  Eigen::Matrix<sparse_int_t,-1,1> rH,rC,cH,cC;
  qpOASES::SparseMatrix CISQP;
  DenseMatrix CIDQP;
  SymSparseMat HSQP;
  SymDenseMat HDQP;
  x.resize(p.size());
  if(_L)
    LQP=_L->cast<real_t>();
  if(_U)
    UQP=_U->cast<real_t>();
  qpOASES::SymmetricMatrix* HQP=NULL;
  if(HS) {
    rH=Eigen::Map<const Eigen::Matrix<SMat::StorageIndex,-1,1> >(HS->innerIndexPtr(),HS->nonZeros()).cast<sparse_int_t>();
    cH=Eigen::Map<const Eigen::Matrix<SMat::StorageIndex,-1,1> >(HS->outerIndexPtr(),HS->outerSize()+1).cast<sparse_int_t>();
    valH=Eigen::Map<const Eigen::Matrix<SMat::Scalar,-1,1> >(HS->valuePtr(),HS->nonZeros()).cast<real_t>();
    HSQP=SymSparseMat(HS->rows(),HS->cols(),rH.data(),cH.data(),valH.data());
    HSQP.createDiagInfo();
    HQP=&HSQP;
  } else if(HD) {
    HDTmp=HD->cast<real_t>();
    HDQP=SymDenseMat(HDTmp.rows(),HDTmp.cols(),HDTmp.cols(),HDTmp.data());
    HQP=&HDQP;
  }
  int_t nWSR=1E4;
  QProblem prob(vars(),cons(),(!HS&&!HD)?HST_IDENTITY:HST_UNKNOWN);
  prob.setPrintLevel(PrintLevel::PL_LOW);
  if(_CIS) {
    rC=Eigen::Map<const Eigen::Matrix<SMat::StorageIndex,-1,1> >(_CIS->innerIndexPtr(),_CIS->nonZeros()).cast<sparse_int_t>();
    cC=Eigen::Map<const Eigen::Matrix<SMat::StorageIndex,-1,1> >(_CIS->outerIndexPtr(),_CIS->outerSize()+1).cast<sparse_int_t>();
    valC=Eigen::Map<const Eigen::Matrix<SMat::Scalar,-1,1> >(_CIS->valuePtr(),_CIS->nonZeros()).cast<real_t>();
    CISQP=qpOASES::SparseMatrix(_CIS->rows(),_CIS->cols(),rC.data(),cC.data(),valC.data());
    CILQP=_CIL->cast<real_t>();
    CIUQP=_CIU->cast<real_t>();
    prob.init(HQP,GQP.data(),&CISQP,_L?LQP.data():NULL,_U?UQP.data():NULL,CILQP.data(),CIUQP.data(),nWSR);
  } else if(_CID) {
    CIDTmp=_CID->cast<real_t>();
    CIDQP=DenseMatrix(CIDTmp.rows(),CIDTmp.cols(),CIDTmp.cols(),CIDTmp.data());
    CILQP=_CIL->cast<real_t>();
    CIUQP=_CIU->cast<real_t>();
    prob.init(HQP,GQP.data(),&CIDQP,_L?LQP.data():NULL,_U?UQP.data():NULL,CILQP.data(),CIUQP.data(),nWSR);
  } else prob.init(HQP,GQP.data(),NULL,_L?LQP.data():NULL,_U?UQP.data():NULL,NULL,NULL,nWSR);
  succ=prob.getPrimalSolution(x.data())==qpOASES::SUCCESSFUL_RETURN;
  return x.template cast<scalarD>();
}
void QPInterface::prepareEnforceCons(const Cold& p,const Cold* g) {
  if(!_invCICIT) {
    std::vector<scalarD> RHS;
    std::set<sizeType> IG,IE;
    if(_CIS||_CID) {
      Cold pc=_CIS?Cold(*_CIS*p):Cold(*_CID*p);
      Cold pcg=g?(_CIS?Cold(*_CIS**g):Cold(*_CID**g)):Cold();
      for(sizeType i=0; i<_CIL->rows(); i++)
        if(_CIL->coeff(i)==_CIU->coeff(i)) {
          IG.insert(i);
          RHS.push_back(_CIL->coeff(i));
        } else if(pc[i]<=_CIL->coeff(i)&&(!g||pcg[i]>=0)) {
          IG.insert(i);
          RHS.push_back(_CIL->coeff(i));
        } else if(pc[i]>=_CIU->coeff(i)&&(!g||pcg[i]<=0)) {
          IG.insert(i);
          RHS.push_back(_CIU->coeff(i));
        }
    }
    if(_L||_U)
      for(sizeType i=0; i<_L->rows(); i++)
        if(_L->coeff(i)==_U->coeff(i)) {
          IE.insert(i);
          RHS.push_back(_L->coeff(i));
        } else if(p[i]<=_L->coeff(i)&&(!g||(*g)[i]>=0)) {
          IE.insert(i);
          RHS.push_back(_L->coeff(i));
        } else if(p[i]>=_U->coeff(i)&&(!g||(*g)[i]<=0)) {
          IE.insert(i);
          RHS.push_back(_U->coeff(i));
        }
    Eigen::Map<const Cold> CI0(&RHS[0],RHS.size());
    if(_CIS) {
      _CIES.reset(new SMat(createCIS(IG,IE)));
      if(!_invCICIT)
        _invCICIT=LinearSolverTraits<SMat>::getCholSolver();
      if(_CIES->rows()>0) {
        ASSERT_MSG(_invCICIT->recomputeAAT(*_CIES,0,false),"Cannot enforce constraint, factorize(AAT) failed!");
      }
      if(!_CI0)
        _CI0.reset(new Cold(CI0));
    } else {
      _CIED.reset(new Matd(createCID(IG,IE)));
      if(!_invCICIT)
        _invCICIT=LinearSolverTraits<Matd>::getCholSolver();
      if(_CIED->rows()>0) {
        ASSERT_MSG(_invCICIT->recomputeAAT(*_CIED,0,false),"Cannot enforce constraint, factorize(AAT) failed!");
      }
      if(!_CI0)
        _CI0.reset(new Cold(CI0));
    }
  }
}
QPInterface::SMat QPInterface::createCIS(const std::set<sizeType>& IG,const std::set<sizeType>& IE) const {
  SMat unit;
  STrips trips;
  sizeType off=0;
  if(_CIS||_CID)
    for(sizeType i=0; i<_CIL->rows(); i++)
      if(IG.find(i)!=IG.end()) {
        if(_CIS) {
          unit.resize(_CIS->rows(),1);
          unit.coeffRef(i,0)=1;
          addBlock(trips,off++,0,SMat(unit.transpose()**_CIS));
        } else addBlock(trips,off++,0,_CID->row(i));
      }
  if(_L||_U)
    for(sizeType i=0; i<_L->rows(); i++)
      if(IE.find(i)!=IE.end())
        trips.push_back(STrip(off++,i));
  SMat CIES;
  CIES.resize(IG.size()+IE.size(),vars());
  CIES.setFromTriplets(trips.begin(),trips.end());
  return CIES;
}
Matd QPInterface::createCID(const std::set<sizeType>& IG,const std::set<sizeType>& IE) const {
  std::vector<Cold,Eigen::aligned_allocator<Cold> > rows;
  if(_CIS||_CID)
    for(sizeType i=0; i<_CIL->rows(); i++)
      if(IG.find(i)!=IG.end()) {
        if(_CIS)
          rows.push_back(_CIS->transpose()*Cold::Unit(_CIS->rows(),i));
        else rows.push_back(_CID->row(i));
      }
  if(_L||_U)
    for(sizeType i=0; i<_L->rows(); i++)
      if(IE.find(i)!=IE.end())
        rows.push_back(Cold::Unit(_L->size(),i));
  Matd CIED;
  CIED.resize(IG.size()+IE.size(),vars());
  for(sizeType i=0; i<(sizeType)rows.size(); i++)
    CIED.row(i)=rows[i].transpose();
  return CIED;
}
