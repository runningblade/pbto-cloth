#ifndef AUGMENTED_LAGRANGIAN_H
#define AUGMENTED_LAGRANGIAN_H

#include "LMInterface.h"
#include "SparseUtils.h"
#include "BLEICInterface.h"

PRJ_BEGIN

//Augmented Lagrangian Solver
template <typename CONSTRAINT=sizeType,typename BACKEND=LMInterface>
class AugmentedLagrangian : public Objective<scalarD>
{
public:
  typedef Eigen::SparseVector<scalarD,0,sizeType> SCold;
  struct ConstraintInfo
  {
    ConstraintInfo():_value(0),_lambda(0),_isNew(true) {}
    scalarD _value;
    Cold _gradDense;
    SCold _gradSparse;
    scalarD _lambda;
    bool _isNew;
  };
  class AdaptiveConstraint
  {
  public:
    virtual void evaluateConstraint(const Cold& x,CONSTRAINT& c,ConstraintInfo& cInfo,bool fjac) {
      FUNCTION_NOT_IMPLEMENTED
    }
    virtual void getNewConstraint(const Cold& x,std::vector<CONSTRAINT>& newCons) {}
    virtual void getConstraint(const Cold& x,std::map<CONSTRAINT,ConstraintInfo>& cons,bool fjac,bool addNew) {
      //initially set all already-seen constraint to be old
      for(typename std::map<CONSTRAINT,ConstraintInfo>::iterator beg=cons.begin(),end=cons.end(); beg!=end; beg++)
        beg->second._isNew=false;
      //generate new constraints incrementally
      if(addNew) {
        std::vector<CONSTRAINT> newCons;
        getNewConstraint(x,newCons);
        //if any constraint is also contained in newConstraint list, tag is as new again
        for(sizeType i=0; i<(sizeType)newCons.size(); i++) {
          typename std::map<CONSTRAINT,ConstraintInfo>::iterator it=cons.find(newCons[i]);
          if(it!=cons.end()) {
            ConstraintInfo info=it->second;
            info._isNew=true;
            cons.erase(newCons[i]);
            cons[newCons[i]]=info;
          } else {
            cons[newCons[i]]=ConstraintInfo();
          }
        }
      }
      //evaluate
      _tmp.clear();
      for(typename std::map<CONSTRAINT,ConstraintInfo>::iterator beg=cons.begin(),end=cons.end(); beg!=end; beg++) {
        CONSTRAINT c=beg->first;
        evaluateConstraint(x,c,beg->second,fjac);
        _tmp[c]=beg->second;
      }
      _tmp.swap(cons);
    }
  protected:
    std::map<CONSTRAINT,ConstraintInfo> _tmp;
  };
public:
  AugmentedLagrangian()
  {
    setInitMu(1000);
    setMaxIter(1E6);
    setTauMax(1E6);
    setThresVio(0);
    setTolG(1E-5f);
    setTolX(0);
    setTolF(0);
  }
  void setTolG(scalarD tolg)
  {
    _subProblemSolver.setTolG(tolg);
  }
  void setTolF(scalarD tolf)
  {
    _subProblemSolver.setTolF(tolf);
  }
  void setTolX(scalarD tolx)
  {
    _subProblemSolver.setTolX(tolx);
  }
  void setThresVio(scalarD thres) {
    _thresVio=thres;
  }
  void setMaxIter(sizeType maxIter)
  {
    _maxIter=maxIter;
  }
  void setMinMu(scalarD minMu)
  {
    _subProblemSolver.setMinMu(minMu);
  }
  void setInitMu(scalarD initMu)
  {
    _mu=initMu;
  }
  void setTauMax(scalarD tauMax) {
    _subProblemSolver.setTauMax(tauMax);
  }
  void setKrylov(scalarD kry) {
    _subProblemSolver.setKrylov(kry);
  }
  void clearCI()
  {
    _subProblemSolver.clearCI();
  }
  void setCI(const SMat& CI,const Cold& CI0,const Coli& type)
  {
    _subProblemSolver.setCI(CI,CI0,type);
  }
  void setCI(const Matd& CI,const Cold& CI0,const Coli& type)
  {
    _subProblemSolver.setCI(CI,CI0,type);
  }
  void setCB(const Cold& L,const Cold& U)
  {
    _subProblemSolver.setCB(L,U);
  }
  void useInterface(boost::shared_ptr<LinearSolverInterface> sol)
  {
    _subProblemSolver.useInterface(sol);
  }
  template <typename MAT,bool gradientBased>
  bool solveTpl(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb,const Colc* sign=NULL)
  {
    Cold gvec;
    _obj=&obj;
    sizeType iter=0;
    _lambda.setZero(obj.values());
    if(sign && sign->size()==obj.values())
      _sign=*sign;
    else _sign.setZero(obj.values());
    _cons.clear();
    bool report=dynamic_cast<NoCallback<scalarD,Kernel<scalarD> >*>(&cb) == NULL;
    while(iter<_maxIter) {
      //solve subproblem
      _subProblemSolver.setMaxIter(std::min<sizeType>(_maxIter-iter,std::sqrt(_maxIter)));
      if(typeid(MAT)==typeid(DMat) && !gradientBased)
        _subProblemSolver.solveDense(p,*this,cb);
      else if(typeid(MAT)==typeid(SMat) && !gradientBased)
        _subProblemSolver.solveSparse(p,*this,cb);
      else if(typeid(MAT)==typeid(SMat) && gradientBased)
        _subProblemSolver.solve(p,*this,cb);
      else {
        ASSERT_MSG(false,"Unknown MAT type!")
      }
      //return true;
      iter+=_subProblemSolver.nrIter();
      //test result
      if(_subProblemSolver.nrIter()==0)
        return true;
      //update multiplier
      updateMultiplier<MAT>(p,false);
      //constraint violation, also add new constraint
      getConstraintViolation<MAT>(p,gvec,true);
      scalarD maxVio=gvec.cwiseAbs().maxCoeff();
      if(report) {
        INFOV("maxVio: %f!",maxVio)
      }
      if(maxVio<_thresVio)
        return true;
    }
    return false;
  }
  bool solveSparse(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb,const Colc* sign=NULL)
  {
    return solveTpl<SMat,false>(p,obj,cb,sign);
  }
  bool solveDense(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb,const Colc* sign=NULL)
  {
    return solveTpl<DMat,false>(p,obj,cb,sign);
  }
  bool solve(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb,const Colc* sign=NULL)
  {
    return solveTpl<SMat,true>(p,obj,cb,sign);
  }
  //dynamic constraints
  void getConstraint(const Cold& x,bool fjac,bool addNew)
  {
    if(dynamic_cast<AdaptiveConstraint*>(_obj))
      dynamic_cast<AdaptiveConstraint*>(_obj)->getConstraint(x,_cons,fjac,addNew);
  }
  template <typename MAT>
  void getConstraintViolation(const Cold& x,Cold& gvec,bool addNew) {
    bool clamped=false;
    //non-adaptive constraint
    _obj->operator()(x,gvec,(MAT*)NULL,true);
    for(sizeType i=0; i<gvec.size(); i++)
      gvec[i]=clampCons(gvec[i],_sign[i],clamped);
    //adaptive constraint
    _values.clear();
    getConstraint(x,false,addNew);
    for(typename std::map<CONSTRAINT,ConstraintInfo>::iterator beg=_cons.begin(),end=_cons.end(); beg!=end; beg++)
      _values.push_back(clampCons(beg->second._value,1,clamped));
    //concat
    gvec=concat(gvec,Eigen::Map<Cold>(&_values[0],(sizeType)_values.size()));
  }
  //for L-BFGS Optimization
  virtual int operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient) {
    Vec fvec;
    SMat fjac;
    _obj->operator()(x,FX,DFDX,step,wantGradient);
    operatorTpl<SMat>(x,fvec,&fjac,true);
    FX+=fvec.squaredNorm()/2;
    DFDX+=fjac.transpose()*fvec;
    return 0;
  }
  //for LM Optimization
  template <typename MAT>
  int operatorTpl(const Vec& x,Vec& fvec,MAT* fjac,bool modifiableOrSameX) {
    //non-adaptive constraint
    _obj->operator()(x,fvec,fjac,modifiableOrSameX);
    bool clamped=false;
    Cold clampeds=Cold::Zero(fvec.size());
    ASSERT(fvec.size()==_lambda.size())
    for(sizeType i=0; i<fvec.size(); i++) {
      fvec[i]=clampCons(fvec[i]+_lambda[i]/_mu,_sign[i],clamped);
      if(!clamped) {
        fvec[i]*=std::sqrt(_mu);
        clampeds[i]=std::sqrt(_mu);
      }
    }
    if(fjac)
      *fjac=clampeds.asDiagonal()**fjac;
    //adaptive constraint
    _values.clear();
    _gradDenses.clear();
    _gradSparses.clear();
    getConstraint(x,fjac!=NULL,false);
    for(typename std::map<CONSTRAINT,ConstraintInfo>::iterator beg=_cons.begin(),end=_cons.end(); beg!=end; beg++) {
      scalarD value=clampCons(beg->second._value+beg->second._lambda/_mu,1,clamped);
      if(!clamped) {
        _values.push_back(value*std::sqrt(_mu));
        if(fjac!=NULL) {
          if(typeid(MAT)==typeid(DMat))
            _gradDenses.push_back(beg->second._gradDense*std::sqrt(_mu));
          else _gradSparses.push_back(beg->second._gradSparse*std::sqrt(_mu));
        }
      }
    }
    //insert fvec
    fvec=concat(fvec,Eigen::Map<Cold>(&_values[0],_values.size()));
    return 0;
  }
  virtual int operator()(const Vec& x,Vec& fvec,DMat* fjac,bool modifiableOrSameX) override {
    operatorTpl<DMat>(x,fvec,fjac,modifiableOrSameX);
    if(fjac) {
      DMat fjacA=DMat::Zero((sizeType)_values.size(),inputs());
      for(sizeType i=0; i<(sizeType)_values.size(); i++)
        fjacA.row(i)=_gradDenses[i].transpose();
      *fjac=concatRow(*fjac,fjacA);
    }
    return 0;
  }
  virtual int operator()(const Vec& x,Vec& fvec,SMat* fjac,bool modifiableOrSameX) override {
    operatorTpl<SMat>(x,fvec,fjac,modifiableOrSameX);
    if(fjac) {
      _tmp.clear();
      addBlock(_tmp,0,0,*fjac);
      for(sizeType i=0; i<(sizeType)_gradSparses.size(); i++)
        for(SCold::InnerIterator beg(_gradSparses[i]); beg; ++beg)
          _tmp.push_back(STrip(fjac->rows()+i,beg.index(),beg.value()));
      SMat fjacAll;
      fjacAll.resize(fjac->rows()+_values.size(),inputs());
      fjacAll.setFromTriplets(_tmp.begin(),_tmp.end());
      *fjac=fjacAll;
    }
    return 0;
  }
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiableOrSameX) override {
    FUNCTION_NOT_IMPLEMENTED
    return -1;
  }
  //for SQP Optimization
  virtual scalarD operator()(const Vec& x,Vec* fgrad,DMat* fhess) override {
    return _obj->operator()(x,fgrad,fhess);
  }
  virtual scalarD operator()(const Vec& x,Vec* fgrad,SMat* fhess) override {
    return _obj->operator()(x,fgrad,fhess);
  }
  virtual scalarD operator()(const Vec& x,Vec* fgrad,STrips* fhess) override {
    return _obj->operator()(x,fgrad,fhess);
  }
  //Dimension Info
  int inputs() const override {
    return _obj->inputs();
  }
  int values() const override {
    return _obj->values();
  }
protected:
  scalarD clampCons(scalarD x,char sign,bool& clamped) {
    if(sign>0) {
      if(x>0) {
        x=0;
        clamped=true;
      } else clamped=false;
    } else if(sign<0) {
      if(x<0) {
        x=0;
        clamped=true;
      } else clamped=false;
    } else clamped=false;
    return x;
  }
  template <typename MAT>
  void updateMultiplier(const Cold& x,bool addNew) {
    Cold fvec;
    bool clamped;
    //non-adaptive constraints
    _obj->operator()(x,fvec,(MAT*)NULL,true);
    ASSERT(fvec.size()==_lambda.size())
    for(sizeType i=0; i<fvec.size(); i++)
      _lambda[i]=clampCons(_lambda[i]+_mu*fvec[i],_sign[i],clamped);
    //adaptive constraints
    getConstraint(x,false,addNew);
    for(typename std::map<CONSTRAINT,ConstraintInfo>::iterator beg=_cons.begin(),end=_cons.end(); beg!=end; beg++)
      beg->second._lambda=clampCons(beg->second._lambda+_mu*beg->second._value,1,clamped);
  }
  //data
  BACKEND _subProblemSolver;
  sizeType _maxIter;
  scalarD _mu,_thresVio;
  Cold _lambda;
  Colc _sign;
  //temporary data
  std::vector<Cold,Eigen::aligned_allocator<Cold> > _gradDenses;
  std::vector<SCold,Eigen::aligned_allocator<SCold> > _gradSparses;
  std::vector<scalarD> _values;
  std::map<CONSTRAINT,ConstraintInfo> _cons;
  Objective<scalarD>* _obj;
};

PRJ_END

#endif
