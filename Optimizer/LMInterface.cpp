#include "LMInterface.h"
#include "QPInterface.h"
#include "DebugGradient.h"
#include "DebugObjective.h"
#include "SparseUtils.h"
#include "Utils.h"
#include <CommonFile/solvers/QPSolver.h>
#include <CommonFile/solvers/PMinresQLP.h>
#include <CommonFile/IO.h>
#ifdef EPS
#undef EPS
#endif

PRJ_BEGIN

//helper
template <typename MAT_TYPE>
class LMKrylov : public KrylovMatrix<scalarD>
{
public:
  typedef LMInterface::SMat SMat;
  LMKrylov(const MAT_TYPE& A,const MAT_TYPE* J,const MAT_TYPE* CI,scalarD mu)
    :_A(A),_J(J),_CI(CI),_mu(mu) {}
  virtual void multiply(const Vec& b,Vec& out) const {
    sizeType nP=_A.rows(),nD=b.size()-nP;
    Cold bP=b.segment(0,nP),bD=b.segment(nP,nD);

    out.setZero(n());
    out.segment(0,nP)=_A*bP+_mu*bP;
    if(_J)
      out.segment(0,nP)+=_J->transpose()*(*_J*bP);
    if(_CI) {
      out.segment(0,nP)+=_CI->transpose()*bD;
      out.segment(nP,nD)=*_CI*bP;
    }
  }
  virtual sizeType n() const {
    return _A.rows()+(_CI?_CI->rows():0);
  }
  const MAT_TYPE& _A;
  const MAT_TYPE* _J;
  const MAT_TYPE* _CI;
  scalarD _mu;
  Cold _tmp;
};
template <typename MAT>
MAT woodbury(LinearSolverInterface& invA,const Matd& J,const Matd& invAJT,const MAT& b)
{
  Matd I_JInvAJT=Matd::Identity(J.rows(),J.rows())+J*invAJT;
  return invA.solve(b)-invAJT*I_JInvAJT.ldlt().solve(invAJT.transpose()*b);
}
//LMInterface
LMInterface::LMInterface()
  :_maxIter(1E8),_tau(1),_tauMax(1000),_krylov(0),
   _tolg(1E-9f),_tolf(1E-9f),_tolx(1E-9f),_minMu(1E-3f),
   _useCache(false),_useWoodbury(false) {}
void LMInterface::setTolG(scalarD tolg)
{
  _tolg=tolg;
}
void LMInterface::setTolF(scalarD tolf)
{
  _tolf=tolf;
}
void LMInterface::setTolX(scalarD tolx)
{
  _tolx=tolx;
}
void LMInterface::setMaxIter(sizeType maxIter)
{
  _maxIter=maxIter;
}
void LMInterface::setMinMu(scalarD minMu)
{
  ASSERT_MSG(minMu > 0 && minMu < 1,"MinMu must be in range (0,1)")
  _minMu=minMu;
}
void LMInterface::clearCI()
{
  _cons=NULL;
}
void LMInterface::setCI(const SMat& CI,const Cold& CI0,const Coli& type)
{
  if(!_cons)
    _cons.reset(new QPInterface);
  _cons->setCI(CI,CI0,type);
}
void LMInterface::setCI(const Matd& CI,const Cold& CI0,const Coli& type)
{
  if(!_cons)
    _cons.reset(new QPInterface);
  _cons->setCI(CI,CI0,type);
}
void LMInterface::setCB(const Cold& L,const Cold& U)
{
  if(!_cons)
    _cons.reset(new QPInterface);
  _cons->setCB(L,U);
}
void LMInterface::useCache(bool useCache)
{
  _useCache=useCache;
}
void LMInterface::useWoodbury(bool useWoodbury)
{
  _useWoodbury=useWoodbury;
}
void LMInterface::useInterface(boost::shared_ptr<LinearSolverInterface> sol)
{
  _sol=sol;
  if(!_sol)
    _sol.reset(new EigenCholInterface);
}
void LMInterface::setKrylov(scalar krylov)
{
  _krylov=krylov;
}
bool LMInterface::solveSparse(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb)
{
  if(!_sol)
    _sol=LinearSolverTraits<SMat>::getCholSolver();
  return solveInternal<SMat>(p,obj,cb);
}
bool LMInterface::solveDense(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb)
{
  if(!_sol)
    _sol=LinearSolverTraits<Matd>::getCholSolver();
  return solveInternal<Matd>(p,obj,cb);
}
bool LMInterface::solve(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb)
{
  ASSERT_MSG(false,"LMInterface does not support gradient-based optimization!")
  return false;
}
void LMInterface::writeMuHistory(const std::string& path) const
{
  boost::filesystem::ofstream os(path);
  os << "Iter mu" << std::endl;
  for(sizeType i=0; i<(sizeType)_muHistory.size(); i++)
    os << (i+1) << " " << _muHistory[i] << std::endl;
}
//debug quadratic LM example
void LMInterface::debugLMExampleQuadratic()
{
#define N 20
  Quadratic quad(N,0);
  Cold x=Cold::Random(N);
  Callback<scalarD,Kernel<scalarD> > cb;
  solveDense(x,quad,cb);
#undef N
}
//debug general nonlinear LM example
void LMInterface::debugLMExampleNonlinear(bool soft,bool sparse,bool oneSide,bool bound)
{
  DEFINE_NUMERIC_DELTA
  Matd fjac;
  MassSpring<Objective<scalarD>> obj(10,3,0.5f,5,2);
  Cold x=Cold::Random(obj.inputs());
  Cold delta=Cold::Random(obj.inputs());
  Cold fvec,fvec2;

  std::string type=std::string(soft?"Soft":"Hard")+std::string(sparse?"Sparse":"Dense")+std::string(oneSide?"OneSide":"TwoSide")+std::string(bound?"Bound":"");
  obj.setSoft(soft);
  if(soft)
    clearCI();
  else obj.setSparse(*this,sparse,oneSide);
  if(bound)
    obj.setBound(*this);

  obj.Objective<scalarD>::operator()(x,fvec,&fjac,false);
  obj.Objective<scalarD>::operator()(x+delta*DELTA,fvec2,(Matd*)NULL,false);
  DEBUG_GRADIENT("FJac",(fjac*delta).norm(),(fjac*delta-(fvec2-fvec)/DELTA).norm())
  Callback<scalarD,Kernel<scalarD> > cb;
  create("massSpring");

  _sol=NULL;
  solveDense(x=Cold::Zero(obj.inputs()),obj,cb);
  obj.writeVTK(x,"massSpring/massSpringDense"+type+".vtk");

  _sol=NULL;
  useWoodbury(false);
  solveSparse(x=Cold::Zero(obj.inputs()),obj,cb);
  obj.writeVTK(x,"massSpring/massSpringSparse"+type+".vtk");

  _sol=NULL;
  useWoodbury(true);
  solveSparse(x=Cold::Zero(obj.inputs()),obj,cb);
  obj.writeVTK(x,"massSpring/massSpringSparseWoodbury"+type+".vtk");
}
//debug constraints
Cold proj(const Cold& x,int type)
{
  if(type == 0)
    return x;
  else if(type>0)
    return x.cwiseMin(Cold::Zero(x.size()));
  else return x.cwiseMax(Cold::Zero(x.size()));
}
scalarD proj(const Cold& p,const Cold& x,int type,scalarD range)
{
  if(range<=0)
    return proj(x,type).cwiseAbs().sum();
  else return proj(x,type).cwiseAbs().sum()+
              (p-Cold::Constant(p.size(),range)).cwiseMax(Cold::Zero(p.size())).cwiseAbs().sum()+
              (p+Cold::Constant(p.size(),range)).cwiseMin(Cold::Zero(p.size())).cwiseAbs().sum();
}
void LMInterface::debugConstraint(bool sparse,int type,scalarD range)
{
#define M 10
#define N 100
  bool succ;
  SMat CIS;
  Matd CI=Matd::Random(M,N);
  Cold CI0=Cold::Random(M);
  Cold p=Cold::Random(N),pStar;
  //SMat
  CIS.resize(M,N);
  for(sizeType r=0; r<M; r++)
    for(sizeType c=0; c<N; c++)
      CIS.coeffRef(r,c)=CI(r,c);
  //debug enforce cons
  LMInterface lm;
  if(sparse) {
    lm.setCI(CIS,CI0,Coli::Constant(M,type));
    lm._sol=LinearSolverTraits<SMat>::getCholSolver();
  } else {
    lm.setCI(CI,CI0,Coli::Constant(M,type));
    lm._sol=LinearSolverTraits<Matd>::getCholSolver();
  }
  if(range>0)
    lm.setCB(Cold::Constant(N,-range),Cold::Constant(N,range));
  pStar=lm._cons->enforceCons(p);
  INFOV("CI: %f Err: %f",(CI*p+CI0).norm(),proj(pStar,CI*pStar+CI0,type,range))
  //solve system: dense
  Cold RHS=Cold::Random(N);
  Matd LHS=Matd::Random(N,N);
  LHS=(LHS.transpose()*LHS).eval();
  lm._sol=LinearSolverTraits<Matd>::getCholSolver();
  Cold DENSE=lm.solveSys(pStar,LHS,RHS,1E-3f,NULL,false,succ);
  ASSERT_MSG(succ,"Solver failed!")
  INFOV("Dense Norm: %f Cons: %f",DENSE.norm(),proj(DENSE+pStar,CI*(DENSE+pStar)+CI0,type,range))
  //solve system: sparse
  SMat LHSS;
  LHSS.resize(N,N);
  for(sizeType r=0; r<N; r++)
    for(sizeType c=0; c<N; c++)
      LHSS.coeffRef(r,c)=LHS(r,c);
  lm._sol=LinearSolverTraits<SMat>::getCholSolver();
  Cold SPARSE=lm.solveSys(pStar,LHSS,RHS,1E-3f,NULL,false,succ);
  ASSERT_MSG(succ,"Solver failed!")
  INFOV("Sparse Norm: %f Cons: %f",SPARSE.norm(),proj(SPARSE+pStar,CI*(SPARSE+pStar)+CI0,type,range))
#undef N
#undef M
}
Cold LMInterface::solveSys(const Cold& p,const SMat& A,const Cold& RHS,scalarD mu,const SMat* J,bool sameA,bool& succ) const
{
  if(_cons && _cons->nrInequality() > 0)
    return solveSysQP(p,A,RHS,mu,J,sameA,succ);

  if(_krylov > 0) {
    Cold x,RHSAug=RHS;
    PMINRESSolverQLP<scalarD> sol;
    sol.setSolverParameters(_krylov,1E4);
    sol.setKrylovMatrix(boost::shared_ptr<KrylovMatrix<scalarD> >(new LMKrylov<SMat>(A,J,_cons?_cons->CIS():NULL,mu)));
    if(_cons)
      RHSAug=concat(RHS,Cold::Zero(_cons->nrEquality()));
    x.setZero(RHSAug.size());
    //sol.setCallback(boost::shared_ptr<Callback<scalarD,Kernel<scalarD> > >(new Callback<scalarD,Kernel<scalarD> > ));
    succ=sol.solve(RHSAug,x) == PMINRESSolverQLP<scalarD>::SUCCESSFUL;
    return x.segment(0,RHS.size());
  }

  succ=true;
  Cold invARHS;
  Matd invAB,invAJT,JD;
  SMat LHS=A;
  if(J && !_useWoodbury)
    LHS+=J->transpose()**J;
  succ=_sol->recompute(LHS,mu,sameA);
  if(!succ)
    return Cold();
  if(!_cons) {
    if(_useWoodbury) {
      JD=J->toDense();
      invAJT=_sol->solve(Matd(JD.transpose()));
      return woodbury<Cold>(*_sol,JD,invAJT,RHS);
    } else return _sol->solve(RHS);
  } else {
    if(_useWoodbury) {
      JD=J->toDense();
      invAJT=_sol->solve(Matd(JD.transpose()));
      invAB=woodbury<Matd>(*_sol,JD,invAJT,_cons->CID()->transpose());
      invARHS=woodbury<Cold>(*_sol,JD,invAJT,RHS);
    } else {
      invAB=_sol->solve(Matd(_cons->CIS()->transpose()));
      invARHS=_sol->solve(RHS);
    }
    return invARHS-invAB*(*(_cons->CIS())*invAB).eval().ldlt().solve(Matd(invAB.transpose()*RHS));
  }
}
Cold LMInterface::solveSys(const Cold& p,const Matd& A,const Cold& RHS,scalarD mu,const Matd* J,bool sameA,bool& succ) const
{
  if(_cons && _cons->nrInequality() > 0)
    return solveSysQP(p,A,RHS,mu,J,sameA,succ);

  succ=true;
  Matd LHS=A;
  if(J)
    LHS+=J->transpose()**J;
  if(!_cons) {
    return (LHS+Matd::Identity(LHS.rows(),LHS.cols())*mu).llt().solve(RHS);
  } else {
    sizeType nr=LHS.rows(),nrA=_cons->CID()->rows();
    Matd LHSC=Matd::Zero(nr+nrA,nr+nrA);
    LHSC.block(0,0,nr,nr)=LHS+Matd::Identity(LHS.rows(),LHS.cols())*mu;
    LHSC.block(nr,0,nrA,nr)=*(_cons->CID());
    LHSC.block(0,nr,nr,nrA)=_cons->CID()->transpose();
    return LHSC.fullPivLu().solve(concat(RHS,Cold::Zero(nrA))).segment(0,nr);
  }
}
Cold LMInterface::solveSysQP(const Cold& p,const SMat& A,const Cold& RHS,scalarD mu,const SMat* J,bool sameA,bool& succ) const
{
  if(_krylov > 0 && _cons->nrGeneral() == 0) {
    MPRGPQPSolver<scalarD,Kernel<scalarD> > sol;
    Cold LL=_cons->L()-p,UL=_cons->U()-p;
    sol.reset(LL,UL);
    sol.setSolverParameters(_krylov,1E4);
    sol.setKrylovMatrix(boost::shared_ptr<KrylovMatrix<scalarD,Kernel<scalarD> > >(new LMKrylov<SMat>(A,J,NULL,mu)));

    Cold x=Cold::Zero(A.rows());
    succ=sol.solve(RHS,x) == MPRGPQPSolver<scalarD,Kernel<scalarD> >::SUCCESSFUL;
    return x;
  } else {
    SMat HS=A;
    if(J)
      HS+=J->transpose()**J;
    for(sizeType i=0; i<A.rows(); i++)
      HS.coeffRef(i,i)+=mu;
    HS.makeCompressed();
    return _cons->solveSysQP(p,&HS,NULL,-RHS-HS*p,succ)-p;
  }
}
Cold LMInterface::solveSysQP(const Cold& p,const Matd& A,const Cold& RHS,scalarD mu,const Matd* J,bool sameA,bool& succ) const
{
  if(_krylov > 0 && _cons->nrGeneral() == 0) {
    MPRGPQPSolver<scalarD,Kernel<scalarD> > sol;
    Cold LL=_cons->L()-p,UL=_cons->U()-p;
    sol.reset(LL,UL);
    sol.setSolverParameters(_krylov,1E4);
    sol.setKrylovMatrix(boost::shared_ptr<KrylovMatrix<scalarD,Kernel<scalarD> > >(new LMKrylov<Matd>(A,J,NULL,mu)));

    Cold x=Cold::Zero(A.rows());
    sol.solve(RHS,x);
    return x;
  } else {
    Matd H=A;
    if(J)
      H+=J->transpose()**J;
    H.diagonal().array()+=mu;
    return _cons->solveSysQP(p,NULL,&H,-RHS-H*p,succ)-p;
  }
}
//helper
template <typename MAT>
bool LMInterface::solveInternal(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb)
{
  if(_cons)
    p=_cons->enforceCons(p);

  _iter=0;
  MAT J,A,JNew,ANew;
  Cold f,g,gc,fNew,gNew,pNew,delta;
  bool report=dynamic_cast<NoCallback<scalarD,Kernel<scalarD> >*>(&cb) == NULL;
  //we allow problem to be modified here
  if(!obj.onNewIter(p))
    return true;
  obj(p,f,&J,true); //compute new gradient and jacobian
  scalarD ENew,E=f.squaredNorm()/2+obj(p,&g,&A);  //add a non-square part if there is some
  scalarD mu=tau()*maxDiagElem(A,J),muMax=std::max<scalarD>(mu,1.0f)*tauMax();
  g+=J.transpose()*f;
  gc=_cons ? _cons->enforceConsZero(p,g) : g;
  if(gc.cwiseAbs().maxCoeff() < _tolg) {
    if(report) {
      INFO("Exit on tolG")
    }
    return true;
  }
  cb.reset();
  cb(p,gc,E,p.norm(),gc.norm(),mu,0,1);

#define DEC_MU  \
mu=std::max<scalarD>(mu*std::max<scalarD>(1/3.0f,1-pow(2*rho-1,3)),_minMu);  \
nu=2; \
sameA=false;
#define INC_MU  \
mu=std::max<scalarD>(mu*nu,_minMu); \
nu*=2;  \
sameA=true;
  _muHistory.clear();
  bool sameA=false,succ;
  scalarD nu=2,rho=0,deltaF=0;
  while(true) {
    delta=solveSys(p,A,-g,mu,&J,sameA,succ);
    if(succ && delta.norm() < _tolx*p.norm()) {
      if(report) {
        INFO("Exit on tolX")
      }
      break;
    } else if(!succ) {
      INC_MU
      if(mu > muMax) {
        if(report) {
          INFO("Exit on maxMu")
        }
        return false;
      }
      _iter++;
    } else {
      _muHistory.push_back(mu);
      pNew=p+delta;
      if(_cons)
        pNew=_cons->enforceCons(pNew);
      if(!_useCache) {
        obj(pNew,fNew,(MAT*)NULL,false);
        ENew=fNew.squaredNorm()/2+obj(pNew,(Cold*)NULL,(MAT*)NULL);
      } else {
        obj(pNew,fNew,&JNew,false); //compute new gradient and jacobian
        ENew=fNew.squaredNorm()/2+obj(pNew,&gNew,&ANew); //add a non-square part if there is some
      }
      deltaF=E-ENew;
      rho=deltaF/(delta.dot(mu*delta-g)/2);
      if(rho > 0 && deltaF > 0) {
        p=pNew;
        //we allow problem to be modified here
        if(!obj.onNewIter(pNew))
          return true;
        if(!_useCache) {
          obj(pNew,f,&J,true); //compute new gradient and jacobian
          E=f.squaredNorm()/2+obj(pNew,&g,&A); //add a non-square part if there is some
        } else {
          std::swap(E,ENew);
          J.swap(JNew);
          A.swap(ANew);
          f.swap(fNew);
          g.swap(gNew);
        }
        g+=J.transpose()*f;
        gc=_cons ? _cons->enforceConsZero(p,g) : g;
        if(gc.cwiseAbs().maxCoeff() < _tolg) {
          if(report) {
            INFO("Exit on tolG")
          }
          break;
        }
        if(deltaF < _tolf) {
          if(report) {
            INFO("Exit on tolF")
          }
          break;
        }
        cb(p,gc,E,p.norm(),gc.norm(),mu,_iter,1);
        DEC_MU
        _iter++;
      } else {
        INC_MU
        if(mu > muMax) {
          if(report) {
            INFO("Exit on maxMu")
          }
          return false;
        }
        _iter++;
      }
    }
    if(_iter == _maxIter) {
      if(report) {
        INFO("Exit on maxIter")
      }
      break;
    }
  }
#undef DEC_MU
#undef INC_MU
  return _iter < _maxIter;// && mu < muMax;
}
void LMInterface::setTauMax(scalarD tauMax)
{
  _tauMax=std::max<scalarD>(tauMax,1E-10f);
}
void LMInterface::setTau(scalarD tau)
{
  _tau=std::max<scalarD>(tau,1E-10f);
}
scalarD LMInterface::tau() const
{
  return _tau;
}
scalarD LMInterface::tauMax() const
{
  return _tauMax;
}
sizeType LMInterface::nrIter() const
{
  return _iter;
}
template <typename MAT>
typename MAT::Scalar LMInterface::maxDiagElem(const MAT& A,const MAT& J)
{
  typename MAT::Scalar ret=0;
  for(int a=0; a<A.rows(); a++) {
    scalarD JDiagA=J.rows()==0 ? 0 : J.col(a).squaredNorm();
    ret=std::max(ret,A.coeff(a,a)+JDiagA);
  }
  return ret;
}

PRJ_END
