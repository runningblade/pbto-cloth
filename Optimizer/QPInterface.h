#ifndef QP_INTERFACE_H
#define QP_INTERFACE_H

#include "EigenInterface.h"
#include <CommonFile/solvers/Objective.h>
#include <CommonFile/solvers/Callback.h>

PRJ_BEGIN

class QPInterface
{
public:
  typedef Objective<scalarD>::SMat SMat;
  typedef Objective<scalarD>::STrip STrip;
  typedef Objective<scalarD>::STrips STrips;
  void setCI(const SMat& CI,const Cold& CI0,const Coli& type);
  void setCI(const Matd& CI,const Cold& CI0,const Coli& type);
  void setCB(const Cold& L,const Cold& U);
  sizeType nrEquality() const;
  sizeType nrInequality() const;
  sizeType nrGeneral() const;
  sizeType vars() const;
  sizeType cons() const;
  const Cold& L() const;
  const Cold& U() const;
  const SMat* CIS() const;
  const Matd* CID() const;
  Cold enforceCons(const Cold& p);
  Cold enforceConsZero(const Cold& p,const Cold& g);
  Cold solveSysQP(const Cold& p,SMat* HS,Matd* HD,const Cold& G,bool& succ) const;
private:
  void prepareEnforceCons(const Cold& p,const Cold* g);
  SMat createCIS(const std::set<sizeType>& IG,const std::set<sizeType>& IE) const;
  Matd createCID(const std::set<sizeType>& IG,const std::set<sizeType>& IE) const;
  boost::shared_ptr<SMat> _CIS,_CIES;
  boost::shared_ptr<Matd> _CID,_CIED;
  boost::shared_ptr<Cold> _CIL,_CIU,_L,_U;
  //for enforce constraints
  boost::shared_ptr<Cold> _CI0;
  boost::shared_ptr<LinearSolverInterface> _invCICIT;
};

PRJ_END

#endif
