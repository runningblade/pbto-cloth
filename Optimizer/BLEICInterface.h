#ifndef BLEIC_INTERFACE_H
#define BLEIC_INTERFACE_H

#include <CommonFile/MathBasic.h>
#include <CommonFile/solvers/Objective.h>
#include <CommonFile/solvers/Callback.h>

PRJ_BEGIN

class BLEICInterface
{
public:
  typedef Objective<scalarD>::SMat SMat;
  BLEICInterface();
  void setTolG(scalarD tolg);
  void setTolF(scalarD tolf);
  void setTolX(scalarD tolx);
  void setTauMax(scalarD tau) {}
  void setMaxIter(sizeType maxIter);
  void setCI(const SMat& CI,const Cold& CI0,const Coli& type);
  void setCI(const Matd& CI,const Cold& CI0,const Coli& type);
  void setCB(const Cold& L,const Cold& U);
  template <typename MAT>
  bool solveTpl(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb,bool lbfgs);
  bool solveSparse(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb);
  bool solveDense(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb);
  bool solve(Cold& p,Objective<scalarD>& obj,Callback<scalarD,Kernel<scalarD> >& cb);
  sizeType getTermination() const;
  scalarD getFunctionValue() const;
  sizeType nrIter() const;
  //temporary
  Objective<scalarD>* _obj;
  Callback<scalarD,Kernel<scalarD> >* _cb;
protected:
  //data
  sizeType _maxIter;
  sizeType _ttype,_iter;
  scalarD _tolg,_tolf,_tolx,_f;
  Cold _L,_U,_CI0;
  Matd _CI;
  Coli _type;
};

PRJ_END

#endif
