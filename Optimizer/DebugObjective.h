#ifndef DEBUG_OBJECTIVE_H
#define DEBUG_OBJECTIVE_H

#include "Utils.h"
#include "LMInterface.h"
#include "AugmentedLagrangian.h"

PRJ_BEGIN

//quadratic
class Quadratic : public Objective<scalarD>
{
public:
  Quadratic(scalarD NX,scalarD NF);
  virtual int operator()(const Vec& x,Vec& fvec,Matd* fjac,bool modifiable) override;
  virtual scalarD operator()(const Vec& x,Vec* fgrad,Matd* fhess) override;
  virtual int operator()(const Vec& x,Vec& fvec,SMat* fjac,bool modifiable) override;
  virtual scalarD operator()(const Vec& x,Vec* fgrad,SMat* fhess) override;
  virtual int values() const override;
  virtual int inputs() const override;
  void setNull(sizeType id);
private:
  Matd _J,_H;
  Vec _f,_g;
};
//mass spring
template <typename PARENT>
class MassSpring : public PARENT
{
public:
  typedef typename PARENT::Vec Vec;
  typedef typename PARENT::SMat SMat;
  typedef typename PARENT::STrip STrip;
  typedef typename PARENT::STrips STrips;
  MassSpring(sizeType np,sizeType dim,scalarD l,sizeType nc,scalarD lc);
  Cold initX() const;
  void writeVTK(const Vec& x,const std::string& path) const;
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiable) override;
  sizeType GI(sizeType r,sizeType c) const;
  virtual int values() const override;
  virtual int inputs() const override;
  void setSoft(bool soft);
  void setBound(LMInterface& lm);
  void setSparse(LMInterface& lm,bool sparse,bool oneSide);
  sizeType np() const;
protected:
  bool _soft;
  sizeType _np,_dim;
  std::vector<Cold,Eigen::aligned_allocator<Cold> > _css;
  std::vector<sizeType> _pss;
  scalarD _l;
};
//mass spring dynamics
class MassSpringDynamics : public MassSpring<Objective<scalarD>>
{
public:
  MassSpringDynamics(sizeType np,sizeType dim,scalarD l,sizeType nc,scalarD lc);
  virtual scalarD operator()(const Vec& x,Vec* fgrad,SMat* fhess) override;
  void updateDynamics(const Vec& x);
  void initDynamics();
  scalarD _dt;
  Cold _xN,_x;
};
//DebugObjective
class MassSpringDynamicsWithBall : public MassSpringDynamics, public AugmentedLagrangian<sizeType>::AdaptiveConstraint
{
public:
  typedef AugmentedLagrangian<sizeType>::ConstraintInfo ConstraintInfo;
  MassSpringDynamicsWithBall(sizeType np,sizeType dim,scalarD l,sizeType nc,scalarD lc);
  virtual void evaluateConstraint(const Cold& x,sizeType& c,ConstraintInfo& cInfo,bool fjac) override;
  virtual void getNewConstraint(const Cold& x,std::vector<sizeType>& newCons) override;
  //debug
  template <typename MAT>
  static void debugExampleNonlinear(scalarD t,sizeType N,bool equality)
  {
    std::string path="MassSpring"+boost::lexical_cast<std::string>(equality?"Equality":"Inequality");
    recreate(path);
    MassSpringDynamicsWithBall obj(N,3,1,2,1);
    Callback<scalarD,Kernel<scalarD> > cb;
    Cold x=obj.initX(),x0;
    srand(0);
    x+=Cold::Random(x.size())*0.01f;
    //solver
    AugmentedLagrangian<sizeType> sol;
    sol.useInterface(LinearSolverTraits<MAT>::getCholSolver());
    sol.setThresVio(0.01f);
    //solve
    sizeType id=0;
    Colc sign=Colc::Constant(obj.values(),0);
    sign.segment(0,obj.np()*(obj.np()-1)*2).setConstant(equality?0:-1);
    for(scalarD i=0; i<t; i+=obj._dt) {
      x0=x;
      if(typeid(MAT)==typeid(DMat))
        sol.solveDense(x,obj,cb,&sign);
      else sol.solveSparse(x,obj,cb,&sign);
      obj.updateDynamics(x);
      obj.writeVTK(x,path+"/frm"+boost::lexical_cast<std::string>(id++)+".vtk");
    }
  }
  scalarD _rad;
  Vec3d _ctr;
};

PRJ_END

#endif
