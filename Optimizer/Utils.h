#ifndef UTILS_H
#define UTILS_H

#include <CommonFile/MathBasic.h>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/algorithm/string.hpp>

PRJ_BEGIN

//hash
struct Hash {
  size_t operator()(const Vec2i& key) const;
  size_t operator()(const Vec3i& key) const;
  size_t operator()(const Vec4i& key) const;
  size_t operator()(const Coli& key) const;
};
//string
bool beginsWith(const std::string& name,const std::string& regex);
bool endsWith(const std::string& name,const std::string& regex);
//filesystem
bool exists(const boost::filesystem::path& path);
void remove(const boost::filesystem::path& path);
void removeDir(const boost::filesystem::path& path);
void create(const boost::filesystem::path& path);
void recreate(const boost::filesystem::path& path);
std::vector<boost::filesystem::path> files(const boost::filesystem::path& path);
std::vector<boost::filesystem::path> directories(const boost::filesystem::path& path);
void sortFilesByNumber(std::vector<boost::filesystem::path>& files);
bool isDir(const boost::filesystem::path& path);
size_t fileSize(const boost::filesystem::path& path);
std::vector<std::string> toParams(sizeType argc,char** argv);
std::string parseProps(sizeType argc,char** argv,boost::property_tree::ptree& pt);
std::string parseProps(const std::vector<std::string>& params,boost::property_tree::ptree& pt);
//property tree
void readPtreeBinary(boost::property_tree::ptree& pt,std::istream& is);
void writePtreeBinary(const boost::property_tree::ptree& pt,std::ostream& os);
void readPtreeAscii(boost::property_tree::ptree& pt,const boost::filesystem::path& path);
void writePtreeAscii(const boost::property_tree::ptree& pt,const boost::filesystem::path& path,const std::string& suffix="xml");
void writePtreeAscii(const boost::property_tree::ptree& pt,std::ostream& os,const std::string& suffix="xml");
template <typename SCALAR>
struct PtreeSafeScalar {
  typedef SCALAR SAFE_SCALAR;
};
template <>
struct PtreeSafeScalar<scalarD> {
  typedef scalarF SAFE_SCALAR;
};
template <typename EIGEN_VEC>
EIGEN_VEC parsePtreeDef(const boost::property_tree::ptree& pt,const std::string& path,const std::string& def,sizeType r=-1,sizeType c=-1)
{
  EIGEN_VEC ret;
  if(r<0&&c<0)
    ret.setZero();
  else if(r>=0&&c<0)
    ret.setZero(r);
  else ret.setZero(r,c);
  //read
  std::vector<std::string> strs;
  std::string str=pt.get<std::string>(path,def);
  boost::split(strs,str,boost::is_any_of(" _,"));
  if(str.empty()) //fixing boost bug
    strs.clear();
  ASSERT_MSGV(ret.size()==0 || (sizeType)strs.size() == ret.size(),
              "Try reading: %s, Vec size mismatch, size(strs)=%ld size(ret)=%ld!",
              path.c_str(),strs.size(),ret.size())
  if(ret.size() == 0)
    ret.resize((sizeType)strs.size());
  for(sizeType i=0; i<ret.size(); i++)
    ret[i]=boost::lexical_cast<typename PtreeSafeScalar<typename EIGEN_VEC::Scalar>::SAFE_SCALAR>(strs[i]);
  return ret;
}
template <typename EIGEN_VEC>
EIGEN_VEC parsePtreeDef(const boost::property_tree::ptree& pt,const std::string& path,const EIGEN_VEC& ret,sizeType r=-1,sizeType c=-1)
{
  std::string val;
  for(sizeType i=0; i<ret.size(); i++)
    if(i == 0)
      val+=boost::lexical_cast<std::string>((typename PtreeSafeScalar<typename EIGEN_VEC::Scalar>::SAFE_SCALAR)ret[i]);
    else val+=" "+boost::lexical_cast<std::string>((typename PtreeSafeScalar<typename EIGEN_VEC::Scalar>::SAFE_SCALAR)ret[i]);
  return parsePtreeDef<EIGEN_VEC>(pt,path,val,r,c);
}
template <typename EIGEN_VEC>
EIGEN_VEC parsePtreeDef(const boost::property_tree::ptree& pt,const std::string& path,typename EIGEN_VEC::Scalar val,sizeType r=-1,sizeType c=-1)
{
  EIGEN_VEC ret;
  if(r<0&&c<0)
    ret.setZero();
  else if(r>=0&&c<0)
    ret.setZero(r);
  else ret.setZero(r,c);
  ret.setConstant(val);
  return parsePtreeDef<EIGEN_VEC>(pt,path,ret,r,c);
}
template <typename EIGEN_VEC>
EIGEN_VEC parsePtree(const boost::property_tree::ptree& pt,const std::string& path,sizeType r=-1,sizeType c=-1)
{
  return parsePtreeDef<EIGEN_VEC>(pt,path,"",r,c);
}
template <typename EIGEN_VEC>
void putPtree(boost::property_tree::ptree& pt,const std::string& path,const EIGEN_VEC& ret)
{
  std::string val;
  for(sizeType i=0; i<ret.size(); i++)
    if(i == 0)
      val+=boost::lexical_cast<std::string>((typename PtreeSafeScalar<typename EIGEN_VEC::Scalar>::SAFE_SCALAR)ret[i]);
    else val+=" "+boost::lexical_cast<std::string>((typename PtreeSafeScalar<typename EIGEN_VEC::Scalar>::SAFE_SCALAR)ret[i]);
  pt.put<std::string>(path,val);
}
template <typename T>
void putCond(boost::property_tree::ptree& pt,const std::string& path,T val)
{
  if(!pt.get_optional<T>(path))
    pt.put<T>(path,val);
}
template <typename EIGEN_VEC>
std::string toStringPtree(const EIGEN_VEC& v)
{
  std::ostringstream oss;
  for(sizeType i=0; i<v.size(); i++) {
    oss << v[i];
    if(i<v.size()-1)
      oss << ",";
  }
  return oss.str();
}
template <typename T>
std::string toStringPtree(const std::vector<T>& v)
{
  std::ostringstream oss;
  for(sizeType i=0; i<(sizeType)v.size(); i++) {
    oss << v[i];
    if(i<(sizeType)v.size()-1)
      oss << ",";
  }
  return oss.str();
}
// ----------------------------------------------------------------------------
// Calculates the eigenvalues of a symmetric 3x3 matrix A using Cardano's
// analytical algorithm.
// Only the diagonal and upper triangular parts of A are accessed. The access
// is read-only.
// ----------------------------------------------------------------------------
// Parameters:
//   A: The symmetric input matrix
//   w: Storage buffer for eigenvalues
// ----------------------------------------------------------------------------
// Return value:
//   0: Success
//  -1: Error
// ----------------------------------------------------------------------------
#define M_SQRT3 std::sqrt(T(3.0))
#define SQR(x) ((x)*(x))
template <typename T>
static int dsyevc3(T A[3][3],T w[3])
{
  T m, c1, c0;

  // Determine coefficients of characteristic poynomial. We write
  //       | a   d   f  |
  //  A =  | d*  b   e  |
  //       | f*  e*  c  |
  T de = A[0][1] * A[1][2];                                    // d * e
  T dd = SQR(A[0][1]);                                         // d^2
  T ee = SQR(A[1][2]);                                         // e^2
  T ff = SQR(A[0][2]);                                         // f^2
  m  = A[0][0] + A[1][1] + A[2][2];
  c1 = (A[0][0]*A[1][1] + A[0][0]*A[2][2] + A[1][1]*A[2][2])        // a*b + a*c + b*c - d^2 - e^2 - f^2
       - (dd + ee + ff);
  c0 = A[2][2]*dd + A[0][0]*ee + A[1][1]*ff - A[0][0]*A[1][1]*A[2][2]
       - 2.0 * A[0][2]*de;                                     // c*d^2 + a*e^2 + b*f^2 - a*b*c - 2*f*d*e)

  T p, sqrt_p, q, c, s, phi;
  p = SQR(m) - 3.0*c1;
  q = m*(p - (3.0/2.0)*c1) - (27.0/2.0)*c0;
  sqrt_p = sqrt(fabs(p));

  phi = 27.0 * ( 0.25*SQR(c1)*(p - c1) + c0*(q + 27.0/4.0*c0));
  phi = (1.0/3.0) * atan2(sqrt(fabs(phi)), q);

  c = sqrt_p*cos(phi);
  s = (1.0/M_SQRT3)*sqrt_p*sin(phi);

  w[1]  = (1.0/3.0)*(m - c);
  w[2]  = w[1] + s;
  w[0]  = w[1] + c;
  w[1] -= s;
  return 0;
}
#undef SQR
#undef M_SQRT3

PRJ_END

#endif
