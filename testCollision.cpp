#include "Mesh/Mesh.h"
#include "Mesh/CollisionHandler.h"

USE_PRJ_NAMESPACE

int main()
{
  std::vector<boost::shared_ptr<Mesh>> meshes;
  for(sizeType i=0;i<2;i++)
    meshes.push_back(boost::shared_ptr<Mesh>(new Mesh(Mesh::createSheet(Vec2i(10,20),0.1f))));
  boost::shared_ptr<CollisionDetector> cd(new CollisionDetector);
  cd->debugBroadphase(meshes,0.1f,false);
  cd->debugBroadphase(meshes,0.1f,true);
  {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ofstream os("tmp.dat",std::ios::binary);
    CollisionDetector::registerTypes(dat.get());
    writeBinaryData(cd,os,dat.get());
  }
  {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ifstream is("tmp.dat",std::ios::binary);
    CollisionDetector::registerTypes(dat.get());
    readBinaryData(cd,is,dat.get());
  }
  return 0;
}
