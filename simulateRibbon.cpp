#include "Mesh/Simulator.h"
#include "CommonFile/MakeMesh.h"

USE_PRJ_NAMESPACE

int main()
{
  boost::shared_ptr<Mesh> mesh(new MeshRibbon(0.1f,0.05f,40));
  boost::shared_ptr<Simulator> sim(new Simulator(mesh,Simulator::NRZ_QP));
  {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ofstream os("tmp.dat",std::ios::binary);
    Simulator::registerTypes(dat.get());
    writeBinaryData(sim,os,dat.get());
  }
  {
    sim=NULL;
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ifstream is("tmp.dat",std::ios::binary);
    Simulator::registerTypes(dat.get());
    readBinaryData(sim,is,dat.get());
  }
  sim->initRibbon(1/600.0f,1,Vec3d(0,-9.81f,-1),M_PI*3/4,0.05f);
  sim->fix(0,6,0);
  {
    ObjMesh boxm;
    MakeMesh::makeBox3D(boxm,Vec3(1,1,1));
    boxm.getPos()=Vec3(0,-1.5,0);
    boxm.applyTrans();
    boxm.writeVTK("box.vtk",true);
    boost::shared_ptr<Mesh> box(new Mesh(boxm));
    boost::shared_ptr<MeshCache> boxCache(new MeshCache);
    boxCache->_param=box->param0();
    box->paramToVertex(*boxCache);
    boxCache->saveN();
    sim->getColl().getColl().addMesh(box,boxCache);
  }
  sim->advance("RibbonSim",10.0f,true);
  return 0;
}
