#include "Mesh/Simulator.h"

USE_PRJ_NAMESPACE

int main()
{
  boost::shared_ptr<Mesh> mesh(new Mesh(Mesh::createSheet(Vec2i(10,10),0.1f)));
  boost::shared_ptr<Simulator> sim(new Simulator(mesh));
  {
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ofstream os("tmp.dat",std::ios::binary);
    Simulator::registerTypes(dat.get());
    writeBinaryData(sim,os,dat.get());
  }
  {
    sim=NULL;
    boost::shared_ptr<IOData> dat=getIOData();
    boost::filesystem::ifstream is("tmp.dat",std::ios::binary);
    Simulator::registerTypes(dat.get());
    readBinaryData(sim,is,dat.get());
  }
  sim->initCloth(1/60.0f,1,100,Vec3d(0,-9.81f,-1),M_PI/10,0.01f);
  sim->fixVertex(0);
  sim->fixVertex(10);
  sim->advance("ClothSim",10.0f,true);
  return 0;
}
