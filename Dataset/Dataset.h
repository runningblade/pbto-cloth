#ifndef DATASET_H
#define DATASET_H

#include "Mesh/Simulator.h"

PRJ_BEGIN

class Dataset : public SerializableBase
{
public:
  typedef Objective<scalarD>::Vec Vec;
  typedef Objective<scalarD>::SMat SMat;
  typedef Objective<scalarD>::STrip STrip;
  typedef Objective<scalarD>::STrips STrips;
  typedef boost::unordered_map<sizeType,boost::unordered_set<sizeType>> CONNECTIVITY;
  Dataset();
  Dataset(boost::shared_ptr<Simulator> sim,sizeType nrBendComponent);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  void buildDataset(sizeType nrShape);
  void writePython(const std::string& path) const;
  void writeVTK(const std::string& path) const;
protected:
  Vec sampleShape();
  void dijkstra(sizeType source);
  bool canConnect(sizeType r,sizeType c);
  //dataset
  Matd _shapes;
  CONNECTIVITY _connectivity;
  std::vector<scalarD> _dist;
  std::vector<sizeType> _prev;
  //configuration
  sizeType _nrBendComponent;
  boost::shared_ptr<Simulator> _sim;
};

PRJ_END

#endif
