#include "Dataset.h"
#include "CommonFile/Heap.h"
#include "Optimizer/Utils.h"

USE_PRJ_NAMESPACE

Dataset::Dataset() {}
Dataset::Dataset(boost::shared_ptr<Simulator> sim,sizeType nrBendComponent):_nrBendComponent(nrBendComponent),_sim(sim) {}
bool Dataset::read(std::istream& is,IOData* dat)
{
  Simulator::registerTypes(dat);
  readBinaryData(_shapes,is,dat);
  readBinaryData(_connectivity,is,dat);
  readBinaryData(_nrBendComponent,is,dat);
  readBinaryData(_sim,is,dat);
  readBinaryData(_dist,is,dat);
  readBinaryData(_prev,is,dat);
  return is.good();
}
bool Dataset::write(std::ostream& os,IOData* dat) const
{
  Simulator::registerTypes(dat);
  writeBinaryData(_shapes,os,dat);
  writeBinaryData(_connectivity,os,dat);
  writeBinaryData(_nrBendComponent,os,dat);
  writeBinaryData(_sim,os,dat);
  writeBinaryData(_dist,os,dat);
  writeBinaryData(_prev,os,dat);
  return os.good();
}
boost::shared_ptr<SerializableBase> Dataset::copy() const
{
  return boost::shared_ptr<SerializableBase>(new Dataset);
}
std::string Dataset::type() const
{
  return typeid(Dataset).name();
}
void Dataset::buildDataset(sizeType nrShape)
{
  boost::shared_ptr<Mesh> mesh=_sim->getColl().getColl().getMesh(0);
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(mesh);
  //sample shapes
  sizeType r0;
  if(_connectivity.empty()) {
    //create dataset
    _connectivity.clear();
    _shapes.resize(ribbon->nrParam(),nrShape);
    _shapes.col(0)=ribbon->param0();
    r0=1;
  } else {
    //augment dataset
    Matd tmp=Matd::Zero(ribbon->nrParam(),_shapes.cols()+nrShape);
    tmp.block(0,0,_shapes.rows(),_shapes.cols())=_shapes;
    r0=_shapes.cols();
    _shapes=tmp;
  }
  for(sizeType r=r0; r<_shapes.cols(); r++) {
    INFOV("Sampling shape %ld/%ld!",r,nrShape)
    bool connected=false;
    while(!connected) {
      _shapes.col(r)=sampleShape();
      for(sizeType c=0; c<r; c++)
        if(canConnect(r,c)) {
          _connectivity[r].insert(c);
          _connectivity[c].insert(r);
          connected=true;
        }
    }
  }
  //compute Dijkstra from shape[0]
  dijkstra(0);
}
void Dataset::writePython(const std::string& path) const
{
  boost::filesystem::ofstream os(path);
  os << _shapes.cols() << std::endl;
  os << _shapes.transpose() << std::endl;
  for(CONNECTIVITY::const_iterator beg=_connectivity.begin(),end=_connectivity.end();beg!=end;beg++) {
    for(boost::unordered_set<sizeType>::const_iterator beg2=beg->second.begin(),end2=beg->second.end();beg2!=end2;beg2++)
      os << beg->first << "<->" << *beg2 << " ";
    os << std::endl;
  }
  os << Eigen::Map<const Vec>(&_dist[0],(sizeType)_dist.size()).transpose() << std::endl;
  os << Eigen::Map<const Coli>(&_prev[0],(sizeType)_prev.size()).transpose() << std::endl;
}
void Dataset::writeVTK(const std::string& path) const
{
  recreate(path);
  boost::shared_ptr<Mesh> mesh=_sim->getColl().getColl().getMesh(0);
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(mesh);
  boost::shared_ptr<MeshCache> cache=_sim->getColl().getColl().getNode(0)->_cache;
  for(sizeType i=0;i<(sizeType)_shapes.cols();i++) {
    cache->_param=_shapes.col(i);
    ribbon->paramToVertex(*cache,false);
    ribbon->writeVTK(*cache,path+"/frm"+std::to_string(i)+".vtk");
  }
}
//helper
Dataset::Vec Dataset::sampleShape()
{
  boost::shared_ptr<Mesh> mesh=_sim->getColl().getColl().getMesh(0);
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(mesh);
  boost::shared_ptr<MeshCache> cache=_sim->getColl().getColl().getNode(0)->_cache;
  boost::shared_ptr<BendRibbonConstraint> bendLmt=_sim->getLinearConstraint<BendRibbonConstraint>();
  boost::shared_ptr<RuleRibbonConstraint> ruleLmt=_sim->getLinearConstraint<RuleRibbonConstraint>();
  ASSERT_MSG(bendLmt && ruleLmt,"We must have bendLmt & ruleLmt to perform shape sampling!")
  scalarD len=ribbon->segLen()*ribbon->nrSegment();
  scalarD width=ribbon->width();
  scalarD theta=bendLmt->lmt();

  std::vector<scalarD> centers(_nrBendComponent);
  std::vector<scalarD> rules(_nrBendComponent);
  std::vector<scalarD> thetas(_nrBendComponent);
  std::vector<scalarD> fallOffs(_nrBendComponent);
  while(true) {
    //sample parameters
    for(sizeType b=0; b<_nrBendComponent; b++) {
      centers[b]=RandEngine::randR(0,len);
      scalarD ruleLmt=std::min(centers[b],len-centers[b])/width;
      rules[b]=RandEngine::randR(-ruleLmt,ruleLmt);
      thetas[b]=RandEngine::randR(-theta,theta);
      fallOffs[b]=std::log(1E-3f)/RandEngine::randR(len,len*10);
    }
    //use parameters to create param
    cache->_param=ribbon->param0();
    for(sizeType i=1; i<ribbon->nrSegment(); i++)
      for(sizeType b=0; b<_nrBendComponent; b++) {
        scalarD dl=std::abs(i*ribbon->segLen()-centers[b]);
        scalarD fallOff=std::exp(fallOffs[b]*dl);
        cache->_param[4+i*2]+=fallOff*rules[b];
        cache->_param[5+i*2]+=fallOff*thetas[b];
      }
    //check rule limit
    cache->_C.resize(0);
    cache->_DCDParam.clear();
    ruleLmt->operator()(*cache);
    bool ruleSatisfied=compLE(ruleLmt->lb(),cache->_C) && compLE(cache->_C,ruleLmt->ub());
    if(!ruleSatisfied)
      continue;
    //return if no collision
    CollisionHandler handler;
    ribbon->paramToVertex(*cache,false);
    _sim->getColl().getColl().collide(handler,false,false,false,true);
    if(handler.empty())
      return cache->_param;
  }
}
void Dataset::dijkstra(sizeType source)
{

  sizeType err;
  std::vector<sizeType> heap,heapOff(_shapes.cols(),-1);
  _dist.assign(_shapes.cols(),ScalarUtil<scalarD>::scalar_max());
  std::vector<bool> visited(_shapes.cols(),false);
  _prev.assign(_shapes.cols(),-1);
  _dist[source]=0;
  for(sizeType i=0; i<_shapes.cols(); i++)
    pushHeapDef(_dist,heapOff,heap,i);
  //dijkstra
  while(!heap.empty()) {
    sizeType minId=popHeapDef(_dist,heapOff,heap,err);
    visited[minId]=true;
    //process
    const CONNECTIVITY::const_iterator& it=_connectivity.find(minId);
    if(it==_connectivity.end())
      continue;
    for(boost::unordered_set<sizeType>::const_iterator
        beg=it->second.begin(),end=it->second.end(); beg!=end; beg++) {
      sizeType other=*beg;
      if(_dist[minId]+1<_dist[other]) {
        _dist[other]=_dist[minId]+1;
        updateHeapDef(_dist,heapOff,heap,other);
        _prev[other]=minId;
      }
    }
  }
}
bool Dataset::canConnect(sizeType r,sizeType c)
{
  boost::shared_ptr<Mesh> mesh=_sim->getColl().getColl().getMesh(0);
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(mesh);
  boost::shared_ptr<MeshCache> cache=_sim->getColl().getColl().getNode(0)->_cache;
  //fill cache
  cache->_param=_shapes.col(r);
  ribbon->paramToVertex(*cache);
  cache->saveN();
  cache->_param=_shapes.col(c);
  ribbon->paramToVertex(*cache);
  //test continuous collision
  CollisionHandler handler;
  _sim->getColl().getColl().collide(handler,true,false,false,true);
  return handler.empty();
}
