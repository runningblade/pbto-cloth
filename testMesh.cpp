#include "Mesh/Mesh.h"

USE_PRJ_NAMESPACE

int main()
{
  {
    Mesh mesh=Mesh::createSheet(Vec2i(10,20),0.1f);
    mesh.debugGradient();
    mesh.writeVTKParam(mesh.randomParam(0.3),"testMesh.vtk");
  }
  {
    MeshRibbon mesh(0.1,0.05,10);
    mesh.debugGradient();
    mesh.writeVTKParam(mesh.randomParam(1,1,10,0.3),"testMeshRibbon.vtk");
  }
  return 0;
}
