#ifndef IMPULSE_COLLISION_SOLVER_H
#define IMPULSE_COLLISION_SOLVER_H

#include "CollisionHandler.h"

PRJ_BEGIN

struct ImpulseCollisionHandler : public CollisionHandler
{
  ImpulseCollisionHandler(boost::shared_ptr<CollisionDetector> coll,scalarD mu,scalarD e,scalarD thickness);
  virtual void handle(const Cache<VertexRef,TriangleRef>& VT) override;
  virtual void handle(const Cache<EdgeRef,EdgeRef>& EE) override;
  void applyImpulse(const VertexRef vs[4],const Vec4d& m,const Vec3d& dv,const Vec4d& omg,const Vec3d& n);
  Eigen::Map<Vec3d> pNonConst(const VertexRef& ref);
  Vec3d p(const VertexRef& ref) const;
  scalarD m(const VertexRef& ref) const;
  boost::shared_ptr<CollisionDetector> _coll;
  scalarD _mu,_e,_thickness;
  Cold _posNew;
  bool _more;
};
class ImpulseCollisionSolver : public SerializableBase
{
public:
  ImpulseCollisionSolver();
  ImpulseCollisionSolver(boost::shared_ptr<Mesh> mesh,boost::shared_ptr<MeshCache> cache);
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  const CollisionDetector& getColl() const;
  CollisionDetector& getColl();
  virtual bool solve();
protected:
  bool errExit();
  boost::shared_ptr<Mesh> _mesh;
  boost::shared_ptr<MeshCache> _cache;
  boost::shared_ptr<CollisionDetector> _coll;
  scalarD _mu,_e;
  scalarD _thicknessCCR;
  scalarD _thicknessIZ;
};

PRJ_END

#endif
