#include "Simulator.h"
#include "Optimizer/Utils.h"
#include "Optimizer/SparseUtils.h"
#include "Optimizer/AugmentedLagrangian.h"

USE_PRJ_NAMESPACE

Simulator::Simulator() {}
Simulator::Simulator(boost::shared_ptr<Mesh> mesh,COLL coll):_mesh(mesh)
{
  _c.reset(new MeshCache);
  _c->_param=_mesh->param0();
  _mesh->paramToVertex(*_c,true);
  _c->saveNN();
  _c->saveN();

  if(coll==IMPULSE)
    _coll.reset(new ImpulseCollisionSolver(_mesh,_c));
  else if(coll==NRZ)
    _coll.reset(new NRZCollisionSolver(_mesh,_c,NULL));
  else if(coll==NRZ_QP) {
    boost::shared_ptr<LinearSpringEnergy> e(new LinearSpringEnergy(_mesh,0.01f));
    _coll.reset(new NRZCollisionSolver(_mesh,_c,e));
  } else {
    ASSERT_MSG(false,"Unknown collision type!")
  }

  resetVariableBound();
  resetLinearConstraint();
  resetNonlinearConstraint();
}
bool Simulator::read(std::istream& is,IOData* dat)
{
  CollisionDetector::registerTypes(dat);
  registerType<ImpulseCollisionSolver>(dat);
  registerType<NRZCollisionSolver>(dat);
  readBinaryData(_mesh,is,dat);
  readBinaryData(_fixes,is,dat);
  readBinaryData(_ess,is,dat);
  readBinaryData(_css,is,dat);
  readBinaryData(_lcss,is,dat);
  readBinaryData(_coll,is,dat);
  readBinaryData(_c,is,dat);
  readBinaryData(_CI,is);
  readBinaryData(_CI0,is,dat);
  readBinaryData(_NCI0,is,dat);
  readBinaryData(_l,is,dat);
  readBinaryData(_u,is,dat);
  return is.good();
}
bool Simulator::write(std::ostream& os,IOData* dat) const
{
  CollisionDetector::registerTypes(dat);
  registerType<ImpulseCollisionSolver>(dat);
  registerType<NRZCollisionSolver>(dat);
  writeBinaryData(_mesh,os,dat);
  writeBinaryData(_fixes,os,dat);
  writeBinaryData(_ess,os,dat);
  writeBinaryData(_css,os,dat);
  writeBinaryData(_lcss,os,dat);
  writeBinaryData(_coll,os,dat);
  writeBinaryData(_c,os,dat);
  writeBinaryData(_CI,os);
  writeBinaryData(_CI0,os,dat);
  writeBinaryData(_NCI0,os,dat);
  writeBinaryData(_l,os,dat);
  writeBinaryData(_u,os,dat);
  return os.good();
}
boost::shared_ptr<SerializableBase> Simulator::copy() const
{
  return boost::shared_ptr<SerializableBase>(new Simulator);
}
std::string Simulator::type() const
{
  return typeid(Simulator).name();
}
void Simulator::initRibbon(scalarD dt,scalarD bendCoef,const Vec3d& g,scalarD bendLmt,scalarD ruleReg)
{
  ASSERT(bendCoef>0)
  addEnergy(boost::shared_ptr<Energy>(new BendRibbonEnergy(_mesh,bendCoef)));
  ASSERT(dt>0)
  addEnergy(boost::shared_ptr<Energy>(new KinematicRibbonEnergy(_mesh,1/(dt*dt))));
  if(g.norm()>0)
    addEnergy(boost::shared_ptr<Energy>(new ConstantForceRibbonEnergy(_mesh,g,1)));
  ASSERT(ruleReg>0)
  addConstraint(boost::shared_ptr<LinearConstraint>(new RuleRibbonConstraint(_mesh,ruleReg)));
  if(bendLmt>0)
    addConstraint(boost::shared_ptr<LinearConstraint>(new BendRibbonConstraint(_mesh,bendLmt)));
}
void Simulator::initCloth(scalarD dt,scalarD bendCoef,scalarD stretchCoef,const Vec3d& g,scalarD bendLmt,scalarD stretchLmt)
{
  ASSERT(bendCoef>0)
  addEnergy(boost::shared_ptr<Energy>(new BendEnergy(_mesh,bendCoef)));
  addEnergy(boost::shared_ptr<Energy>(new StretchEnergy(_mesh,stretchCoef,0.45f)));
  ASSERT(dt>0)
  addEnergy(boost::shared_ptr<Energy>(new KinematicEnergy(_mesh,1/(dt*dt))));
  if(g.norm()>0)
    addEnergy(boost::shared_ptr<Energy>(new ConstantForceEnergy(_mesh,g,1)));
  if(bendLmt>0)
    addConstraint(boost::shared_ptr<Constraint>(new BendConstraint(_mesh,bendLmt)));
  if(stretchLmt>0)
    addConstraint(boost::shared_ptr<Constraint>(new SpringConstraint(_mesh,Vec2d(1-stretchLmt,1+stretchLmt))));
}
void Simulator::addEnergy(boost::shared_ptr<Energy> e)
{
  if(std::find(_ess.begin(),_ess.end(),e)==_ess.end())
    _ess.push_back(e);
}
void Simulator::delEnergy(boost::shared_ptr<Energy> e)
{
  std::vector<boost::shared_ptr<Energy>>::const_iterator it=std::find(_ess.begin(),_ess.end(),e);
  if(it!=_ess.end())
    _ess.erase(it);
}
void Simulator::addConstraint(boost::shared_ptr<Constraint> c)
{
  if(boost::dynamic_pointer_cast<LinearConstraint>(c))
    addConstraint(boost::dynamic_pointer_cast<LinearConstraint>(c));
  else {
    if(std::find(_css.begin(),_css.end(),c)==_css.end())
      _css.push_back(c);
    resetNonlinearConstraint();
  }
}
void Simulator::addConstraint(boost::shared_ptr<LinearConstraint> c)
{
  if(std::find(_lcss.begin(),_lcss.end(),c)==_lcss.end()) {
    _lcss.push_back(c);
    resetLinearConstraint();
  }
}
void Simulator::delConstraint(boost::shared_ptr<Constraint> c)
{
  std::vector<boost::shared_ptr<Constraint>>::const_iterator it=std::find(_css.begin(),_css.end(),c);
  if(it!=_css.end()) {
    _css.erase(it);
    resetNonlinearConstraint();
  }
  std::vector<boost::shared_ptr<LinearConstraint>>::const_iterator itl=std::find(_lcss.begin(),_lcss.end(),c);
  if(itl!=_lcss.end()) {
    _lcss.erase(itl);
    resetLinearConstraint();
  }
}
scalarD Simulator::getDt() const
{
  for(std::vector<boost::shared_ptr<Energy>>::const_iterator
      beg=_ess.begin(),end=_ess.end(); beg!=end; beg++)
  {
    boost::shared_ptr<Energy> e=(*beg);
    if(boost::dynamic_pointer_cast<KinematicEnergy>(e))
      return boost::dynamic_pointer_cast<KinematicEnergy>(e)->dt();
    else if(boost::dynamic_pointer_cast<KinematicRibbonEnergy>(e))
      return boost::dynamic_pointer_cast<KinematicRibbonEnergy>(e)->dt();
  }
  ASSERT(false)
  return 0;
}
void Simulator::fixVertex(sizeType id)
{
  for(sizeType i=0; i<3; i++)
    _fixes[id*3+i]=_mesh->param0()[id*3+i];
  resetVariableBound();
}
void Simulator::fix(sizeType id,scalarD val)
{
  _fixes[id]=val;
  resetVariableBound();
}
void Simulator::fix(sizeType beg,sizeType end,scalarD val)
{
  while(beg!=end)
    _fixes[beg++]=val;
  resetVariableBound();
}
//simulator
void Simulator::advance(bool useCB)
{
  _c->_dEdVss.resize(_mesh->nrVertex()*3);
  _c->_dEdParam.resize(_mesh->nrParam());
  Callback<scalarD,Kernel<scalarD>> cb;
  NoCallback<scalarD,Kernel<scalarD>> noCb;
  if(_css.empty()) {
    BLEICInterface sol;
    sol.setCB(_l,_u);
    sol.setCI(_CI,_CI0,Coli::Constant(_CI0.size(),1));
    sol.solve(_c->_param,*this,useCB?cb:noCb);
  } else {
    AugmentedLagrangian<sizeType,BLEICInterface> sol;
    Colc sign=Colc::Constant(values(),1);
    sol.setCB(_l,_u);
    sol.setCI(_CI,_CI0,Coli::Constant(_CI0.size(),1));
    sol.solve(_c->_param,*this,useCB?cb:noCb,&sign);
  }
  if(!_coll->solve())
    return;
  _c->saveN2NN();
  _c->saveN();
}
void Simulator::advance(const std::string& path,scalarD t,bool useCB)
{
  recreate(path);
  sizeType off=0;
  scalarD dt=getDt();
  for(scalarD i=0; i<t; i+=dt) {
    advance(useCB);
    _mesh->writeVTK(*_c,path+"/frm"+std::to_string(off)+".vtk");
    off++;
  }
}
const ImpulseCollisionSolver& Simulator::getColl() const
{
  return *_coll;
}
ImpulseCollisionSolver& Simulator::getColl()
{
  return *_coll;
}
void Simulator::registerTypes(IOData* dat)
{
  registerType<Mesh>(dat);
  registerType<MeshCache>(dat);
  registerType<MeshRibbon>(dat);
  registerType<Simulator>(dat);
  //Energy
  registerType<SpringEnergy>(dat);
  registerType<LinearSpringEnergy>(dat);
  registerType<StretchEnergy>(dat);
  registerType<BendEnergy>(dat);
  registerType<BendRibbonEnergy>(dat);
  registerType<KinematicEnergy>(dat);
  registerType<KinematicRibbonEnergy>(dat);
  registerType<ConstantForceEnergy>(dat);
  registerType<ConstantForceRibbonEnergy>(dat);
  registerType<SoftPointConstraintEnergy>(dat);
  registerType<SoftNormalConstraintEnergy>(dat);
  //Constraint
  registerType<SpringConstraint>(dat);
  registerType<BendConstraint>(dat);
  registerType<RuleRibbonConstraint>(dat);
  registerType<BendRibbonConstraint>(dat);
}
//objective
int Simulator::operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient)
{
  _c->_param=x;
  _mesh->paramToVertex(*_c);
  _c->_E=0;
  _c->_dEdVss.setZero();
  _c->_dEdParam.setZero();
  for(sizeType i=0; i<(sizeType)_ess.size(); i++)
    _ess[i]->operator()(*_c);
  _mesh->paramToVertexGradient(*_c);
  FX=_c->_E;
  DFDX=_c->_dEdParam;
  return 0;
}
int Simulator::operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiableOrSameX)
{
  _c->_C.setZero(0);
  _c->_DCDParam.clear();
  for(sizeType i=0; i<(sizeType)_css.size(); i++)
    _css[i]->operator()(*_c);
  fvec=_NCI0+concat(_c->_C,-_c->_C);
  if(fjac) {
    fjac->clear();
    const STrips::vector_type& trips=_c->_DCDParam.getVector();
    for(sizeType i=0; i<(sizeType)trips.size(); i++) {
      const STrip& t=trips[i];
      fjac->push_back(t);
      fjac->push_back(STrip(_c->_C.size()+t.row(),t.col(),-t.value()));
    }
  }
  return 0;
}
int Simulator::inputs() const
{
  return _mesh->nrParam();
}
int Simulator::values() const
{
  return _NCI0.size();
}
//data
void Simulator::resetVariableBound()
{
  _l.setConstant(_mesh->nrParam(),-1E6);
  _u.setConstant(_mesh->nrParam(), 1E6);
  for(std::map<sizeType,scalarD>::const_iterator beg=_fixes.begin(),end=_fixes.end(); beg!=end; beg++)
    _l[beg->first]=_u[beg->first]=beg->second;
}
void Simulator::resetLinearConstraint()
{
  _c->_param=_mesh->param0();
  _mesh->paramToVertex(*_c,true);
  _c->_C.resize(0);
  _c->_DCDParam.clear();
  //lb,ub
  Vec lb,ub;
  for(sizeType i=0; i<(sizeType)_lcss.size(); i++) {
    _lcss[i]->operator()(*_c);
    if(i==0) {
      lb=_lcss[i]->lb();
      ub=_lcss[i]->ub();
    } else {
      lb=concat(lb,_lcss[i]->lb());
      ub=concat(ub,_lcss[i]->ub());
    }
  }
  //constraint
  _CI=_c->dCdParam();
  _CI=concat<scalarD>(_CI,-_CI);
  _CI0=concat(-lb,ub);
}
void Simulator::resetNonlinearConstraint()
{
  //lb,ub
  Vec lb,ub;
  for(sizeType i=0; i<(sizeType)_css.size(); i++)
    if(i==0) {
      lb=_css[i]->lb();
      ub=_css[i]->ub();
    } else {
      lb=concat(lb,_css[i]->lb());
      ub=concat(ub,_css[i]->ub());
    }
  //constraint
  _NCI0=concat(-lb,ub);
}
