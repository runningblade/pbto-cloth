#include "CollisionHandler.h"
#include "CommonFile/Interp.h"
#include "Optimizer/Utils.h"

USE_PRJ_NAMESPACE

CollisionHandler::CollisionHandler() {}
CollisionHandler::~CollisionHandler() {}
void CollisionHandler::handle(const Cache<TriangleRef,TriangleRef>& c)
{
  _cacheTT.insert(c);
}
void CollisionHandler::handle(const Cache<VertexRef,TriangleRef>& c)
{
  _cacheVT.insert(c);
}
void CollisionHandler::handle(const Cache<EdgeRef,EdgeRef>& c)
{
  _cacheEE.insert(c);
}
bool CollisionHandler::operator==(const CollisionHandler& other) const
{
  if(_cacheTT.size()!=other._cacheTT.size())
    return false;
  if(_cacheVT.size()!=other._cacheVT.size())
    return false;
  if(_cacheEE.size()!=other._cacheEE.size())
    return false;
  return _cacheTT==other._cacheTT && _cacheVT==other._cacheVT && _cacheEE==other._cacheEE;
}
bool CollisionHandler::empty() const
{
  return _cacheVT.empty() && _cacheEE.empty() && _cacheTT.empty();
}
void CollisionHandler::writeVTK(const std::string& path)
{
  _path=path;
  recreate(_path);
  sizeType off;
  //TTs
  off=0;
  for(CollisionDetector::CACHE_TTs::const_iterator beg=_cacheTT.begin(),end=_cacheTT.end(); beg!=end; beg++)
    writeVTK(*beg,off++);
  //VTs
  off=0;
  for(CollisionDetector::CACHE_VTs::const_iterator beg=_cacheVT.begin(),end=_cacheVT.end(); beg!=end; beg++)
    writeVTK(*beg,off++);
  //EEs
  off=0;
  for(CollisionDetector::CACHE_EEs::const_iterator beg=_cacheEE.begin(),end=_cacheEE.end(); beg!=end; beg++)
    writeVTK(*beg,off++);
}
void CollisionHandler::writeVTK(const Cache<TriangleRef,TriangleRef>& c,sizeType off) const
{
  const Vec3d vss[6]= {c._A.pos(0),c._A.pos(1),c._A.pos(2),c._B.pos(0),c._B.pos(1),c._B.pos(2)};
  {
    VTKWriter<scalarD> os("TT",_path+"/frmTT"+std::to_string(off)+".vtk",true);
    os.appendPoints(vss,vss+6);
    os.appendCells(VTKWriter<scalarD>::IteratorIndex<Vec3i>(0,3,0),VTKWriter<scalarD>::IteratorIndex<Vec3i>(2,3,0),VTKWriter<scalarD>::TRIANGLE);
  }
}
void CollisionHandler::writeVTK(const Cache<VertexRef,TriangleRef>& c,sizeType off) const
{
  const Vec3d vssF[4]= {c._A.posN(),c._B.posN(0),c._B.posN(1),c._B.posN(2)};
  const Vec3d vssT[4]= {c._A.pos (),c._B.pos (0),c._B.pos (1),c._B.pos (2)};
  const Vec3d vssC[4]= {interp1D<Vec3d,scalarD>(vssF[0],vssT[0],c._t),
                        interp1D<Vec3d,scalarD>(vssF[1],vssT[1],c._t),
                        interp1D<Vec3d,scalarD>(vssF[2],vssT[2],c._t),
                        interp1D<Vec3d,scalarD>(vssF[3],vssT[3],c._t)
                       };
  static const Vec3i pss[1]= {Vec3i(0,0,0)};
  static const Vec3i tss[1]= {Vec3i(1,2,3)};
  {
    Vec3d bary,cp;
    scalarD sqrDist;
    TriangleTpl<scalarD> t1(vssC[1],vssC[2],vssC[3]);
    t1.calcPointDist<Vec3d>(vssC[0],sqrDist,cp,bary);
    INFOV("VTError: %f",sqrDist)
  }
  {
    VTKWriter<scalarD> os("VT",_path+"/frmVTF"+std::to_string(off)+".vtk",true);
    os.appendPoints(vssF,vssF+4);
    os.appendCells(pss,pss+1,VTKWriter<scalarD>::POINT);
    os.appendCells(tss,tss+1,VTKWriter<scalarD>::TRIANGLE);
  }
  {
    VTKWriter<scalarD> os("VT",_path+"/frmVTT"+std::to_string(off)+".vtk",true);
    os.appendPoints(vssT,vssT+4);
    os.appendCells(pss,pss+1,VTKWriter<scalarD>::POINT);
    os.appendCells(tss,tss+1,VTKWriter<scalarD>::TRIANGLE);
  }
  {
    VTKWriter<scalarD> os("VT",_path+"/frmVTC"+std::to_string(off)+".vtk",true);
    os.appendPoints(vssC,vssC+4);
    os.appendCells(pss,pss+1,VTKWriter<scalarD>::POINT);
    os.appendCells(tss,tss+1,VTKWriter<scalarD>::TRIANGLE);
  }
}
void CollisionHandler::writeVTK(const Cache<EdgeRef,EdgeRef>& c,sizeType off) const
{
  const Vec3d vssF[4]= {c._A.posN(0),c._A.posN(1),c._B.posN(0),c._B.posN(1)};
  const Vec3d vssT[4]= {c._A.pos (0),c._A.pos (1),c._B.pos (0),c._B.pos (1)};
  const Vec3d vssC[4]= {interp1D<Vec3d,scalarD>(vssF[0],vssT[0],c._t),
                        interp1D<Vec3d,scalarD>(vssF[1],vssT[1],c._t),
                        interp1D<Vec3d,scalarD>(vssF[2],vssT[2],c._t),
                        interp1D<Vec3d,scalarD>(vssF[3],vssT[3],c._t)
                       };
  static const Vec2i ess[2]= {Vec2i(0,1),Vec2i(2,3)};
  {
    scalarD a,b,sqrDist;
    LineSegTpl<scalarD> l0(vssC[0],vssC[1]),l1(vssC[2],vssC[3]);
    l0.calcLineDist(l1,sqrDist,a,b);
    INFOV("EEError: %f",sqrDist)
  }
  {
    VTKWriter<scalarD> os("EE",_path+"/frmEEF"+std::to_string(off)+".vtk",true);
    os.appendPoints(vssF,vssF+4);
    os.appendCells(ess,ess+2,VTKWriter<scalarD>::LINE);
  }
  {
    VTKWriter<scalarD> os("EE",_path+"/frmEET"+std::to_string(off)+".vtk",true);
    os.appendPoints(vssT,vssT+4);
    os.appendCells(ess,ess+2,VTKWriter<scalarD>::LINE);
  }
  {
    VTKWriter<scalarD> os("EE",_path+"/frmEEC"+std::to_string(off)+".vtk",true);
    os.appendPoints(vssC,vssC+4);
    os.appendCells(ess,ess+2,VTKWriter<scalarD>::LINE);
  }
}
