#include "NRZCollisionSolver.h"
#include "Optimizer/QPInterface.h"

USE_PRJ_NAMESPACE

//NRZConstraint
NRZConstraint::NRZConstraint() {}
NRZConstraint::NRZConstraint(VertexRef vs[4],Vec3d coef[4],scalarD delta) {
  _vs[0]=vs[0];
  _vs[1]=vs[1];
  _vs[2]=vs[2];
  _vs[3]=vs[3];
  _coef[0]=coef[0];
  _coef[1]=coef[1];
  _coef[2]=coef[2];
  _coef[3]=coef[3];
  _delta=delta;
}
//NRZHandler
NRZHandler::NRZHandler(boost::shared_ptr<LinearSpringEnergy> energy,boost::shared_ptr<CollisionDetector> coll,const Cold& target,scalarD thickness)
  :_energy(energy),_coll(coll),_target(target),_thickness(thickness)
{
  if(_energy) {
    boost::shared_ptr<Mesh> cmesh=_coll->getMesh(0);
    boost::shared_ptr<NarrowNode> node=_coll->getNode(0);
    for(sizeType i=0; i<cmesh->nrVertex(); i++)
      _vids[VertexRef(node,i)]=i;
  }
}
void NRZHandler::handle(const Cache<VertexRef,TriangleRef>& VT) {
  VertexRef vs[4]= {VT._A,VT._B.id(0),VT._B.id(1),VT._B.id(2)};
  Vec3d coef[4]= {-VT._omg[0]*VT._n,-VT._omg[1]*VT._n,-VT._omg[2]*VT._n,-VT._omg[3]*VT._n};
  NRZConstraint cons(vs,coef,_thickness);
  _cons.push_back(cons);
  for(sizeType v=0; v<4; v++)
    _coll->activate(vs[v]);
  _more=true;
}
void NRZHandler::handle(const Cache<EdgeRef,EdgeRef>& EE) {
  VertexRef vs[4]= {EE._A.id(0),EE._A.id(1),EE._B.id(0),EE._B.id(1)};
  Vec3d coef[4]= {-EE._omg[0]*EE._n,-EE._omg[1]*EE._n,-EE._omg[2]*EE._n,-EE._omg[3]*EE._n};
  NRZConstraint cons(vs,coef,_thickness);
  _cons.push_back(cons);
  for(sizeType v=0; v<4; v++)
    _coll->activate(vs[v]);
  _more=true;
}
const std::vector<NRZConstraint>& NRZHandler::getCons() const {
  return _cons;
}
Eigen::Map<Vec3d> NRZHandler::pNonConst(const VertexRef& ref)
{
  ASSERT(!ref._node->_mesh->isFreeze())
  return Eigen::Map<Vec3d>(ref._node->_cache->_vss.data()+ref._id*3);
}
bool NRZHandler::solve() {
  if(!_more)
    return false;
  //reorder
  sizeType vid=(sizeType)_vids.size();
  for(sizeType i=0; i<(sizeType)_cons.size(); i++)
    for(sizeType v=0; v<4; v++)
      if(!_cons[i]._vs[v]._node->_mesh->isFreeze()) {
        boost::unordered_map<VertexRef,sizeType,CacheHash<VertexRef,VertexRef>>::const_iterator iter=_vids.find(_cons[i]._vs[v]);
        if(iter == _vids.end()) {
          _cons[i]._vid[v]=vid;
          _vids[_cons[i]._vs[v]]=vid++;
        } else _cons[i]._vid[v]=iter->second;
      } else _cons[i]._vid[v]=-1;
  //optimize zone position
  optimize();
  return true;
}
void NRZHandler::optimize() {
  //set constraint
  SMat CI;
  STrips trips;
  Vec CI0=Vec::Zero((sizeType)_cons.size());
  for(sizeType i=0; i<(sizeType)_cons.size(); i++) {
    CI0[i]=_cons[i]._delta;
    for(sizeType d=0; d<4; d++)
      if(_cons[i]._vid[d]>=0)
        addBlock(trips,i,_cons[i]._vid[d]*3,_cons[i]._coef[d].transpose());
      else CI0[i]+=_cons[i]._coef[d].dot(_cons[i]._vs[d].pos());
  }
  CI.resize((sizeType)_cons.size(),(sizeType)_vids.size()*3);
  CI.setFromTriplets(trips.begin(),trips.end());
  //set objective
  MeshCache& c=*(_coll->getNode(0)->_cache);
  c._E=0;
  c._ddEddVss.clear();
  c._dEdVss.setZero((sizeType)_vids.size()*3);
  Vec xNew=Vec::Zero((sizeType)_vids.size()*3);
  for(boost::unordered_map<VertexRef,sizeType,CacheHash<VertexRef,VertexRef>>::const_iterator beg=_vids.begin(),end=_vids.end(); beg!=end; beg++) {
    addBlockId(c._ddEddVss,beg->second*3,beg->second*3,3,1);
    c._dEdVss.segment<3>(beg->second*3)=-beg->first.pos();
    xNew.segment<3>(beg->second*3)=beg->first.pos();
  }
  if(_energy)
    (*_energy)[c];
  SMat HS;
  HS.resize((sizeType)_vids.size()*3,(sizeType)_vids.size()*3);
  HS.setFromTriplets(c._ddEddVss.begin(),c._ddEddVss.end());
  //solve
  bool succ;
  QPInterface qp;
  qp.setCI(CI,CI0,Coli::Constant(_cons.size(),-1));
  xNew=qp.solveSysQP(xNew,&HS,NULL,c._dEdVss,succ);
  //assign
  for(boost::unordered_map<VertexRef,sizeType,CacheHash<VertexRef,VertexRef>>::const_iterator beg=_vids.begin(),end=_vids.end(); beg!=end; beg++)
    pNonConst(beg->first)=xNew.segment<3>(beg->second*3);
}
//NRZCollisionSolver
NRZCollisionSolver::NRZCollisionSolver() {}
NRZCollisionSolver::NRZCollisionSolver(boost::shared_ptr<Mesh> mesh,boost::shared_ptr<MeshCache> cache,boost::shared_ptr<LinearSpringEnergy> energy):ImpulseCollisionSolver(mesh,cache),_energy(energy) {}
bool NRZCollisionSolver::write(std::ostream& os,IOData* dat) const
{
  ImpulseCollisionSolver::write(os,dat);
  writeBinaryData(_energy,os,dat);
  return os.good();
}
bool NRZCollisionSolver::read(std::istream& is,IOData* dat)
{
  ImpulseCollisionSolver::read(is,dat);
  readBinaryData(_energy,is,dat);
  return is.good();
}
boost::shared_ptr<SerializableBase> NRZCollisionSolver::copy() const
{
  return boost::shared_ptr<SerializableBase>(new NRZCollisionSolver);
}
std::string NRZCollisionSolver::type() const
{
  return typeid(NRZCollisionSolver).name();
}
bool NRZCollisionSolver::solve()
{
  Cold target=_cache->_vss;
  if(_energy)
    _energy->_vss=target;
  CollisionDetector::_thickness=_thicknessCCR;
  NRZHandler handler(_energy,_coll,target,_thicknessCCR);
  _coll->restartActive();
  for(sizeType i=0; true; i++) {
    handler._more=false;
    _coll->collide(handler,true,!_energy && i>0,false);
    if(handler._more) {
      if(i > 50)
        return errExit();
      handler.solve();
    } else break;
  }
  _coll->clearActive();
  return true;
}
