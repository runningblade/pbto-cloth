#include "Constraint.h"
#include "Optimizer/SparseUtils.h"
#include "Optimizer/DebugGradient.h"

USE_PRJ_NAMESPACE

//Constraint
Constraint::Constraint() {}
Constraint::Constraint(boost::shared_ptr<Mesh> mesh):_mesh(mesh) {}
bool Constraint::read(std::istream& is,IOData* dat) {
  readBinaryData(_mesh,is,dat);
  readBinaryData(_lb,is);
  readBinaryData(_ub,is);
  return is.good();
}
bool Constraint::write(std::ostream& os,IOData* dat) const {
  writeBinaryData(_mesh,os,dat);
  writeBinaryData(_lb,os);
  writeBinaryData(_ub,os);
  return os.good();
}
void Constraint::debugGradient(const std::string& name,scalarD scale,sizeType nrIter) const
{
  DEFINE_NUMERIC_DELTA_T(scalarD)
  for(sizeType i=0; i<nrIter; i++) {
    Vec dParam=Vec::Random(_mesh->nrParam());
    MeshCache c,c2;
    //cache
    c._param=_mesh->param0()+Vec::Random(_mesh->nrParam())*scale;
    _mesh->paramToVertex(c,true);
    c._C.resize(0);
    c._DCDParam.clear();
    operator()(c);

    //cache2
    c2=c;
    c2._param+=dParam*DELTA;
    _mesh->paramToVertex(c2,false);
    c2._C.resize(0);
    c2._DCDParam.clear();
    operator()(c2);
    DEBUG_GRADIENT(name.c_str(),(c.dCdParam()*dParam).norm(),(c.dCdParam()*dParam-(c2._C-c._C)/DELTA).norm())
  }
}
int Constraint::values() const
{
  return _mesh->nrEdge();
}
const Constraint::Vec& Constraint::lb() const
{
  return _lb;
}
const Constraint::Vec& Constraint::ub() const
{
  return _ub;
}
//LinearConstraint
LinearConstraint::LinearConstraint() {}
LinearConstraint::LinearConstraint(boost::shared_ptr<Mesh> mesh):Constraint(mesh) {}
//SpringConstraint
SpringConstraint::SpringConstraint() {}
SpringConstraint::SpringConstraint(boost::shared_ptr<Mesh> mesh,const Vec2d& lu):Constraint(mesh) {
  _lb.resize(values());
  _ub.resize(values());
  for(sizeType i=0; i<values(); i++) {
    const Vec2i e=_mesh->eid(i);
    _lb[i]=(_mesh->v0(e[0])-_mesh->v0(e[1])).norm()*lu[0];
    _ub[i]=(_mesh->v0(e[0])-_mesh->v0(e[1])).norm()*lu[1];
  }
}
bool SpringConstraint::read(std::istream& is,IOData* dat)
{
  Constraint::read(is,dat);
  return is.good();
}
bool SpringConstraint::write(std::ostream& os,IOData* dat) const
{
  Constraint::write(os,dat);
  return os.good();
}
boost::shared_ptr<SerializableBase> SpringConstraint::copy() const
{
  return boost::shared_ptr<SerializableBase>(new SpringConstraint);
}
std::string SpringConstraint::type() const
{
  return typeid(SpringConstraint).name();
}
void SpringConstraint::operator()(MeshCache& cache) const
{
  Vec C=Vec::Zero(_mesh->nrEdge());
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<_mesh->nrEdge(); i++) {
    const Vec2i e=_mesh->eid(i);
    const Vec3d e0=cache._vss.segment<3>(e[0]*3);
    const Vec3d e1=cache._vss.segment<3>(e[1]*3);
    C[i]=(e0-e1).norm();
    addBlock(cache._DCDParam,cache._C.size()+i,e[0]*3,(e0-e1).transpose()/C[i]);
    addBlock(cache._DCDParam,cache._C.size()+i,e[1]*3,(e1-e0).transpose()/C[i]);
  }
  cache._C=concat(cache._C,C);
}
void SpringConstraint::debugGradient(scalarD scale,sizeType nrIter) const
{
  Constraint::debugGradient("Spring",scale,nrIter);
}
int SpringConstraint::values() const
{
  return _mesh->nrEdge();
}
//BendConstraint
BendConstraint::BendConstraint() {}
BendConstraint::BendConstraint(boost::shared_ptr<Mesh> mesh,scalarD thres):Constraint(mesh) {
  _edges=BendEnergy(_mesh,1).edges();
  _lb.setConstant(values(),1-std::cos(thres));
  _ub.setConstant(values(),1+std::cos(thres));
}
bool BendConstraint::read(std::istream& is,IOData* dat)
{
  Constraint::read(is,dat);
  readBinaryData(_edges,is);
  return is.good();
}
bool BendConstraint::write(std::ostream& os,IOData* dat) const
{
  Constraint::write(os,dat);
  writeBinaryData(_edges,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> BendConstraint::copy() const
{
  return boost::shared_ptr<SerializableBase>(new BendConstraint);
}
std::string BendConstraint::type() const
{
  return typeid(BendConstraint).name();
}
void BendConstraint::operator()(MeshCache& cache) const
{
  Vec C=Vec::Zero(_edges.cols());
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<_edges.cols(); i++) {
    Vec3d ga,gb,gc,gd;
    const Vec4i e=_edges.col(i);
    scalarD theta=BendEnergy::thetaGradient(cache._vss.segment<3>(e[0]*3),
                                            cache._vss.segment<3>(e[1]*3),
                                            cache._vss.segment<3>(e[2]*3),
                                            cache._vss.segment<3>(e[3]*3),
                                            ga,gb,gc,gd);
    C[i]=theta;
    addBlock(cache._DCDParam,cache._C.size()+i,e[0]*3,ga.transpose());
    addBlock(cache._DCDParam,cache._C.size()+i,e[1]*3,gb.transpose());
    addBlock(cache._DCDParam,cache._C.size()+i,e[2]*3,gc.transpose());
    addBlock(cache._DCDParam,cache._C.size()+i,e[3]*3,gd.transpose());
  }
  cache._C=concat(cache._C,C);
}
void BendConstraint::debugGradient(scalarD scale,sizeType nrIter) const
{
  Constraint::debugGradient("Bend",scale,nrIter);
}
int BendConstraint::values() const
{
  return _edges.cols();
}
//RuleRibbonConstraint
RuleRibbonConstraint::RuleRibbonConstraint() {}
RuleRibbonConstraint::RuleRibbonConstraint(boost::shared_ptr<Mesh> mesh,scalarD reg):LinearConstraint(mesh) {
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
  _lb.setConstant(values(),-ribbon->segLen()/ribbon->width()*(1-reg));
  _ub.setConstant(values(), ribbon->segLen()/ribbon->width()*(1-reg));
}
bool RuleRibbonConstraint::read(std::istream& is,IOData* dat)
{
  Constraint::read(is,dat);
  return is.good();
}
bool RuleRibbonConstraint::write(std::ostream& os,IOData* dat) const
{
  Constraint::write(os,dat);
  return os.good();
}
boost::shared_ptr<SerializableBase> RuleRibbonConstraint::copy() const
{
  return boost::shared_ptr<SerializableBase>(new RuleRibbonConstraint);
}
std::string RuleRibbonConstraint::type() const
{
  return typeid(RuleRibbonConstraint).name();
}
void RuleRibbonConstraint::operator()(MeshCache& cache) const
{
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
  sizeType nrSeg=ribbon->nrSegment(),offC=cache._C.size();
  Vec C=Vec::Zero(nrSeg);
  for(sizeType i=0,off=6; i<nrSeg; i++,off+=2) {
    if(off<ribbon->nrParam()) {
      cache._DCDParam.push_back(STrip(offC+i,off,1));
      C[i]+=cache._param[off];
    }
    if(off>6) {
      cache._DCDParam.push_back(STrip(offC+i,off-2,-1));
      C[i]-=cache._param[off-2];
    }
  }
  cache._C=concat(cache._C,C);
}
void RuleRibbonConstraint::debugGradient(scalarD scale,sizeType nrIter) const
{
  Constraint::debugGradient("RuleRibbon",scale,nrIter);
}
int RuleRibbonConstraint::values() const
{
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
  return ribbon->nrSegment();
}
//BendRibbonConstraint
BendRibbonConstraint::BendRibbonConstraint() {}
BendRibbonConstraint::BendRibbonConstraint(boost::shared_ptr<Mesh> mesh,scalarD thres):LinearConstraint(mesh)
{
  _lb.setConstant(values(),-thres);
  _ub.setConstant(values(), thres);
}
bool BendRibbonConstraint::read(std::istream& is,IOData* dat)
{
  Constraint::read(is,dat);
  return is.good();
}
bool BendRibbonConstraint::write(std::ostream& os,IOData* dat) const
{
  Constraint::write(os,dat);
  return os.good();
}
boost::shared_ptr<SerializableBase> BendRibbonConstraint::copy() const
{
  return boost::shared_ptr<SerializableBase>(new BendRibbonConstraint);
}
std::string BendRibbonConstraint::type() const
{
  return typeid(BendRibbonConstraint).name();
}
void BendRibbonConstraint::operator()(MeshCache& cache) const
{
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
  sizeType nrSeg=ribbon->nrSegment(),offC=cache._C.size();
  Vec C=Vec::Zero(nrSeg-1);
  for(sizeType i=0,off=7; i<nrSeg-1; i++,off+=2) {
    cache._DCDParam.push_back(STrip(offC+i,off,1));
    C[i]+=cache._param[off];
  }
  cache._C=concat(cache._C,C);
}
void BendRibbonConstraint::debugGradient(scalarD scale,sizeType nrIter) const
{
  Constraint::debugGradient("BendRibbon",scale,nrIter);
}
int BendRibbonConstraint::values() const
{
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
  return ribbon->nrSegment()-1;
}
scalarD BendRibbonConstraint::lmt() const
{
  return _ub[0];
}
