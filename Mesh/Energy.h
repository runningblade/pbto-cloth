#ifndef ENERGY_H
#define ENERGY_H

#include "Mesh.h"
#include "Optimizer/SparseUtils.h"

PRJ_BEGIN

class Energy : public Objective<scalarD>, public SerializableBase
{
public:
  Energy();
  Energy(boost::shared_ptr<Mesh> mesh);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual void operator()(MeshCache& cache) const;  //no hessian, x=cache._vss
  virtual void operator[](MeshCache& cache) const;  //with hessian, x=0
  virtual void debugGradient(const std::string& name,scalarD scale,sizeType nrIter=10) const;
protected:
  static void atomicAdd(Vec& vss,sizeType off,const Vec3d& gV);
  boost::shared_ptr<Mesh> _mesh;
};
class SpringEnergy : public Energy
{
public:
  SpringEnergy();
  SpringEnergy(boost::shared_ptr<Mesh> mesh,scalarD coef);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
protected:
  Vec _l0;
  scalarD _coef;
};
class LinearSpringEnergy : public Energy
{
public:
  LinearSpringEnergy();
  LinearSpringEnergy(boost::shared_ptr<Mesh> mesh,scalarD coef);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator[](MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
  Vec _vss;
protected:
  scalarD _coef;
};
class StretchEnergy : public Energy
{
public:
  StretchEnergy();
  StretchEnergy(boost::shared_ptr<Mesh> mesh,scalarD Y,scalarD nu);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
protected:
  static scalarD energyGradient(const Vec3d& a,const Vec3d& b,const Vec3d& c,
                                const Mat3d& mbc,const Mat3d& mac,const Mat3d& mab,const Mat3d& m0,
                                scalarD alpha,scalarD beta,Vec3d& ga,Vec3d& gb,Vec3d& gc);
  static Vec3d distTo(const Vec3d& v,const Vec3d& a,const Vec3d& b);
  std::vector<Mat3d,Eigen::aligned_allocator<Mat3d>> _mbc,_mac,_mab,_m0;
  Vec _alpha,_beta;
};
class BendEnergy : public Energy
{
public:
  BendEnergy();
  BendEnergy(boost::shared_ptr<Mesh> mesh,scalarD coef);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
  const Mat4Xi& edges() const;
  static scalarD thetaGradient(const Vec3d& a,const Vec3d& b,const Vec3d& c,const Vec3d& d,
                               Vec3d& ga,Vec3d& gb,Vec3d& gc,Vec3d& gd,scalarD eps=0);
protected:
  Mat4Xi _edges;
  Vec _coef;
};
class BendRibbonEnergy : public Energy
{
public:
  BendRibbonEnergy();
  BendRibbonEnergy(boost::shared_ptr<Mesh> mesh,scalarD coef);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
protected:
  scalarD _coef;
};
class KinematicEnergy : public Energy
{
public:
  KinematicEnergy();
  KinematicEnergy(boost::shared_ptr<Mesh> mesh,scalarD rho);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
  scalarD dt() const;
protected:
  SMat _mass;
};
class KinematicRibbonEnergy : public Energy
{
public:
  struct InterpInfo {
    Vec3d interp(const Vec& vss) const;
    Vec3d dIdPhi(const Vec& vss) const;
    sizeType _i;
    scalarD _alpha,_dAlphadPhi;
    bool _bottom;
  };
  KinematicRibbonEnergy();
  KinematicRibbonEnergy(boost::shared_ptr<Mesh> mesh,scalarD rho);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
  scalarD dt() const;
protected:
  static void H(scalarD width,scalarD segLen,Mat4d& H,Mat4d& DHCoef);
  void searchForInterpInfo(const MeshCache& cache,std::vector<InterpInfo>& bNI,std::vector<InterpInfo>& tNI,std::vector<InterpInfo>& bNNI,std::vector<InterpInfo>& tNNI) const;
  void buildInterpInfo(const std::vector<scalarD>& targets,bool bottom,const Vec& param,std::vector<InterpInfo>& infos) const;
  Mat4d _H0,_DHCoef; //final H=_H0+_DHCoef*(phir-phil)
  Mat12d _H03,_DHCoef3;
};
class ConstantForceEnergy : public KinematicEnergy
{
public:
  ConstantForceEnergy();
  ConstantForceEnergy(boost::shared_ptr<Mesh> mesh,const Vec3d& f,scalarD rho);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
protected:
  Vec _f;
};
class ConstantForceRibbonEnergy : public KinematicRibbonEnergy
{
public:
  ConstantForceRibbonEnergy();
  ConstantForceRibbonEnergy(boost::shared_ptr<Mesh> mesh,const Vec3d& f,scalarD rho);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
protected:
  Vec12d _f;
};
class SoftPointConstraintEnergy : public Energy
{
public:
  SoftPointConstraintEnergy();
  SoftPointConstraintEnergy(boost::shared_ptr<Mesh> mesh,sizeType id,const Vec3d& b,scalarD coef);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
  scalarD _coef;
  SMat _H;
  Vec _b;
};
class SoftNormalConstraintEnergy : public Energy
{
public:
  typedef Eigen::Matrix<scalarD,3,9> Mat3X9d;
  SoftNormalConstraintEnergy();
  SoftNormalConstraintEnergy(boost::shared_ptr<Mesh> mesh,sizeType id,const Vec3d& b,scalarD coef);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
  static Vec3d crossDiff(const Vec3d& a,const Vec3d& b,const Vec3d& c,Mat3X9d& g,scalarD eps=1E-6f);
  scalarD _coef;
  SMat _H;
  Vec _b;
};

PRJ_END

#endif
