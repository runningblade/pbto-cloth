#ifndef COLLISION_DETECTOR_H
#define COLLISION_DETECTOR_H

#include "Mesh.h"
#include "NarrowNode.h"

PRJ_BEGIN

struct CollisionHandler;
template <typename TA,typename TB>
struct CacheHash;
template <typename TA,typename TB>
struct Cache : public SerializableBase
{
  typedef CacheHash<TA,TB> HashType;
  Cache();
  Cache(const TA& A,const TB& B);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  bool operator!=(const Cache<TA,TB>& other) const;
  bool operator==(const Cache<TA,TB>& other) const;
  bool operator<(const Cache<TA,TB>& other) const;
  scalarD _t;
  Vec4d _omg;
  Vec3d _n;
  TA _A;
  TB _B;
};
template <typename T>
struct Cache<T,T> : public SerializableBase
{
  typedef CacheHash<T,T> HashType;
  Cache();
  Cache(const T& A,const T& B);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  bool operator!=(const Cache<T,T>& other) const;
  bool operator==(const Cache<T,T>& other) const;
  bool operator<(const Cache<T,T>& other) const;
  scalarD _t;
  Vec4d _omg;
  Vec3d _n;
  T _A;
  T _B;
};
template <typename TA,typename TB>
struct CacheHash
{
  typedef MeshRef<VERTEX> VertexRef;
  typedef MeshRef<TRIANGLE> TriangleRef;
  typedef MeshRef<EDGE> EdgeRef;
  size_t operator()(const Cache<TA,TB>& c) const;
  size_t operator()(const boost::shared_ptr<NarrowNode>& c) const;
  size_t operator()(const VertexRef& ref) const;
  size_t operator()(const TriangleRef& ref) const;
  size_t operator()(const EdgeRef& ref) const;
  size_t operator()(sizeType c) const;
};
class CollisionDetector : public SerializableBase
{
public:
  typedef NarrowNode::BBOX BBOX;
  typedef MeshRef<VERTEX> VertexRef;
  typedef MeshRef<TRIANGLE> TriangleRef;
  typedef MeshRef<EDGE> EdgeRef;
  //cache
  typedef Cache<boost::shared_ptr<NarrowNode>,boost::shared_ptr<NarrowNode>> CACHE;
  typedef Cache<TriangleRef,TriangleRef> CACHE_TT;
  typedef Cache<VertexRef,TriangleRef> CACHE_VT;
  typedef Cache<EdgeRef,EdgeRef> CACHE_EE;
  //caches
  typedef boost::unordered_set<CACHE,CACHE::HashType> CACHEs;
  typedef boost::unordered_set<CACHE_TT,CACHE_TT::HashType> CACHE_TTs;
  typedef boost::unordered_set<CACHE_VT,CACHE_VT::HashType> CACHE_VTs;
  typedef boost::unordered_set<CACHE_EE,CACHE_EE::HashType> CACHE_EEs;
public:
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  sizeType nrMesh() const;
  boost::shared_ptr<Mesh> getMesh(sizeType i) const;
  boost::shared_ptr<NarrowNode> getNode(sizeType i) const;
  void updateMesh(boost::shared_ptr<Mesh> mesh);
  void addMesh(boost::shared_ptr<Mesh> mesh,boost::shared_ptr<MeshCache> cache);
  void delMesh(boost::shared_ptr<Mesh> mesh);
  void collide(CollisionHandler& handler,bool continuous,bool useActive,bool bruteForce,bool stopFirst=false);
  void collideBruteForce(CollisionHandler& handler,bool stopFirst=false);
  void collideFineGrained(CollisionHandler& handler,bool stopFirst=false);
  void activate(const VertexRef& v);
  void restartActive();
  void clearActive();
  void onCell(const Node<boost::shared_ptr<NarrowNode>,BBOX>& A,const Node<boost::shared_ptr<NarrowNode>,BBOX>& B);
  void onCell(const Node<sizeType,BBOX>& nA,const Node<sizeType,BBOX>& nB);
  void onCell(const EdgeRef& A,const EdgeRef& B);
  void onCell(const VertexRef& A,const TriangleRef& B);
  static void registerTypes(IOData* dat);
  //debug code
  void debugBroadphase(std::vector<boost::shared_ptr<Mesh>> meshes,scalarD scale,bool continuous);
  void debugBroadphaseActive(std::vector<boost::shared_ptr<Mesh>> meshes,scalarD scale,sizeType nrIter);
private:
  //data
  std::vector<Node<boost::shared_ptr<NarrowNode>,BBOX>> _bvh;
  CACHE_TTs _cacheTT;
  CACHE_VTs _cacheVT;
  CACHE_EEs _cacheEE;
  CACHE _activeCache;
  CACHEs _cache;
  bool _continuous;
public:
  //param
  static scalarD _staticDist;
  static scalarD _thickness;
  static scalarD _rounding;
  static scalarD _timeRes;
};

PRJ_END

#endif
