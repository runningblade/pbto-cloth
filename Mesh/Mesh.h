#ifndef MESH_H
#define MESH_H

#include "CommonFile/ObjMesh.h"
#include "CommonFile/solvers/Objective.h"

PRJ_BEGIN

struct MeshCache : public SerializableBase
{
  typedef Objective<scalarD>::Vec Vec;
  typedef Objective<scalarD>::SMat SMat;
  typedef Objective<scalarD>::STrips STrips;
public:
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  void saveN();
  void saveNN();
  void saveN2NN();
  SMat dCdParam() const;
  //data
  Vec _param,_paramN,_paramNN;
  Vec _vss,_vssN,_vssNN;
  //energy
  scalarD _E;
  Vec _dEdParam,_dEdVss;
  STrips _ddEddVss;
  //constraint
  Vec _C;
  STrips _DCDParam;
  //RibbonSpecific
  Vec3d _wR[3];
  std::vector<Mat3X4d,Eigen::aligned_allocator<Mat3X4d>> _RT,_DRT;
};
class Mesh : public Objective<scalarD>, public SerializableBase
{
public:
  Mesh();
  Mesh(const ObjMesh& mesh);
  Mesh(const Mat3Xi& iss,const Vec& vss);
  Mesh(const Mat2Xi& ess,const Vec& vss);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void paramToVertex(MeshCache& cache,bool makeValid=false) const;
  virtual void paramToVertexGradient(MeshCache& cache) const;
  virtual void debugGradient(sizeType nrIter=10) const;
  virtual sizeType nrEdge() const;
  virtual sizeType nrTriangle() const;
  virtual sizeType nrVertex() const;
  virtual sizeType nrParam() const;
  Vec2i eid(sizeType id) const;
  Vec3i tid(sizeType id) const;
  Vec3d v0(sizeType id) const;
  const Vec& param0() const;
  bool isFreeze() const;
  //IO
  void writeVTK(const MeshCache& cache,const std::string& path) const;
  void writeVTKParam(MeshCache cache,const std::string& path) const;
  //MeshSpecific
  void buildEdge();
  MeshCache randomParam(scalarD scale) const;
  static Mesh createSheet(const Vec2i& nrXY,scalarD len);
protected:
  Mat3Xi _tss;
  Mat2Xi _ess;
  Vec _param0;
  bool _isFreeze;
};
class MeshRibbon : public Mesh
{
public:
  MeshRibbon();
  MeshRibbon(scalarD width,scalarD segLen,sizeType nrSeg);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void paramToVertex(MeshCache& cache,bool makeValid=false) const override;
  virtual void paramToVertexGradient(MeshCache& cache) const override;
  virtual void debugGradient(sizeType nrIter=10) const override;
  virtual sizeType nrParam() const override;
  virtual sizeType nrSegment() const;
  scalarD width() const;
  scalarD segLen() const;
  //RibbonSpecific
  void projectParam(MeshCache& cache,scalarD reg=0.05f) const;
  MeshCache constantParam(scalarD R,scalarD T,scalarD phi,scalarD theta) const;
  MeshCache randomParam(scalarD R,scalarD T,scalarD phi,scalarD theta) const;
  static sizeType ID0(sizeType segId);
  static sizeType ID1(sizeType segId);
protected:
  static Mat3d rotY(scalarD phi,scalarD theta);
  static Mat3X4d dEVdParam(const Vec3d& dEdV,const Vec3d& v);
  static void rotYDiff(scalarD phi,scalarD theta,Mat3d& DRDPhi,Mat3d& DRDTheta);
  scalarD _width;
  scalarD _segLen;
};

PRJ_END

#endif
