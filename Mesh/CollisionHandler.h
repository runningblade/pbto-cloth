#ifndef COLLISION_HANDLER_H
#define COLLISION_HANDLER_H

#include "Mesh.h"
#include "CollisionDetector.h"

PRJ_BEGIN

struct CollisionHandler
{
  typedef CollisionDetector::VertexRef VertexRef;
  typedef CollisionDetector::TriangleRef TriangleRef;
  typedef CollisionDetector::EdgeRef EdgeRef;
  CollisionHandler();
  virtual ~CollisionHandler();
  virtual void handle(const Cache<TriangleRef,TriangleRef>& c);
  virtual void handle(const Cache<VertexRef,TriangleRef>& c);
  virtual void handle(const Cache<EdgeRef,EdgeRef>& c);
  bool operator==(const CollisionHandler& other) const;
  bool empty() const;
  //writeVTK
  void writeVTK(const std::string& path);
  void writeVTK(const Cache<TriangleRef,TriangleRef>& c,sizeType off) const;
  void writeVTK(const Cache<VertexRef,TriangleRef>& c,sizeType off) const;
  void writeVTK(const Cache<EdgeRef,EdgeRef>& c,sizeType off) const;
private:
  CollisionDetector::CACHE_TTs _cacheTT;
  CollisionDetector::CACHE_VTs _cacheVT;
  CollisionDetector::CACHE_EEs _cacheEE;
  std::string _path;
};

PRJ_END

#endif
