#include "NarrowNode.h"
#include "CollisionDetector.h"
#include "CommonFile/geom/BVHBuilder.h"
#include "Optimizer/Utils.h"

USE_PRJ_NAMESPACE

//NarrowNode
NarrowNode::NarrowNode() {}
void NarrowNode::buildBVH()
{
  sizeType nrT=(sizeType)_mesh->nrTriangle();
  _bvh.resize(nrT);
  for(sizeType i=0; i<nrT; i++) {
    _bvh[i]._nrCell=1;
    _bvh[i]._parent=-1;
    _bvh[i]._cell=i;
  }
  refit(false);
  BVHBuilder<Node<sizeType,BBOX>,3>().buildBVH(_bvh);
  for(sizeType i=nrT; i<(sizeType)_bvh.size(); i++)
    _bvh[i]._cell=-1;
  //refit();
  //writeBVHByLevel(_bvh,boost::shared_ptr<ClothMesh::ClothTriangle>());

  //build oneRing
  _oneRing.clear();
  for(sizeType i=0; i<nrT; i++)
    for(sizeType d=0; d<3; d++)
      _oneRing[_mesh->tid(i)[d]].insert(i);

  //build edge of triangle
  _edgeOfTri.clear();
  boost::unordered_map<Vec2i,boost::unordered_set<sizeType>,Hash> edgeToTri;
  for(sizeType i=0; i<nrT; i++)
    for(sizeType d=0; d<3; d++) {
      Vec2i teid(_mesh->tid(i)[d],_mesh->tid(i)[(d+1)%3]);
      if(teid[0]>teid[1])std::swap(teid[0],teid[1]);
      edgeToTri[teid].insert(i);
    }
  sizeType nrE=(sizeType)_mesh->nrEdge();
  for(sizeType i=0; i<nrE; i++) {
    Vec2i eid=_mesh->eid(i);
    if(eid[0]>eid[1])std::swap(eid[0],eid[1]);
    const boost::unordered_set<sizeType>& tris=edgeToTri[eid];
    for(boost::unordered_set<sizeType>::const_iterator beg=tris.begin(),end=tris.end(); beg!=end; beg++)
      _edgeOfTri[*beg].insert(i);
  }
  for(boost::unordered_map<sizeType,boost::unordered_set<sizeType>>::const_iterator
      beg=_edgeOfTri.begin(),end=_edgeOfTri.end(); beg!=end; beg++) {
    ASSERT((sizeType)beg->second.size()==3)
  }
}
NarrowNode::BBOX NarrowNode::refit(bool useLastPos)
{
  _vbb.resize(_mesh->nrVertex());
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)_mesh->nrVertex(); i++) {
    _vbb[i].reset();
    _vbb[i].setUnion(_cache->_vss.segment<3>(i*3).cast<scalar>());
    if(useLastPos)
      _vbb[i].setUnion(_cache->_vssN.segment<3>(i*3).cast<scalar>());
    _vbb[i].enlarged((scalar)CollisionDetector::_thickness);
  }
  _ebb.resize(_mesh->nrEdge());
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<(sizeType)_mesh->nrEdge(); i++) {
    _ebb[i].reset();
    _ebb[i].setUnion(_vbb[_mesh->eid(i)[0]]);
    _ebb[i].setUnion(_vbb[_mesh->eid(i)[1]]);
  }
  for(sizeType i=0; i<(sizeType)_bvh.size(); i++) {
    _bvh[i]._bb.reset();
    if(_bvh[i]._cell >= 0) {
      _bvh[i]._bb.setUnion(_vbb[_mesh->tid(i)[0]]);
      _bvh[i]._bb.setUnion(_vbb[_mesh->tid(i)[1]]);
      _bvh[i]._bb.setUnion(_vbb[_mesh->tid(i)[2]]);
    } else {
      _bvh[i]._bb.setUnion(_bvh[_bvh[i]._l]._bb);
      _bvh[i]._bb.setUnion(_bvh[_bvh[i]._r]._bb);
    }
  }
  if(!_active.empty()) {
    sizeType nrLeave=((sizeType)_bvh.size()+1)/2;
    ASSERT((sizeType)_active.size() == nrLeave || _active.size() == _bvh.size());
    _active.resize(_bvh.size());
    for(sizeType i=nrLeave; i<(sizeType)_bvh.size(); i++)
      _active[i]=_active[_bvh[i]._l]||_active[_bvh[i]._r];
  }
  if(_bvh.empty())
    return BBOX();
  else return _bvh.back()._bb;
}
bool NarrowNode::read(std::istream& is,IOData* dat)
{
  readBinaryData(_vbb,is);
  readBinaryData(_ebb,is);
  readBinaryData(_bvh,is);
  readBinaryData(_active,is);
  readBinaryData(_mesh,is,dat);
  readBinaryData(_cache,is,dat);
  readBinaryData(_oneRing,is);
  readBinaryData(_edgeOfTri,is);
  return is.good();
}
bool NarrowNode::write(std::ostream& os,IOData* dat) const
{
  writeBinaryData(_vbb,os);
  writeBinaryData(_ebb,os);
  writeBinaryData(_bvh,os);
  writeBinaryData(_active,os);
  writeBinaryData(_mesh,os,dat);
  writeBinaryData(_cache,os,dat);
  writeBinaryData(_oneRing,os);
  writeBinaryData(_edgeOfTri,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> NarrowNode::copy() const
{
  return boost::shared_ptr<SerializableBase>(new NarrowNode);
}
std::string NarrowNode::type() const
{
  return typeid(NarrowNode).name();
}
bool NarrowNode::operator<(const NarrowNode& other) const
{
  return _mesh<other._mesh;
}
//MeshRef<VERTEX>
MeshRef<VERTEX>::MeshRef() {}
MeshRef<VERTEX>::MeshRef(boost::shared_ptr<NarrowNode> node,sizeType id):_node(node),_id(id) {}
bool MeshRef<VERTEX>::read(std::istream& is,IOData* dat)
{
  readBinaryData(_node,is,dat);
  readBinaryData(_id,is);
  return is.good();
}
bool MeshRef<VERTEX>::write(std::ostream& os,IOData* dat) const
{
  writeBinaryData(_node,os,dat);
  writeBinaryData(_id,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> MeshRef<VERTEX>::copy() const
{
  return boost::shared_ptr<SerializableBase>(new MeshRef<VERTEX>);
}
std::string MeshRef<VERTEX>::type() const
{
  return typeid(MeshRef<VERTEX>).name();
}
Eigen::Map<const Vec3d> MeshRef<VERTEX>::posN() const
{
  return Eigen::Map<const Vec3d>(_node->_cache->_vssN.data()+_id*3);
}
Eigen::Map<const Vec3d> MeshRef<VERTEX>::pos() const
{
  return Eigen::Map<const Vec3d>(_node->_cache->_vss.data()+_id*3);
}
const NarrowNode::BBOX& MeshRef<VERTEX>::BBox() const
{
  return _node->_vbb[_id];
}
bool MeshRef<VERTEX>::operator!=(const MeshRef& other) const
{
  return _node!=other._node || _id!=other._id;
}
bool MeshRef<VERTEX>::operator==(const MeshRef& other) const
{
  return _node==other._node && _id==other._id;
}
bool MeshRef<VERTEX>::operator<(const MeshRef& other) const
{
  if(_node<other._node)
    return true;
  else if(other._node<_node)
    return false;
  if(_id<other._id)
    return true;
  else if(other._id<_id)
    return false;
  return false;
}
//MeshRef<TRIANGLE>
MeshRef<TRIANGLE>::MeshRef() {}
MeshRef<TRIANGLE>::MeshRef(boost::shared_ptr<NarrowNode> node,sizeType id):_node(node),_id(id) {}
bool MeshRef<TRIANGLE>::read(std::istream& is,IOData* dat)
{
  readBinaryData(_node,is,dat);
  readBinaryData(_id,is);
  return is.good();
}
bool MeshRef<TRIANGLE>::write(std::ostream& os,IOData* dat) const
{
  writeBinaryData(_node,os,dat);
  writeBinaryData(_id,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> MeshRef<TRIANGLE>::copy() const
{
  return boost::shared_ptr<SerializableBase>(new MeshRef<TRIANGLE>);
}
std::string MeshRef<TRIANGLE>::type() const
{
  return typeid(MeshRef<TRIANGLE>).name();
}
Eigen::Map<const Vec3d> MeshRef<TRIANGLE>::posN(sizeType vid) const
{
  return Eigen::Map<const Vec3d>(_node->_cache->_vssN.data()+_node->_mesh->tid(_id)[vid]*3);
}
Eigen::Map<const Vec3d> MeshRef<TRIANGLE>::pos(sizeType vid) const
{
  return Eigen::Map<const Vec3d>(_node->_cache->_vss.data()+_node->_mesh->tid(_id)[vid]*3);
}
MeshRef<VERTEX> MeshRef<TRIANGLE>::id(sizeType vid) const
{
  return MeshRef<VERTEX>(_node,_node->_mesh->tid(_id)[vid]);
}
const NarrowNode::BBOX& MeshRef<TRIANGLE>::BBox() const
{
  return _node->_bvh[_id]._bb;
}
bool MeshRef<TRIANGLE>::operator!=(const MeshRef& other) const
{
  return _node!=other._node || _id!=other._id;
}
bool MeshRef<TRIANGLE>::operator==(const MeshRef& other) const
{
  return _node==other._node && _id==other._id;
}
bool MeshRef<TRIANGLE>::operator<(const MeshRef& other) const
{
  if(_node<other._node)
    return true;
  else if(other._node<_node)
    return false;
  if(_id<other._id)
    return true;
  else if(other._id<_id)
    return false;
  return false;
}
//MeshRef<EDGE>
MeshRef<EDGE>::MeshRef() {}
MeshRef<EDGE>::MeshRef(boost::shared_ptr<NarrowNode> node,sizeType id):_node(node),_id(id) {}
bool MeshRef<EDGE>::read(std::istream& is,IOData* dat)
{
  readBinaryData(_node,is,dat);
  readBinaryData(_id,is);
  return is.good();
}
bool MeshRef<EDGE>::write(std::ostream& os,IOData* dat) const
{
  writeBinaryData(_node,os,dat);
  writeBinaryData(_id,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> MeshRef<EDGE>::copy() const
{
  return boost::shared_ptr<SerializableBase>(new MeshRef<EDGE>);
}
std::string MeshRef<EDGE>::type() const
{
  return typeid(MeshRef<EDGE>).name();
}
Eigen::Map<const Vec3d> MeshRef<EDGE>::posN(sizeType vid) const
{
  return Eigen::Map<const Vec3d>(_node->_cache->_vssN.data()+_node->_mesh->eid(_id)[vid]*3);
}
Eigen::Map<const Vec3d> MeshRef<EDGE>::pos(sizeType vid) const
{
  return Eigen::Map<const Vec3d>(_node->_cache->_vss.data()+_node->_mesh->eid(_id)[vid]*3);
}
MeshRef<VERTEX> MeshRef<EDGE>::id(sizeType vid) const
{
  return MeshRef<VERTEX>(_node,_node->_mesh->eid(_id)[vid]);
}
const NarrowNode::BBOX& MeshRef<EDGE>::BBox() const
{
  return _node->_ebb[_id];
}
bool MeshRef<EDGE>::operator!=(const MeshRef& other) const
{
  return _node!=other._node || _id!=other._id;
}
bool MeshRef<EDGE>::operator==(const MeshRef& other) const
{
  return _node==other._node && _id==other._id;
}
bool MeshRef<EDGE>::operator<(const MeshRef& other) const
{
  if(_node<other._node)
    return true;
  else if(other._node<_node)
    return false;
  if(_id<other._id)
    return true;
  else if(other._id<_id)
    return false;
  return false;
}
