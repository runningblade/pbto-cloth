#ifndef NRZ_COLLISION_SOLVER_H
#define NRZ_COLLISION_SOLVER_H

#include "ImpulseCollisionSolver.h"
#include "Energy.h"

PRJ_BEGIN

//non-rigid impact zone handler
struct NRZConstraint {
  typedef CollisionDetector::VertexRef VertexRef;
  NRZConstraint();
  NRZConstraint(VertexRef vs[4],Vec3d coef[4],scalarD delta);
  VertexRef _vs[4];
  scalarD _delta;
  Vec3d _coef[4];
  Vec4i _vid;
};
struct NRZHandler : public CollisionHandler, public Objective<scalarD> {
  NRZHandler(boost::shared_ptr<LinearSpringEnergy> energy,boost::shared_ptr<CollisionDetector> coll,const Cold& target,scalarD thickness);
  void handle(const Cache<VertexRef,TriangleRef>& VT) override;
  void handle(const Cache<EdgeRef,EdgeRef>& EE) override;
  const std::vector<NRZConstraint>& getCons() const;
  Eigen::Map<Vec3d> pNonConst(const VertexRef& ref);
  bool solve();
  void optimize();
  bool _more;
protected:
  boost::shared_ptr<LinearSpringEnergy> _energy;
  boost::shared_ptr<CollisionDetector> _coll;
  const Cold& _target;
  scalarD _thickness;
  //optimization data
  std::vector<NRZConstraint> _cons;
  boost::unordered_map<VertexRef,sizeType,CacheHash<VertexRef,VertexRef>> _vids;
};
class NRZCollisionSolver : public ImpulseCollisionSolver
{
public:
  NRZCollisionSolver();
  NRZCollisionSolver(boost::shared_ptr<Mesh> mesh,boost::shared_ptr<MeshCache> cache,boost::shared_ptr<LinearSpringEnergy> energy);
  bool write(std::ostream& os,IOData* dat) const override;
  bool read(std::istream& is,IOData* dat) override;
  boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual bool solve() override;
  boost::shared_ptr<LinearSpringEnergy> _energy;
};

PRJ_END

#endif
