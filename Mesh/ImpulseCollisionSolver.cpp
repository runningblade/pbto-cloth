#include "ImpulseCollisionSolver.h"

USE_PRJ_NAMESPACE

//ImpulseCollisionHandler
ImpulseCollisionHandler::ImpulseCollisionHandler(boost::shared_ptr<CollisionDetector> coll,scalarD mu,scalarD e,scalarD thickness):_coll(coll),_mu(mu),_e(e),_thickness(thickness) {}
void ImpulseCollisionHandler::handle(const Cache<VertexRef,TriangleRef>& VT)
{
  const VertexRef vs[4]= {VT._A,VT._B.id(0),VT._B.id(1),VT._B.id(2)};
  Vec3d dv=p(vs[0])*VT._omg[0]+p(vs[1])*VT._omg[1]+p(vs[2])*VT._omg[2]+p(vs[3])*VT._omg[3];
  if(_thickness >= dv.dot(VT._n))
    applyImpulse(vs,Vec4d(m(vs[0]),m(vs[1]),m(vs[2]),m(vs[3])),dv,VT._omg,VT._n);
  _more=true;
}
void ImpulseCollisionHandler::handle(const Cache<EdgeRef,EdgeRef>& EE)
{
  const VertexRef vs[4]= {EE._A.id(0),EE._A.id(1),EE._B.id(0),EE._B.id(1)};
  Vec3d dv=p(vs[0])*EE._omg[0]+p(vs[1])*EE._omg[1]+p(vs[2])*EE._omg[2]+p(vs[3])*EE._omg[3];
  if(_thickness >= dv.dot(EE._n))
    applyImpulse(vs,Vec4d(m(vs[0]),m(vs[1]),m(vs[2]),m(vs[3])),dv,EE._omg,EE._n);
  _more=true;
}
void ImpulseCollisionHandler::applyImpulse(const VertexRef vs[4],const Vec4d& m,const Vec3d& dv,const Vec4d& omg,const Vec3d& n)
{
  scalarD coeff=0.0f;
  for(char v=0; v<4; v++)
    coeff+=omg[v]*omg[v]/m[v];

  scalarD vn=_thickness-dv.dot(n);
  Vec3d dvt=dv-vn*n;
  scalarD In=vn/coeff*(1+_e);
  scalarD It=dvt.norm()/coeff;

  Vec3d I;
  if(It < In*_mu)
    I=In*n-dvt/coeff;
  else I=In*n-dvt*In*_mu/dvt.norm();
  for(sizeType v=0; v<4; v++) {
    _coll->activate(vs[v]);
    if(!vs[v]._node->_mesh->isFreeze())
      pNonConst(vs[v])+=I*omg[v]/m[v];
  }
}
Eigen::Map<Vec3d> ImpulseCollisionHandler::pNonConst(const VertexRef& ref)
{
  ASSERT(!ref._node->_mesh->isFreeze())
  return Eigen::Map<Vec3d>(_posNew.data()+ref._id*3);
}
Vec3d ImpulseCollisionHandler::p(const VertexRef& ref) const
{
  if(!ref._node->_mesh->isFreeze())
    return Eigen::Map<const Vec3d>(_posNew.data()+ref._id*3);
  else return ref.pos();
}
scalarD ImpulseCollisionHandler::m(const VertexRef& ref) const
{
  return ref._node->_mesh->isFreeze()?std::numeric_limits<scalarD>::infinity():1;
}
//ImpulseCollisionSolver
ImpulseCollisionSolver::ImpulseCollisionSolver() {}
ImpulseCollisionSolver::ImpulseCollisionSolver(boost::shared_ptr<Mesh> mesh,boost::shared_ptr<MeshCache> cache):_mesh(mesh),_cache(cache),_mu(0.75f),_e(0.1f)
{
  _thicknessCCR=1E-3f;
  _thicknessIZ=1E-4f;
  _coll.reset(new CollisionDetector);
  _coll->addMesh(_mesh,_cache);
  ASSERT(!_mesh->isFreeze())
}
bool ImpulseCollisionSolver::write(std::ostream& os,IOData* dat) const
{
  CollisionDetector::registerTypes(dat);
  writeBinaryData(_mesh,os,dat);
  writeBinaryData(_cache,os,dat);
  writeBinaryData(_coll,os,dat);
  writeBinaryData(_mu,os);
  writeBinaryData(_e,os);
  writeBinaryData(_thicknessCCR,os);
  writeBinaryData(_thicknessIZ,os);
  return os.good();
}
bool ImpulseCollisionSolver::read(std::istream& is,IOData* dat)
{
  CollisionDetector::registerTypes(dat);
  readBinaryData(_mesh,is,dat);
  readBinaryData(_cache,is,dat);
  readBinaryData(_coll,is,dat);
  readBinaryData(_mu,is);
  readBinaryData(_e,is);
  readBinaryData(_thicknessCCR,is);
  readBinaryData(_thicknessIZ,is);
  return is.good();
}
boost::shared_ptr<SerializableBase> ImpulseCollisionSolver::copy() const
{
  return boost::shared_ptr<SerializableBase>(new ImpulseCollisionSolver);
}
std::string ImpulseCollisionSolver::type() const
{
  return typeid(ImpulseCollisionSolver).name();
}
const CollisionDetector& ImpulseCollisionSolver::getColl() const
{
  return *_coll;
}
CollisionDetector& ImpulseCollisionSolver::getColl()
{
  return *_coll;
}
bool ImpulseCollisionSolver::solve()
{
  //handler for repulsive force
  ImpulseCollisionHandler impulser(_coll,_mu,_e,_thicknessCCR);
  impulser._more=false;
  //perform repulsive step
  CollisionDetector::_thickness=_thicknessCCR;
  impulser._posNew=_cache->_vss;
  _coll->collide(impulser,true,false,false);
  _cache->_vss=impulser._posNew;
  //apply collision and repulsion
  CollisionDetector::_thickness=_thicknessIZ;
  impulser._mu=0.0f;
  impulser._e=0.0f;
  impulser._more=true;
  _coll->restartActive();
  for(sizeType i=0; impulser._more; i++) {
    impulser._more=false;
    impulser._posNew=_cache->_vss;
    _coll->collide(impulser,true,i>0,false);
    _cache->_vss=impulser._posNew;
    if(i > 50)
      return errExit();
  }
  _coll->clearActive();
  return true;
}
bool ImpulseCollisionSolver::errExit()
{
  SerializableBase::write("errConfCloth.dat");
  WARNING("Collision Response Stuck!")
  return false;
}
