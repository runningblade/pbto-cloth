#include "Mesh.h"
#include "Constraint.h"
#include "RotationUtil.h"
#include "Optimizer/Utils.h"
#include "Optimizer/SparseUtils.h"
#include "Optimizer/QPInterface.h"
#include "Optimizer/DebugGradient.h"
#include "CommonFile/CameraModel.h"

USE_PRJ_NAMESPACE

//MeshCache
bool MeshCache::read(std::istream& is,IOData* dat)
{
  readBinaryData(_param,is);
  readBinaryData(_paramN,is);
  readBinaryData(_paramNN,is);
  readBinaryData(_vss,is);
  readBinaryData(_vssN,is);
  readBinaryData(_vssNN,is);

  //readBinaryData(_E,is);
  //readBinaryData(_dEdParam,is);
  //readBinaryData(_dEdVss,is);
  //readBinaryData(_ddEddVss,is);

  //readBinaryData(_C,is);
  //readBinaryData(_DCDParam,is);

  //readBinaryData(_wR[0],is);
  //readBinaryData(_wR[1],is);
  //readBinaryData(_wR[2],is);
  //readBinaryData(_RT,is);
  //readBinaryData(_DRT,is);
  return is.good();
}
bool MeshCache::write(std::ostream& os,IOData* dat) const
{
  writeBinaryData(_param,os);
  writeBinaryData(_paramN,os);
  writeBinaryData(_paramNN,os);
  writeBinaryData(_vss,os);
  writeBinaryData(_vssN,os);
  writeBinaryData(_vssNN,os);

  //writeBinaryData(_E,os);
  //writeBinaryData(_dEdParam,os);
  //writeBinaryData(_dEdVss,os);
  //writeBinaryData(_ddEddVss,os);

  //writeBinaryData(_C,os);
  //writeBinaryData(_DCDParam,os);

  //writeBinaryData(_wR[0],os);
  //writeBinaryData(_wR[1],os);
  //writeBinaryData(_wR[2],os);
  //writeBinaryData(_RT,os);
  //writeBinaryData(_DRT,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> MeshCache::copy() const
{
  return boost::shared_ptr<SerializableBase>(new MeshCache);
}
std::string MeshCache::type() const
{
  return typeid(MeshCache).name();
}
void MeshCache::saveN()
{
  _paramN=_param;
  _vssN=_vss;
}
void MeshCache::saveNN()
{
  _paramNN=_param;
  _vssNN=_vss;
}
void MeshCache::saveN2NN()
{
  _paramNN=_paramN;
  _vssNN=_vssN;
}
MeshCache::SMat MeshCache::dCdParam() const
{
  SMat ret;
  ret.resize(_C.size(),_param.size());
  ret.setFromTriplets(_DCDParam.begin(),_DCDParam.end());
  return ret;
}
//IteratorMesh
template <typename ID,typename IDOut>
struct IteratorMesh {
  typedef IDOut value_type;
  IteratorMesh(const ID& m,const Vec2i& id,const Vec2i& off):_m(m),_id(id),_off(off) {}
  void operator++() {
    _id+=_off;
  }
  bool operator!=(const IteratorMesh& other) const {
    return _id!=other._id;
  }
  virtual IDOut operator*() const {
    return _m.template block<IDOut::RowsAtCompileTime,IDOut::ColsAtCompileTime>(_id[0],_id[1]);
  }
  const ID& _m;
  Vec2i _id,_off;
};
//Mesh
Mesh::Mesh():_isFreeze(false) {}
Mesh::Mesh(const ObjMesh& mesh):_isFreeze(true)
{
  _tss.resize(3,(sizeType)mesh.getI().size());
  for(sizeType j=0;j<(sizeType)mesh.getI().size();j++)
    _tss.col(j)=mesh.getI()[j];
  buildEdge();
  _param0.resize((sizeType)mesh.getV().size()*3);
  for(sizeType i=0;i<(sizeType)mesh.getV().size();i++)
    _param0.segment<3>(i*3)=mesh.getV(i).cast<scalarD>();
}
Mesh::Mesh(const Mat3Xi& tss,const Vec& vss):_tss(tss),_param0(vss),_isFreeze(false)
{
  buildEdge();
}
Mesh::Mesh(const Mat2Xi& ess,const Vec& vss):_ess(ess),_param0(vss),_isFreeze(false) {}
bool Mesh::read(std::istream& is,IOData* dat)
{
  readBinaryData(_tss,is,dat);
  readBinaryData(_ess,is,dat);
  readBinaryData(_param0,is,dat);
  readBinaryData(_isFreeze,is);
  return is.good();
}
bool Mesh::write(std::ostream& os,IOData* dat) const
{
  writeBinaryData(_tss,os,dat);
  writeBinaryData(_ess,os,dat);
  writeBinaryData(_param0,os,dat);
  writeBinaryData(_isFreeze,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> Mesh::copy() const
{
  return boost::shared_ptr<SerializableBase>(new Mesh);
}
std::string Mesh::type() const
{
  return typeid(Mesh).name();
}
void Mesh::paramToVertex(MeshCache& cache,bool makeValid) const
{
  cache._vss=cache._param;
}
void Mesh::paramToVertexGradient(MeshCache& cache) const
{
  cache._dEdParam+=cache._dEdVss;
}
void Mesh::debugGradient(sizeType nrIter) const
{
  DEFINE_NUMERIC_DELTA_T(scalarD)
  for(sizeType i=0; i<nrIter; i++) {
    Vec dp=Vec::Random(nrParam());
    MeshCache c,c2;
    //cache
    c._param=Vec::Random(nrParam());
    paramToVertex(c);
    c._dEdVss=Vec::Random(nrVertex()*3);
    c._dEdParam.setZero(nrParam());
    paramToVertexGradient(c);
    //cache2
    c2._param=c._param+dp*DELTA;
    paramToVertex(c2);
    DEBUG_GRADIENT("ParamToVertex",dp.dot(c._dEdParam),dp.dot(c._dEdParam)-c._dEdVss.dot((c2._vss-c._vss)/DELTA))
  }
}
sizeType Mesh::nrEdge() const
{
  return _ess.cols();
}
sizeType Mesh::nrTriangle() const
{
  return _tss.cols();
}
sizeType Mesh::nrVertex() const
{
  sizeType ret=0;
  if(_tss.size()>0)
    ret=_tss.maxCoeff()+1;
  else if(_ess.size()>0)
    ret=_ess.maxCoeff()+1;
  return ret;
}
sizeType Mesh::nrParam() const
{
  return nrVertex()*3;
}
Vec2i Mesh::eid(sizeType id) const
{
  return _ess.col(id);
}
Vec3i Mesh::tid(sizeType id) const
{
  return _tss.col(id);
}
Vec3d Mesh::v0(sizeType id) const
{
  return _param0.segment<3>(id*3);
}
const Mesh::Vec& Mesh::param0() const
{
  return _param0;
}
bool Mesh::isFreeze() const
{
  return _isFreeze;
}
//IO
void Mesh::writeVTK(const MeshCache& cache,const std::string& path) const
{
  VTKWriter<scalarD> os("mesh",path,true);
  os.appendPoints(IteratorMesh<Vec,Vec3d>(cache._vss,Vec2i(0,0),Vec2i(3,0)),
                  IteratorMesh<Vec,Vec3d>(cache._vss,Vec2i(cache._vss.size(),0),Vec2i(3,0)));
  os.appendCells(IteratorMesh<Mat3Xi,Vec3i>(_tss,Vec2i(0,0),Vec2i(0,1)),
                 IteratorMesh<Mat3Xi,Vec3i>(_tss,Vec2i(0,nrTriangle()),Vec2i(0,1)),
                 VTKWriter<scalarD>::TRIANGLE);
  os.appendCells(IteratorMesh<Mat2Xi,Vec2i>(_ess,Vec2i(0,0),Vec2i(0,1)),
                 IteratorMesh<Mat2Xi,Vec2i>(_ess,Vec2i(0,nrEdge()),Vec2i(0,1)),
                 VTKWriter<scalarD>::LINE);
}
void Mesh::writeVTKParam(MeshCache cache,const std::string& path) const
{
  paramToVertex(cache);
  writeVTK(cache,path);
}
//MeshSpecific
void Mesh::buildEdge()
{
  Vec2i e;
  boost::unordered_set<Vec2i,Hash> ess;
  for(sizeType i=0; i<nrTriangle(); i++)
    for(sizeType d=0; d<3; d++) {
      e=Vec2i(_tss(d,i),_tss((d+1)%3,i));
      if(e[1]>e[0])
        std::swap(e[0],e[1]);
      ess.insert(e);
    }
  sizeType off=0;
  _ess.resize(2,ess.size());
  for(boost::unordered_set<Vec2i,Hash>::const_iterator beg=ess.begin(),end=ess.end(); beg!=end; beg++,off++)
    _ess.col(off)=*beg;
}
MeshCache Mesh::randomParam(scalarD scale) const
{
  MeshCache ret;
  ret._param=_param0+Vec::Random(_param0.size())*scale;
  return ret;
}
Mesh Mesh::createSheet(const Vec2i& nrXY,scalarD len)
{
#define GI(R,C) ((R)*(nrXY[1]+1)+(C))
  std::vector<Vec3i,Eigen::aligned_allocator<Vec3i>> tss;
  for(sizeType r=0; r<nrXY[0]; r++)
    for(sizeType c=0; c<nrXY[1]; c++) {
      tss.push_back(Vec3i(GI(r,c),GI(r+1,c),GI(r+1,c+1)));
      tss.push_back(Vec3i(GI(r,c),GI(r+1,c+1),GI(r,c+1)));
    }
  Mat3Xi tsss=Mat3Xi::Zero(3,(sizeType)tss.size());
  for(sizeType c=0; c<tsss.cols(); c++)
    tsss.col(c)=tss[c];
  Vec vss=Vec::Zero((nrXY[0]+1)*(nrXY[1]+1)*3);
  for(sizeType r=0; r<=nrXY[0]; r++)
    for(sizeType c=0; c<=nrXY[1]; c++)
      vss.segment<3>(GI(r,c)*3)=Vec3d(r,c,0)*len;
  return Mesh(tsss,vss);
#undef GI
}
//MeshRibbon
MeshRibbon::MeshRibbon() {}
MeshRibbon::MeshRibbon(scalarD width,scalarD segLen,sizeType nrSeg):_width(width),_segLen(segLen)
{
  _tss.resize(3,nrSeg*2);
  for(sizeType i=0; i<nrSeg; i++) {
    _tss.col(i*2+0)=Vec3i(ID0(i),ID0(i+1),ID1(i+1));
    _tss.col(i*2+1)=Vec3i(ID0(i),ID1(i+1),ID1(i));
  }
  buildEdge();
  _param0.setZero(nrParam());
}
bool MeshRibbon::read(std::istream& is,IOData* dat)
{
  Mesh::read(is,dat);
  readBinaryData(_width,is);
  readBinaryData(_segLen,is);
  return is.good();
}
bool MeshRibbon::write(std::ostream& os,IOData* dat) const
{
  Mesh::write(os,dat);
  writeBinaryData(_width,os);
  writeBinaryData(_segLen,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> MeshRibbon::copy() const
{
  return boost::shared_ptr<SerializableBase>(new MeshRibbon);
}
std::string MeshRibbon::type() const
{
  return typeid(MeshRibbon).name();
}
void MeshRibbon::paramToVertex(MeshCache& cache,bool makeValid) const
{
  if(makeValid)
    projectParam(cache);
  sizeType nrV=nrVertex(),nrSeg=nrV/2-1;
  cache._vss.resize(nrV*3);
  cache._RT.clear();
  cache._DRT.clear();
  Mat3X4d RT=Mat3X4d::Identity();
  Mat3X4d DRT=Mat3X4d::Identity();
  RT.block<3,1>(0,3)=cache._param.segment<3>(3);
  RT.block<3,3>(0,0)=expWGradV<scalarD,Vec3d>(cache._param.segment<3>(0),cache._wR);
  cache._vss.segment<3>(ID0(0)*3)=transformHomo<scalarD>(RT,Vec3d(0,-_width,0));
  cache._vss.segment<3>(ID1(0)*3)=transformHomo<scalarD>(RT,Vec3d(0, _width,0));
  for(sizeType i=1,off=6; i<=nrSeg; i++,off+=2) {
    scalarD phi=i==nrSeg?0:cache._param[off];
    cache._vss.segment<3>(ID0(i)*3)=transformHomo<scalarD>(RT,Vec3d(_segLen+phi*_width,-_width,0));
    cache._vss.segment<3>(ID1(i)*3)=transformHomo<scalarD>(RT,Vec3d(_segLen-phi*_width, _width,0));
    cache._RT.push_back(RT);
    if(i<nrSeg) {
      DRT.block<3,1>(0,3)=Vec3d(_segLen,0,0);
      DRT.block<3,3>(0,0)=rotY(phi,cache._param[off+1]);
      RT=transformMul<scalarD>(RT,DRT);
      cache._DRT.push_back(DRT);
    }
  }
}
void MeshRibbon::paramToVertexGradient(MeshCache& cache) const
{
  sizeType nrV=nrVertex(),nrSeg=nrV/2-1;
  Mat3X4d dEdRT=Mat3X4d::Zero();
  Mat3d DRDPhi,DRDTheta;
  for(sizeType i=nrSeg,off=nrParam(); i>=1; i--,off-=2) {
    scalarD phi=i==nrSeg?0:cache._param[off];
    if(i<nrSeg) {
      cache._dEdParam[off]+=cache._dEdVss.segment<3>(ID0(i)*3).dot(cache._RT[i-1].block<3,1>(0,0)* _width);
      cache._dEdParam[off]+=cache._dEdVss.segment<3>(ID1(i)*3).dot(cache._RT[i-1].block<3,1>(0,0)*-_width);
      rotYDiff(phi,cache._param[off+1],DRDPhi,DRDTheta);
      cache._dEdParam[off  ]+=(cache._RT[i-1].block<3,3>(0,0)*(DRDPhi  *dEdRT.block<3,3>(0,0).transpose())).trace();
      cache._dEdParam[off+1]+=(cache._RT[i-1].block<3,3>(0,0)*(DRDTheta*dEdRT.block<3,3>(0,0).transpose())).trace();
      dEdRT.block<3,3>(0,0)=(dEdRT*cache._DRT[i-1].transpose()).eval();
    }
    dEdRT+=dEVdParam(cache._dEdVss.segment<3>(ID0(i)*3),Vec3d(_segLen+phi*_width,-_width,0));
    dEdRT+=dEVdParam(cache._dEdVss.segment<3>(ID1(i)*3),Vec3d(_segLen-phi*_width, _width,0));
  }
  dEdRT+=dEVdParam(cache._dEdVss.segment<3>(ID0(0)*3),Vec3d(0,-_width,0));
  dEdRT+=dEVdParam(cache._dEdVss.segment<3>(ID1(0)*3),Vec3d(0, _width,0));
  Vec3d coefRdEdRTT=invCrossMatTrace<scalarD>(cache._RT[0].block<3,3>(0,0)*dEdRT.block<3,3>(0,0).transpose());
  cache._dEdParam.segment<3>(3)+=dEdRT.block<3,1>(0,3);
  for(sizeType d=0; d<3; d++)
    cache._dEdParam[d]+=coefRdEdRTT.dot(cache._wR[d]);
}
void MeshRibbon::debugGradient(sizeType nrIter) const
{
  DEFINE_NUMERIC_DELTA_T(scalarD)
  for(sizeType i=0; i<nrIter; i++) {
    Mat3d R,DRDPhi,DRDTheta;
    scalarD phi=RandEngine::randR(-1,1);
    scalarD theta=RandEngine::randR(-M_PI,M_PI);
    R=rotY(phi,theta);
    rotYDiff(phi,theta,DRDPhi,DRDTheta);
    DEBUG_GRADIENT("DRDPhi",DRDPhi.norm(),(DRDPhi-(rotY(phi+DELTA,theta)-R)/DELTA).norm())
    DEBUG_GRADIENT("DRDTheta",DRDTheta.norm(),(DRDTheta-(rotY(phi,theta+DELTA)-R)/DELTA).norm())
  }
  Mesh::debugGradient(nrIter);
}
sizeType MeshRibbon::nrParam() const
{
  sizeType nrV=nrVertex(),nrSeg=nrV/2-1;
  sizeType nrP=(nrSeg-1)*2+6;
  return nrP;
}
sizeType MeshRibbon::nrSegment() const
{
  sizeType nrV=nrVertex();
  return nrV/2-1;
}
scalarD MeshRibbon::width() const
{
  return _width;
}
scalarD MeshRibbon::segLen() const
{
  return _segLen;
}
//RibbonSpecific
void MeshRibbon::projectParam(MeshCache& cache,scalarD reg) const
{
  cache._C.resize(0);
  cache._DCDParam.clear();
  boost::shared_ptr<Mesh> m(new MeshRibbon(*this));
  RuleRibbonConstraint c(m,reg);
  c(cache);
  SMat CI=cache.dCdParam(),H;
  CI=concat<scalarD>(CI,-CI);
  Vec CI0=concat(-c.lb(),c.ub());

  bool succ;
  QPInterface qp;
  H.resize(nrParam(),nrParam());
  H.setIdentity();
  qp.setCI(CI,CI0,Coli::Constant(CI0.size(),1));
  cache._param=qp.solveSysQP(cache._param,&H,NULL,-cache._param,succ);
}
MeshCache MeshRibbon::constantParam(scalarD R,scalarD T,scalarD phi,scalarD theta) const
{
  MeshCache ret;
  sizeType nrV=nrVertex(),nrSeg=nrV/2-1;
  ret._param=Vec::Zero((nrSeg-1)*2+6);
  ret._param.segment<3>(0)=Vec3d::Random()*R;
  ret._param.segment<3>(3)=Vec3d::Random()*T;
  for(sizeType i=1,off=6; i<nrSeg; i++,off+=2) {
    ret._param[off+0]=phi;
    ret._param[off+1]=theta;
  }
  projectParam(ret);
  return ret;
}
MeshCache MeshRibbon::randomParam(scalarD R,scalarD T,scalarD phi,scalarD theta) const
{
  MeshCache ret;
  sizeType nrV=nrVertex(),nrSeg=nrV/2-1;
  ret._param=Vec::Zero((nrSeg-1)*2+6);
  ret._param.segment<3>(0)=Vec3d::Random()*R;
  ret._param.segment<3>(3)=Vec3d::Random()*T;
  for(sizeType i=1,off=6; i<nrSeg; i++,off+=2) {
    ret._param[off+0]=RandEngine::randR(-1,1)*phi;
    ret._param[off+1]=RandEngine::randR(-1,1)*theta;
  }
  projectParam(ret);
  return ret;
}
sizeType MeshRibbon::ID0(sizeType segId)
{
  return segId*2+0;
}
sizeType MeshRibbon::ID1(sizeType segId)
{
  return segId*2+1;
}
//helper
Mat3d MeshRibbon::rotY(scalarD phi,scalarD theta)
{
  //input
  Mat3d R;
  //scalarD phi;
  //scalarD theta;

  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;
  scalarD tt7;

  tt1=std::pow(phi,2);
  tt2=tt1+1;
  tt3=1/tt2;
  tt4=std::cos(theta);
  tt5=tt3*(phi*tt4-phi);
  tt6=1/sqrt(tt2);
  tt7=std::sin(theta);
  R(0,0)=tt3*(tt4+tt1);
  R(0,1)=tt5;
  R(0,2)=tt6*tt7;
  R(1,0)=tt5;
  R(1,1)=tt3*(tt1*tt4+1);
  R(1,2)=phi*tt6*tt7;
  R(2,0)=-tt6*tt7;
  R(2,1)=-phi*tt6*tt7;
  R(2,2)=tt4;
  return R;
}
void MeshRibbon::rotYDiff(scalarD phi,scalarD theta,Mat3d& DRDPhi,Mat3d& DRDTheta)
{
  //input
  //scalarD phi;
  //scalarD theta;

  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;
  scalarD tt7;
  scalarD tt8;
  scalarD tt9;
  scalarD tt10;
  scalarD tt11;
  scalarD tt12;

  tt1=std::pow(phi,2);
  tt2=1/(1+2*tt1+std::pow(phi,4));
  tt3=std::cos(theta);
  tt4=2*phi*tt3-2*phi;
  tt5=-tt2*(1-tt1+(tt1-1)*tt3);
  tt6=tt1+1;
  tt7=sqrt(tt6);
  tt8=1/std::pow(tt7,3);
  tt9=std::sin(theta);
  tt10=1/tt6;
  tt11=-phi*tt10*tt9;
  tt12=1/tt7;
  DRDPhi(0,0)=-tt2*tt4;
  DRDPhi(0,1)=tt5;
  DRDPhi(0,2)=-phi*tt8*tt9;
  DRDPhi(1,0)=tt5;
  DRDPhi(1,1)=tt2*tt4;
  DRDPhi(1,2)=tt7*tt2*tt9;
  DRDPhi(2,0)=phi*tt8*tt9;
  DRDPhi(2,1)=-tt7*tt2*tt9;
  DRDPhi(2,2)=0;
  DRDTheta(0,0)=-tt10*tt9;
  DRDTheta(0,1)=tt11;
  DRDTheta(0,2)=tt12*tt3;
  DRDTheta(1,0)=tt11;
  DRDTheta(1,1)=-tt1*tt10*tt9;
  DRDTheta(1,2)=phi*tt12*tt3;
  DRDTheta(2,0)=-tt12*tt3;
  DRDTheta(2,1)=-phi*tt12*tt3;
  DRDTheta(2,2)=-tt9;
}
Mat3X4d MeshRibbon::dEVdParam(const Vec3d& dEdV,const Vec3d& v)
{
  Mat3X4d ret;
  ret.block<3,3>(0,0)=dEdV*v.transpose();
  ret.block<3,1>(0,3)=dEdV;
  return ret;
}
