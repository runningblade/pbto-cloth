#ifndef CONSTRAINTS_H
#define CONSTRAINTS_H

#include "Energy.h"

PRJ_BEGIN

class Constraint : public Objective<scalarD>, public SerializableBase
{
public:
  Constraint();
  Constraint(boost::shared_ptr<Mesh> mesh);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual void operator()(MeshCache& cache) const=0;
  virtual void debugGradient(const std::string& name,scalarD scale,sizeType nrIter=10) const;
  virtual int values() const override;
  const Vec& lb() const;
  const Vec& ub() const;
protected:
  boost::shared_ptr<Mesh> _mesh;
  Vec _lb,_ub;
};
class LinearConstraint : public Constraint
{
public:
  LinearConstraint();
  LinearConstraint(boost::shared_ptr<Mesh> mesh);
};
class SpringConstraint : public Constraint
{
public:
  SpringConstraint();
  SpringConstraint(boost::shared_ptr<Mesh> mesh,const Vec2d& lu);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
  virtual int values() const override;
};
class BendConstraint : public Constraint
{
public:
  BendConstraint();
  BendConstraint(boost::shared_ptr<Mesh> mesh,scalarD thres);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
  virtual int values() const override;
public:
  Mat4Xi _edges;
};
class RuleRibbonConstraint : public LinearConstraint
{
public:
  RuleRibbonConstraint();
  RuleRibbonConstraint(boost::shared_ptr<Mesh> mesh,scalarD reg);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
  virtual int values() const override;
};
class BendRibbonConstraint : public LinearConstraint
{
public:
  BendRibbonConstraint();
  BendRibbonConstraint(boost::shared_ptr<Mesh> mesh,scalarD thres);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  virtual void operator()(MeshCache& cache) const override;
  virtual void debugGradient(scalarD scale,sizeType nrIter=10) const;
  virtual int values() const override;
  scalarD lmt() const;
};

PRJ_END

#endif
