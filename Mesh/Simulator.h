#ifndef SIMULATOR_H
#define SIMULATOR_H

#include "Mesh.h"
#include "Energy.h"
#include "Constraint.h"
#include "ImpulseCollisionSolver.h"
#include "NRZCollisionSolver.h"

PRJ_BEGIN

class Simulator : public Objective<scalarD>, public SerializableBase
{
public:
  enum COLL {
    IMPULSE,
    NRZ,
    NRZ_QP,
    NONE,
  };
  Simulator();
  Simulator(boost::shared_ptr<Mesh> mesh,COLL coll=NONE);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  void initRibbon(scalarD dt,scalarD bendCoef,const Vec3d& g,scalarD bendLmt,scalarD ruleReg);
  void initCloth(scalarD dt,scalarD bendCoef,scalarD stretchCoef,const Vec3d& g,scalarD bendLmt,scalarD stretchLmt);
  void addEnergy(boost::shared_ptr<Energy> e);
  void delEnergy(boost::shared_ptr<Energy> e);
  void addConstraint(boost::shared_ptr<Constraint> c);
  void addConstraint(boost::shared_ptr<LinearConstraint> c);
  void delConstraint(boost::shared_ptr<Constraint> c);
  template <typename T>
  boost::shared_ptr<T> getLinearConstraint() {
    for(sizeType i=0; i<(sizeType)_lcss.size(); i++)
      if(boost::dynamic_pointer_cast<T>(_lcss[i]))
        return boost::dynamic_pointer_cast<T>(_lcss[i]);
    return boost::shared_ptr<T>(NULL);
  }
  scalarD getDt() const;
  void fixVertex(sizeType id);
  void fix(sizeType id,scalarD val);
  void fix(sizeType beg,sizeType end,scalarD val);
  //simulator
  void advance(bool useCB);
  void advance(const std::string& path,scalarD t,bool useCB);
  const ImpulseCollisionSolver& getColl() const;
  ImpulseCollisionSolver& getColl();
  static void registerTypes(IOData* dat);
  //objective
  virtual int operator()(const Vec& x,scalarD& FX,Vec& DFDX,const scalarD& step,bool wantGradient) override;
  virtual int operator()(const Vec& x,Vec& fvec,STrips* fjac,bool modifiableOrSameX) override;
  virtual int inputs() const override;
  virtual int values() const override;
protected:
  void resetVariableBound();
  void resetLinearConstraint();
  void resetNonlinearConstraint();
  //data
  boost::shared_ptr<Mesh> _mesh;
  std::map<sizeType,scalarD> _fixes;
  std::vector<boost::shared_ptr<Energy>> _ess;
  std::vector<boost::shared_ptr<Constraint>> _css;
  std::vector<boost::shared_ptr<LinearConstraint>> _lcss;
  boost::shared_ptr<ImpulseCollisionSolver> _coll;
  boost::shared_ptr<MeshCache> _c;
  SMat _CI;
  Vec _CI0;
  Vec _NCI0;
  Vec _l,_u;
};

PRJ_END

#endif
