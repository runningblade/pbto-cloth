#include "Energy.h"
#include "RotationUtil.h"
#include "Optimizer/Utils.h"
#include "Optimizer/DebugGradient.h"
#include "CommonFile/CameraModel.h"

USE_PRJ_NAMESPACE

//Energy
Energy::Energy() {}
Energy::Energy(boost::shared_ptr<Mesh> mesh):_mesh(mesh) {}
bool Energy::read(std::istream& is,IOData* dat) {
  readBinaryData(_mesh,is,dat);
  return is.good();
}
bool Energy::write(std::ostream& os,IOData* dat) const {
  writeBinaryData(_mesh,os,dat);
  return os.good();
}
void Energy::operator()(MeshCache& cache) const {
  operator[](cache);
  SMat HS;
  HS.resize(cache._vss.size(),cache._vss.size());
  HS.setFromTriplets(cache._ddEddVss.begin(),cache._ddEddVss.end());
  cache._dEdVss+=HS*cache._vss;
}
void Energy::operator[](MeshCache& cache) const {
  FUNCTION_NOT_IMPLEMENTED
}
void Energy::debugGradient(const std::string& name,scalarD scale,sizeType nrIter) const {
  DEFINE_NUMERIC_DELTA_T(scalarD)
  for(sizeType i=0; i<nrIter; i++) {
    Vec dParam=Vec::Random(_mesh->nrParam());
    MeshCache c,c2;
    //cache
    c._param=_mesh->param0()+Vec::Random(_mesh->nrParam())*scale;
    _mesh->paramToVertex(c,true);
    c.saveNN();
    c._param=_mesh->param0()+Vec::Random(c._param.size())*scale;
    _mesh->paramToVertex(c,true);
    c.saveN();
    c._param=_mesh->param0()+Vec::Random(c._param.size())*scale;
    _mesh->paramToVertex(c,true);
    c._E=0;
    c._dEdVss.setZero(c._vss.size());
    c._dEdParam.setZero(_mesh->nrParam());
    operator()(c);
    _mesh->paramToVertexGradient(c);

    //cache2
    c2=c;
    c2._param+=dParam*DELTA;
    _mesh->paramToVertex(c2,false);
    c2._E=0;
    c2._dEdVss.setZero(c._vss.size());
    c2._dEdParam.setZero(_mesh->nrParam());
    operator()(c2);
    DEBUG_GRADIENT(name.c_str(),c._dEdParam.dot(dParam),c._dEdParam.dot(dParam)-(c2._E-c._E)/DELTA)
  }
}
void Energy::atomicAdd(Vec& vss,sizeType off,const Vec3d& gV) {
  for(sizeType d=0; d<3; d++) {
    scalarD& tmp=vss[off+d];
    scalarD gVd=gV[d];
    OMP_ATOMIC_
    tmp+=gVd;
  }
}
//SpringEnergy
SpringEnergy::SpringEnergy() {}
SpringEnergy::SpringEnergy(boost::shared_ptr<Mesh> mesh,scalarD coef):Energy(mesh),_coef(coef) {
  _l0.resize(_mesh->nrEdge());
  for(sizeType i=0; i<_l0.size(); i++) {
    const Vec2i e=_mesh->eid(i);
    _l0[i]=(_mesh->v0(e[0])-_mesh->v0(e[1])).norm();
  }
}
bool SpringEnergy::read(std::istream& is,IOData* dat)
{
  Energy::read(is,dat);
  readBinaryData(_l0,is);
  readBinaryData(_coef,is);
  return is.good();
}
bool SpringEnergy::write(std::ostream& os,IOData* dat) const
{
  Energy::write(os,dat);
  writeBinaryData(_l0,os);
  writeBinaryData(_coef,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> SpringEnergy::copy() const
{
  return boost::shared_ptr<SerializableBase>(new SpringEnergy);
}
std::string SpringEnergy::type() const
{
  return typeid(SpringEnergy).name();
}
void SpringEnergy::operator()(MeshCache& cache) const {
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<_l0.size(); i++) {
    const Vec2i e=_mesh->eid(i);
    const Vec3d e0=cache._vss.segment<3>(e[0]*3);
    const Vec3d e1=cache._vss.segment<3>(e[1]*3);
    scalarD len=(e0-e1).norm();
    scalarD E=std::pow(len-_l0[i],2)/2*_coef;
    OMP_ATOMIC_
    cache._E+=E;
    atomicAdd(cache._dEdVss,e[0]*3,(len-_l0[i])*(e0-e1)/std::max<scalarD>(len,1E-6f)*_coef);
    atomicAdd(cache._dEdVss,e[1]*3,(len-_l0[i])*(e1-e0)/std::max<scalarD>(len,1E-6f)*_coef);
  }
}
void SpringEnergy::debugGradient(scalarD scale,sizeType nrIter) const
{
  Energy::debugGradient("Spring",scale,nrIter);
}
//LinearSpringEnergy
LinearSpringEnergy::LinearSpringEnergy() {}
LinearSpringEnergy::LinearSpringEnergy(boost::shared_ptr<Mesh> mesh,scalarD coef):Energy(mesh),_coef(coef) {}
bool LinearSpringEnergy::read(std::istream& is,IOData* dat)
{
  Energy::read(is,dat);
  readBinaryData(_coef,is,dat);
  return is.good();
}
bool LinearSpringEnergy::write(std::ostream& os,IOData* dat) const
{
  Energy::write(os,dat);
  writeBinaryData(_coef,os,dat);
  return os.good();
}
boost::shared_ptr<SerializableBase> LinearSpringEnergy::copy() const
{
  return boost::shared_ptr<SerializableBase>(new LinearSpringEnergy);
}
std::string LinearSpringEnergy::type() const
{
  return typeid(LinearSpringEnergy).name();
}
void LinearSpringEnergy::operator[](MeshCache& cache) const
{
  sizeType nrE=_mesh->nrEdge();
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<nrE; i++) {
    const Vec2i e=_mesh->eid(i);
    Vec3d l01=_vss.segment<3>(e[0]*3)-_vss.segment<3>(e[1]*3);
    Vec3d dl01=cache._vss.segment<3>(e[0]*3)-cache._vss.segment<3>(e[1]*3)-l01;
    scalarD E=dl01.squaredNorm()*_coef/2;
    OMP_ATOMIC_
    cache._E+=E;
    addBlockId(cache._ddEddVss,e[0]*3,e[0]*3,3, _coef);
    addBlockId(cache._ddEddVss,e[1]*3,e[1]*3,3, _coef);
    addBlockId(cache._ddEddVss,e[1]*3,e[0]*3,3,-_coef);
    addBlockId(cache._ddEddVss,e[0]*3,e[1]*3,3,-_coef);
    atomicAdd(cache._dEdVss,e[0]*3,-l01*_coef);
    atomicAdd(cache._dEdVss,e[1]*3, l01*_coef);
  }
}
void LinearSpringEnergy::debugGradient(scalarD scale,sizeType nrIter) const
{
  MeshCache c;
  c._param=_mesh->param0();
  _mesh->paramToVertex(c);
  const_cast<Vec&>(_vss)=c._vss+Vec::Random(_mesh->nrVertex()*3)*scale;
  Energy::debugGradient("LinearSpring",scale,nrIter);
}
//StretchEnergy
StretchEnergy::StretchEnergy() {}
StretchEnergy::StretchEnergy(boost::shared_ptr<Mesh> mesh,scalarD Y,scalarD nu):Energy(mesh) {
  sizeType nrTri=_mesh->nrTriangle();
  _alpha.resize(nrTri);
  _beta.resize(nrTri);
  _mbc.resize(nrTri);
  _mac.resize(nrTri);
  _mab.resize(nrTri);
  _m0.resize(nrTri);
  for(sizeType i=0; i<nrTri; i++) {
    const Vec3i t=_mesh->tid(i);
    const Vec3d va=_mesh->v0(t[0]);
    const Vec3d vb=_mesh->v0(t[1]);
    const Vec3d vc=_mesh->v0(t[2]);
    scalarD A=(vb-va).cross(vc-va).norm()/2;
    scalarD lbc0Sqr=(vb-vc).squaredNorm();
    scalarD lac0Sqr=(va-vc).squaredNorm();
    scalarD lab0Sqr=(va-vb).squaredNorm();
    const Vec3d ta=distTo(va,vb,vc);
    const Vec3d tb=distTo(vb,va,vc);
    const Vec3d tc=distTo(vc,va,vb);
    _mbc[i]=-(tb*tc.transpose()+tc*tb.transpose())/(8*A*A);
    _mac[i]=-(ta*tc.transpose()+tc*ta.transpose())/(8*A*A);
    _mab[i]=-(ta*tb.transpose()+tb*ta.transpose())/(8*A*A);
    _m0[i]=-_mbc[i]*lbc0Sqr-_mac[i]*lac0Sqr-_mab[i]*lab0Sqr;
    _alpha[i]=A*Y/(2*(1+nu));
    _beta[i]=A*Y*nu/(2*(1+nu)*(1-2*nu));
  }
}
bool StretchEnergy::read(std::istream& is,IOData* dat)
{
  Energy::read(is,dat);
  readBinaryData(_mbc,is);
  readBinaryData(_mac,is);
  readBinaryData(_mab,is);
  readBinaryData(_m0,is);
  readBinaryData(_alpha,is);
  readBinaryData(_beta,is);
  return is.good();
}
bool StretchEnergy::write(std::ostream& os,IOData* dat) const
{
  Energy::write(os,dat);
  writeBinaryData(_mbc,os);
  writeBinaryData(_mac,os);
  writeBinaryData(_mab,os);
  writeBinaryData(_m0,os);
  writeBinaryData(_alpha,os);
  writeBinaryData(_beta,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> StretchEnergy::copy() const
{
  return boost::shared_ptr<SerializableBase>(new StretchEnergy);
}
std::string StretchEnergy::type() const
{
  return typeid(StretchEnergy).name();
}
void StretchEnergy::operator()(MeshCache& cache) const {
  sizeType nrTri=_mesh->nrTriangle();
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<nrTri; i++) {
    Vec3d ga,gb,gc;
    const Vec3i t=_mesh->tid(i);
    scalarD E=energyGradient(cache._vss.segment<3>(t[0]*3),
                             cache._vss.segment<3>(t[1]*3),
                             cache._vss.segment<3>(t[2]*3),
                             _mbc[i],_mac[i],_mab[i],_m0[i],_alpha[i],_beta[i],ga,gb,gc);
    OMP_ATOMIC_
    cache._E+=E;
    atomicAdd(cache._dEdVss,t[0]*3,ga);
    atomicAdd(cache._dEdVss,t[1]*3,gb);
    atomicAdd(cache._dEdVss,t[2]*3,gc);
  }
}
void StretchEnergy::debugGradient(scalarD scale,sizeType nrIter) const
{
  Energy::debugGradient("Stretch",scale,nrIter);
  sizeType nrTri=_mesh->nrTriangle();
  scalarD nErrAll=0,eErrAll=0,bcErrAll=0,acErrAll=0,abErrAll=0;
  for(sizeType i=0; i<nrTri; i++) {
    const Vec3i t=_mesh->tid(i);
    const Vec3d va=_mesh->v0(t[0]);
    const Vec3d vb=_mesh->v0(t[1]);
    const Vec3d vc=_mesh->v0(t[2]);
    const Vec3d n=(vb-va).cross(vc-va).normalized();
    Mat3X4d Rt=Mat3X4d::Random();
    const Vec3d va2=transformHomo<scalarD>(Rt,va);
    const Vec3d vb2=transformHomo<scalarD>(Rt,vb);
    const Vec3d vc2=transformHomo<scalarD>(Rt,vc);
    Vec3d ga,gb,gc;
    Mat3d E=_m0[i];
    E+=(vb2-vc2).squaredNorm()*_mbc[i];
    E+=(va2-vc2).squaredNorm()*_mac[i];
    E+=(va2-vb2).squaredNorm()*_mab[i];
    scalarD nErr=n.dot(E*n);
    scalarD eErr=_alpha[i]*E.squaredNorm()+_beta[i]*std::pow(E.trace(),2)-energyGradient(va2,vb2,vc2,_mbc[i],_mac[i],_mab[i],_m0[i],_alpha[i],_beta[i],ga,gb,gc);
    scalarD bcErr=(vb-vc).dot(E*(vb-vc))+(vb-vc).squaredNorm()-(vb2-vc2).squaredNorm();
    scalarD acErr=(va-vc).dot(E*(va-vc))+(va-vc).squaredNorm()-(va2-vc2).squaredNorm();
    scalarD abErr=(va-vb).dot(E*(va-vb))+(va-vb).squaredNorm()-(va2-vb2).squaredNorm();
    nErrAll+=std::abs(nErr);
    eErrAll+=std::abs(eErr);
    bcErrAll+=std::abs(bcErr);
    acErrAll+=std::abs(acErr);
    abErrAll+=std::abs(abErr);
  }
  INFOV("Stretch: nErr: %f eErr: %f BCErr: %f ACErr: %f ABErr: %f",nErrAll,eErrAll,bcErrAll,acErrAll,abErrAll)
}
scalarD StretchEnergy::energyGradient(const Vec3d& a,const Vec3d& b,const Vec3d& c,
                                      const Mat3d& mbc,const Mat3d& mac,const Mat3d& mab,const Mat3d& m0,
                                      scalarD alpha,scalarD beta,Vec3d& ga,Vec3d& gb,Vec3d& gc) {
  //input
  Mat3d E=m0;
  E+=(b-c).squaredNorm()*mbc;
  E+=(a-c).squaredNorm()*mac;
  E+=(a-b).squaredNorm()*mab;
  Mat3d dFdE=2*(alpha*E+beta*E.trace()*Mat3d::Identity());
  scalarD dFdEmbc=(mbc*dFdE.transpose()).trace();
  scalarD dFdEmac=(mac*dFdE.transpose()).trace();
  scalarD dFdEmab=(mab*dFdE.transpose()).trace();
  ga=2*((a-c)*dFdEmac+(a-b)*dFdEmab);
  gb=2*((b-c)*dFdEmbc+(b-a)*dFdEmab);
  gc=2*((c-a)*dFdEmac+(c-b)*dFdEmbc);
  return alpha*E.squaredNorm()+beta*std::pow(E.trace(),2);
}
Vec3d StretchEnergy::distTo(const Vec3d& v,const Vec3d& a,const Vec3d& b)
{
  Vec3d ab=(a-b).normalized();
  Vec3d ret=(a-v)-ab*ab.dot(a-v);
  return ret.normalized()*(a-b).norm();
}
//BendEnergy
BendEnergy::BendEnergy() {}
BendEnergy::BendEnergy(boost::shared_ptr<Mesh> mesh,scalarD coef):Energy(mesh)
{
  sizeType nrTri=_mesh->nrTriangle();
  boost::unordered_map<Vec2i,Vec2i,Hash> map;
  for(sizeType i=0; i<nrTri; i++) {
    Vec3i t=_mesh->tid(i);
    for(sizeType d=0; d<3; d++) {
      Vec2i e(t[d],t[(d+1)%3]);
      if(e[0]>e[1])
        std::swap(e[0],e[1]);
      if(map.find(e)==map.end())
        map[e]=Vec2i(t[(d+2)%3],-1);
      else map[e][1]=t[(d+2)%3];
    }
  }
  sizeType nrEdge=_mesh->nrEdge(),j=0;
  _coef.resize(nrEdge);
  _edges.resize(4,nrEdge);
  for(sizeType i=0; i<nrEdge; i++) {
    Vec2i e=_mesh->eid(i);
    if(e[0]>e[1])
      std::swap(e[0],e[1]);
    const Vec3d va=_mesh->v0(e[0]);
    const Vec3d vb=_mesh->v0(e[1]);
    ASSERT(map.find(e)!=map.end())
    if(map[e][1]>=0) {
      _coef[j]=(va-vb).norm()*coef;
      _edges.col(j)=concat<Coli>(e,map[e]);
      j++;
    }
  }
  _coef=_coef.segment(0,j).eval();
  _edges=_edges.block(0,0,4,j).eval();
}
bool BendEnergy::read(std::istream& is,IOData* dat)
{
  Energy::read(is,dat);
  readBinaryData(_edges,is);
  readBinaryData(_coef,is);
  return is.good();
}
bool BendEnergy::write(std::ostream& os,IOData* dat) const
{
  Energy::write(os,dat);
  writeBinaryData(_edges,os);
  writeBinaryData(_coef,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> BendEnergy::copy() const
{
  return boost::shared_ptr<SerializableBase>(new BendEnergy);
}
std::string BendEnergy::type() const
{
  return typeid(BendEnergy).name();
}
void BendEnergy::operator()(MeshCache& cache) const
{
  sizeType nrEdge=_edges.cols();
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<nrEdge; i++) {
    Vec3d ga,gb,gc,gd;
    const Vec4i e=_edges.col(i);
    scalarD theta=thetaGradient(cache._vss.segment<3>(e[0]*3),
                                cache._vss.segment<3>(e[1]*3),
                                cache._vss.segment<3>(e[2]*3),
                                cache._vss.segment<3>(e[3]*3),
                                ga,gb,gc,gd);
    scalarD E=std::pow(theta-1,2)*_coef[i];
    OMP_ATOMIC_
    cache._E+=E;
    scalarD coef=2*(theta-1)*_coef[i];
    atomicAdd(cache._dEdVss,e[0]*3,ga*coef);
    atomicAdd(cache._dEdVss,e[1]*3,gb*coef);
    atomicAdd(cache._dEdVss,e[2]*3,gc*coef);
    atomicAdd(cache._dEdVss,e[3]*3,gd*coef);
  }
}
void BendEnergy::debugGradient(scalarD scale,sizeType nrIter) const
{
  DEFINE_NUMERIC_DELTA_T(scalarD)
  Energy::debugGradient("Bend",scale,nrIter);
  for(sizeType i=0; i<nrIter; i++) {
    const Vec3d a=Vec3d::Random();
    const Vec3d b=Vec3d::Random();
    const Vec3d c=Vec3d::Random();
    const Vec3d d=Vec3d::Random();
    const Vec3d n1=(a-c).cross(b-c).normalized();
    const Vec3d n2=(b-d).cross(a-d).normalized();
    //compare
    Vec3d ga,gb,gc,gd;
    scalarD theta=std::acos(n1.dot(n2));
    scalarD theta2=thetaGradient(a,b,c,d,ga,gb,gc,gd,0);
    DEBUG_GRADIENT("BendTheta",theta,theta2-theta)
  }
}
const Mat4Xi& BendEnergy::edges() const
{
  return _edges;
}
scalarD BendEnergy::thetaGradient(const Vec3d& a,const Vec3d& b,const Vec3d& c,const Vec3d& d,
                                  Vec3d& ga,Vec3d& gb,Vec3d& gc,Vec3d& gd,scalarD eps)
{
  //see this note: https://people.eecs.berkeley.edu/~wkahan/MathH110/Cross.pdf//input
  //Vec3d a;
  //Vec3d b;
  //Vec3d c;
  //Vec3d d;
  //scalarD eps;

  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;
  scalarD tt7;
  scalarD tt8;
  scalarD tt9;
  scalarD tt10;
  scalarD tt11;
  scalarD tt12;
  scalarD tt13;
  scalarD tt14;
  scalarD tt15;
  scalarD tt16;
  scalarD tt17;
  scalarD tt18;
  scalarD tt19;
  scalarD tt20;
  scalarD tt21;
  scalarD tt22;
  scalarD tt23;
  scalarD tt24;
  scalarD tt25;
  scalarD tt26;
  scalarD tt27;
  scalarD tt28;
  scalarD tt29;
  scalarD tt30;
  scalarD tt31;
  scalarD tt32;
  scalarD tt33;
  scalarD tt34;
  scalarD tt35;
  scalarD tt36;
  scalarD tt37;
  scalarD tt38;
  scalarD tt39;
  scalarD tt40;
  scalarD tt41;
  scalarD tt42;
  scalarD tt43;
  scalarD tt44;
  scalarD tt45;
  scalarD tt46;
  scalarD tt47;
  scalarD tt48;
  scalarD tt49;
  scalarD tt50;
  scalarD tt51;
  scalarD tt52;
  scalarD tt53;
  scalarD tt54;
  scalarD tt55;

  tt1=-c[0];
  tt2=tt1+b[0];
  tt3=-c[1];
  tt4=tt3+a[1];
  tt5=tt1+a[0];
  tt6=tt3+b[1];
  tt7=tt5*tt6-tt2*tt4;
  tt8=-d[0];
  tt9=tt8+b[0];
  tt10=-d[1];
  tt11=tt10+a[1];
  tt12=tt8+a[0];
  tt13=tt10+b[1];
  tt14=tt9*tt11-tt12*tt13;
  tt15=-c[2];
  tt16=tt15+a[2];
  tt17=tt15+b[2];
  tt18=tt2*tt16-tt5*tt17;
  tt19=-d[2];
  tt20=tt19+a[2];
  tt21=tt19+b[2];
  tt22=tt12*tt21-tt9*tt20;
  tt23=tt4*tt17-tt6*tt16;
  tt24=tt13*tt20-tt11*tt21;
  tt25=tt23*tt24+tt18*tt22+tt7*tt14;
  tt26=std::sqrt(eps+std::pow(tt23,2)+std::pow(tt18,2)+std::pow(tt7,2));
  tt27=1/tt26;
  tt28=std::sqrt(eps+std::pow(tt24,2)+std::pow(tt22,2)+std::pow(tt14,2));
  tt29=1/tt28;
  tt30=-b[1];
  tt31=d[1]+tt30;
  tt32=1/std::pow(tt28,3);
  tt33=-b[2];
  tt34=c[2]+tt33;
  tt35=1/std::pow(tt26,3);
  tt36=d[2]+tt33;
  tt37=-b[0];
  tt38=c[0]+tt37;
  tt39=d[0]+tt37;
  tt40=c[1]+tt30;
  tt41=-a[2];
  tt42=d[2]+tt41;
  tt43=-a[1];
  tt44=c[1]+tt43;
  tt45=-a[0];
  tt46=d[0]+tt45;
  tt47=c[2]+tt41;
  tt48=d[1]+tt43;
  tt49=c[0]+tt45;
  tt50=tt30+a[1];
  tt51=b[2]+tt41;
  tt52=b[0]+tt45;
  tt53=tt33+a[2];
  tt54=tt37+a[0];
  tt55=b[1]+tt43;
  scalarD theta=tt25*tt27*tt29;
  ga[0]=(tt6*tt14+tt7*tt31+tt34*tt22+tt18*tt21)*tt27*tt29-((2*tt7*tt6+2*tt18*tt34)*tt25*tt35*tt29)/2.0-(tt25*(2*tt14*tt31+2*tt22*tt21)*tt27*tt32)/2.0;
  ga[1]=(tt9*tt7+tt38*tt14+tt17*tt24+tt23*tt36)*tt27*tt29-((2*tt38*tt7+2*tt23*tt17)*tt25*tt35*tt29)/2.0-(tt25*(2*tt9*tt14+2*tt24*tt36)*tt27*tt32)/2.0;
  ga[2]=(tt39*tt18+tt13*tt23+tt2*tt22+tt40*tt24)*tt27*tt29-((2*tt2*tt18+2*tt40*tt23)*tt25*tt35*tt29)/2.0-((2*tt39*tt22+2*tt13*tt24)*tt25*tt27*tt32)/2.0;
  gb[0]=(tt44*tt14+tt7*tt11+tt16*tt22+tt18*tt42)*tt27*tt29-((2*tt7*tt44+2*tt18*tt16)*tt25*tt35*tt29)/2.0-(tt25*(2*tt14*tt11+2*tt22*tt42)*tt27*tt32)/2.0;
  gb[1]=(tt46*tt7+tt5*tt14+tt47*tt24+tt23*tt20)*tt27*tt29-((2*tt5*tt7+2*tt23*tt47)*tt25*tt35*tt29)/2.0-(tt25*(2*tt46*tt14+2*tt24*tt20)*tt27*tt32)/2.0;
  gb[2]=(tt12*tt18+tt48*tt23+tt49*tt22+tt4*tt24)*tt27*tt29-((2*tt49*tt18+2*tt4*tt23)*tt25*tt35*tt29)/2.0-((2*tt12*tt22+2*tt48*tt24)*tt25*tt27*tt32)/2.0;
  gc[0]=(tt50*tt14+tt51*tt22)*tt27*tt29-((2*tt50*tt7+2*tt51*tt18)*tt25*tt35*tt29)/2.0;
  gc[1]=(tt52*tt14+tt53*tt24)*tt27*tt29-((2*tt52*tt7+2*tt53*tt23)*tt25*tt35*tt29)/2.0;
  gc[2]=(tt54*tt22+tt55*tt24)*tt27*tt29-((2*tt54*tt18+2*tt55*tt23)*tt25*tt35*tt29)/2.0;
  gd[0]=(tt55*tt7+tt53*tt18)*tt27*tt29-((2*tt55*tt14+2*tt53*tt22)*tt25*tt27*tt32)/2.0;
  gd[1]=(tt54*tt7+tt51*tt23)*tt27*tt29-((2*tt54*tt14+2*tt51*tt24)*tt25*tt27*tt32)/2.0;
  gd[2]=(tt52*tt18+tt50*tt23)*tt27*tt29-((2*tt52*tt22+2*tt50*tt24)*tt25*tt27*tt32)/2.0;
  return theta;
}
//BendRibbonEnergy
BendRibbonEnergy::BendRibbonEnergy() {}
BendRibbonEnergy::BendRibbonEnergy(boost::shared_ptr<Mesh> mesh,scalarD coef):Energy(mesh),_coef(coef) {}
bool BendRibbonEnergy::read(std::istream& is,IOData* dat)
{
  Energy::read(is,dat);
  readBinaryData(_coef,is);
  return is.good();
}
bool BendRibbonEnergy::write(std::ostream& os,IOData* dat) const
{
  Energy::write(os,dat);
  writeBinaryData(_coef,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> BendRibbonEnergy::copy() const
{
  return boost::shared_ptr<SerializableBase>(new BendRibbonEnergy);
}
std::string BendRibbonEnergy::type() const
{
  return typeid(BendRibbonEnergy).name();
}
void BendRibbonEnergy::operator()(MeshCache& cache) const {
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<ribbon->nrSegment()-1; i++) {
    scalarD phi=cache._param[6+i*2];
    scalarD theta=cache._param[7+i*2];
    scalarD edgeLen=std::sqrt(phi*phi+ribbon->width()*ribbon->width());
    scalarD E=edgeLen*theta*theta*_coef;
    OMP_ATOMIC_
    cache._E+=E;
    cache._dEdParam[6+i*2]+=phi/edgeLen*theta*theta*_coef;
    cache._dEdParam[7+i*2]+=2*edgeLen*theta*_coef;
  }
}
void BendRibbonEnergy::debugGradient(scalarD scale,sizeType nrIter) const
{
  Energy::debugGradient("BendRibbon",scale,nrIter);
}
//KinematicEnergy
KinematicEnergy::KinematicEnergy() {}
KinematicEnergy::KinematicEnergy(boost::shared_ptr<Mesh> mesh,scalarD rho):Energy(mesh) {
  STrips trips;
  sizeType nrVert=_mesh->nrVertex();
  sizeType nrTri=_mesh->nrTriangle();
  Mat3d coef=Mat3d::Ones()/12;
  coef.diagonal()*=2;
  for(sizeType i=0; i<nrTri; i++) {
    const Vec3i t=_mesh->tid(i);
    const Vec3d va=_mesh->v0(t[0]);
    const Vec3d vb=_mesh->v0(t[1]);
    const Vec3d vc=_mesh->v0(t[2]);
    scalarD A=(vb-va).cross(vc-va).norm()/2;
    for(sizeType r=0; r<3; r++)
      for(sizeType c=0; c<3; c++)
        addBlockId(trips,t[r]*3,t[c]*3,3,coef(r,c)*A*rho);
  }
  _mass.resize(nrVert*3,nrVert*3);
  _mass.setFromTriplets(trips.begin(),trips.end());
}
bool KinematicEnergy::read(std::istream& is,IOData* dat)
{
  Energy::read(is,dat);
  readBinaryData(_mass,is);
  return is.good();
}
bool KinematicEnergy::write(std::ostream& os,IOData* dat) const
{
  Energy::write(os,dat);
  writeBinaryData(_mass,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> KinematicEnergy::copy() const
{
  return boost::shared_ptr<SerializableBase>(new KinematicEnergy);
}
std::string KinematicEnergy::type() const
{
  return typeid(KinematicEnergy).name();
}
void KinematicEnergy::operator()(MeshCache& cache) const {
  Vec A=cache._vss-cache._vssN*2+cache._vssNN;
  Vec g=_mass*A;
  cache._dEdVss+=g;
  cache._E+=A.dot(g)/2;
}
void KinematicEnergy::debugGradient(scalarD scale,sizeType nrIter) const
{
  Energy::debugGradient("Kinematic",scale,nrIter);
  Vec vss=Vec::Constant(_mesh->nrVertex()*3,1);
  scalarD E=vss.dot(_mass*vss)/2,E2=0;
  sizeType nrTri=_mesh->nrTriangle();
  for(sizeType i=0; i<nrTri; i++) {
    const Vec3i t=_mesh->tid(i);
    const Vec3d va=_mesh->v0(t[0]);
    const Vec3d vb=_mesh->v0(t[1]);
    const Vec3d vc=_mesh->v0(t[2]);
    E2+=1.5f*(vb-va).cross(vc-va).norm()/2;
  }
  INFOV("Rho: %f",E/E2)
}
scalarD KinematicEnergy::dt() const
{
  scalarD sumA=0;
  sizeType nrVert=_mesh->nrVertex();
  sizeType nrTri=_mesh->nrTriangle();
  for(sizeType i=0; i<nrTri; i++) {
    const Vec3i t=_mesh->tid(i);
    const Vec3d va=_mesh->v0(t[0]);
    const Vec3d vb=_mesh->v0(t[1]);
    const Vec3d vc=_mesh->v0(t[2]);
    sumA+=(vb-va).cross(vc-va).norm()/2;
  }
  Vec ones=Vec::Ones(nrVert*3);
  scalarD coef=ones.dot(_mass*ones)/(3*sumA);
  return std::sqrt(1/coef);
}
//KinematicRibbonEnergy
Vec3d KinematicRibbonEnergy::InterpInfo::interp(const Vec& vss) const {
  sizeType id0=_bottom?MeshRibbon::ID0(_i):MeshRibbon::ID1(_i);
  sizeType id1=_bottom?MeshRibbon::ID0(_i+1):MeshRibbon::ID1(_i+1);
  return vss.segment<3>(id0*3)*(1-_alpha)+vss.segment<3>(id1*3)*_alpha;
}
Vec3d KinematicRibbonEnergy::InterpInfo::dIdPhi(const Vec& vss) const {
  sizeType id0=_bottom?MeshRibbon::ID0(_i):MeshRibbon::ID1(_i);
  sizeType id1=_bottom?MeshRibbon::ID0(_i+1):MeshRibbon::ID1(_i+1);
  return (vss.segment<3>(id1*3)-vss.segment<3>(id0*3))*_dAlphadPhi;
}
KinematicRibbonEnergy::KinematicRibbonEnergy() {}
KinematicRibbonEnergy::KinematicRibbonEnergy(boost::shared_ptr<Mesh> mesh,scalarD rho):Energy(mesh) {
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
  H(ribbon->width(),ribbon->segLen(),_H0,_DHCoef);
  _H03=kronecker(_H0,3)*rho;
  _DHCoef3=kronecker(_DHCoef,3)*rho;
}
bool KinematicRibbonEnergy::read(std::istream& is,IOData* dat)
{
  Energy::read(is,dat);
  readBinaryData(_H0,is);
  readBinaryData(_DHCoef,is);
  readBinaryData(_H03,is);
  readBinaryData(_DHCoef3,is);
  return is.good();
}
bool KinematicRibbonEnergy::write(std::ostream& os,IOData* dat) const
{
  Energy::write(os,dat);
  writeBinaryData(_H0,os);
  writeBinaryData(_DHCoef,os);
  writeBinaryData(_H03,os);
  writeBinaryData(_DHCoef3,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> KinematicRibbonEnergy::copy() const
{
  return boost::shared_ptr<SerializableBase>(new KinematicRibbonEnergy);
}
std::string KinematicRibbonEnergy::type() const
{
  return typeid(KinematicRibbonEnergy).name();
}
void KinematicRibbonEnergy::operator()(MeshCache& cache) const
{
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
  sizeType nrSegment=ribbon->nrSegment();
  std::vector<InterpInfo> bNI,tNI,bNNI,tNNI;
  searchForInterpInfo(cache,bNI,tNI,bNNI,tNNI);
  //build energy
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<nrSegment; i++) {
    scalarD phil=i==0?0:cache._param[4+i*2];
    scalarD phir=i==nrSegment-1?0:cache._param[6+i*2];
    //[xbl,xbr,xtl,xtr]
    Mat12d H=_H03+_DHCoef3*(phir-phil);
    Vec12d x,dxdl=Vec12d::Zero(),dxdr=Vec12d::Zero();
    x.segment<3>(0)=cache._vss.segment<3>(MeshRibbon::ID0(i)*3)-bNI[i].interp(cache._vssN)*2+bNNI[i].interp(cache._vssNN);
    x.segment<3>(3)=cache._vss.segment<3>(MeshRibbon::ID0(i+1)*3)-bNI[i+1].interp(cache._vssN)*2+bNNI[i+1].interp(cache._vssNN);
    x.segment<3>(6)=cache._vss.segment<3>(MeshRibbon::ID1(i)*3)-tNI[i].interp(cache._vssN)*2+tNNI[i].interp(cache._vssNN);
    x.segment<3>(9)=cache._vss.segment<3>(MeshRibbon::ID1(i+1)*3)-tNI[i+1].interp(cache._vssN)*2+tNNI[i+1].interp(cache._vssNN);
    dxdl.segment<3>(0)=-bNI[i].dIdPhi(cache._vssN)*2+bNNI[i].dIdPhi(cache._vssNN);
    dxdr.segment<3>(3)=-bNI[i+1].dIdPhi(cache._vssN)*2+bNNI[i+1].dIdPhi(cache._vssNN);
    dxdl.segment<3>(6)=-tNI[i].dIdPhi(cache._vssN)*2+tNNI[i].dIdPhi(cache._vssNN);
    dxdr.segment<3>(9)=-tNI[i+1].dIdPhi(cache._vssN)*2+tNNI[i+1].dIdPhi(cache._vssNN);
    Vec12d g=H*x;
    scalarD E=x.dot(g)/2;
    OMP_ATOMIC_
    cache._E+=E;
    if(i>0) {
      scalarD dEdPhil=dxdl.dot(g)-x.dot(_DHCoef3*x)/2;
      OMP_ATOMIC_
      cache._dEdParam[4+i*2]+=dEdPhil;
    }
    if(i<nrSegment-1) {
      scalarD dEdPhir=dxdr.dot(g)+x.dot(_DHCoef3*x)/2;
      OMP_ATOMIC_
      cache._dEdParam[6+i*2]+=dEdPhir;
    }
    atomicAdd(cache._dEdVss,MeshRibbon::ID0(i)*3,g.segment<3>(0));
    atomicAdd(cache._dEdVss,MeshRibbon::ID0(i+1)*3,g.segment<3>(3));
    atomicAdd(cache._dEdVss,MeshRibbon::ID1(i)*3,g.segment<3>(6));
    atomicAdd(cache._dEdVss,MeshRibbon::ID1(i+1)*3,g.segment<3>(9));
  }
}
void KinematicRibbonEnergy::debugGradient(scalarD scale,sizeType nrIter) const
{
  DEFINE_NUMERIC_DELTA_T(scalarD)
  Energy::debugGradient("KinematicRibbon",scale,nrIter);

  //interpolation
  for(sizeType i=0; i<nrIter; i++) {
    boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
    sizeType nrSegment=ribbon->nrSegment();
    MeshCache c;
    c._param=_mesh->param0();
    for(sizeType i=1; i<nrSegment; i++)
      c._param[4+i*2]+=RandEngine::randR(-1,1)*scale;
    _mesh->paramToVertex(c,true);
    c.saveNN();
    c._param=_mesh->param0();
    for(sizeType i=1; i<nrSegment; i++)
      c._param[4+i*2]+=RandEngine::randR(-1,1)*scale;
    _mesh->paramToVertex(c,true);
    c.saveN();
    c._param=_mesh->param0();
    for(sizeType i=1; i<nrSegment; i++)
      c._param[4+i*2]+=RandEngine::randR(-1,1)*scale;
    _mesh->paramToVertex(c,true);
    scalarD errAll=0;
    std::vector<InterpInfo> bNI,tNI,bNNI,tNNI;
    searchForInterpInfo(c,bNI,tNI,bNNI,tNNI);
    for(sizeType i=0; i<=nrSegment; i++) {
      errAll+=(bNI[i].interp(c._vssN)-c._vss.segment<3>(MeshRibbon::ID0(i)*3)).norm();
      errAll+=(bNNI[i].interp(c._vssNN)-c._vss.segment<3>(MeshRibbon::ID0(i)*3)).norm();
      errAll+=(tNI[i].interp(c._vssN)-c._vss.segment<3>(MeshRibbon::ID1(i)*3)).norm();
      errAll+=(tNNI[i].interp(c._vssNN)-c._vss.segment<3>(MeshRibbon::ID1(i)*3)).norm();
    }
    INFOV("Interpolation err: %f!",errAll)
  }

  //interpolation gradient
  {
    boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
    sizeType nrSegment=ribbon->nrSegment();
    Vec dParam=Vec::Zero(_mesh->nrParam());
    for(sizeType i=1; i<nrSegment; i++)
      dParam[4+i*2]=RandEngine::randR(-1,1)*scale;
    MeshCache c,c2;
    //cache
    c._param=_mesh->param0()+Vec::Random(_mesh->nrParam())*scale;
    _mesh->paramToVertex(c,true);
    c.saveNN();
    c._param=_mesh->param0()+Vec::Random(_mesh->nrParam())*scale;
    _mesh->paramToVertex(c,true);
    c.saveN();
    c._param=_mesh->param0()+Vec::Random(_mesh->nrParam())*scale;
    _mesh->paramToVertex(c,true);
    std::vector<InterpInfo> bNI,tNI,bNNI,tNNI;
    searchForInterpInfo(c,bNI,tNI,bNNI,tNNI);

    //cache2
    c2=c;
    c2._param+=dParam*DELTA;
    _mesh->paramToVertex(c2,false);
    std::vector<InterpInfo> bNI2,tNI2,bNNI2,tNNI2;
    searchForInterpInfo(c2,bNI2,tNI2,bNNI2,tNNI2);
    for(sizeType i=1; i<nrSegment; i++) {
      DEBUG_GRADIENT("bNIGrad",(bNI[i].dIdPhi(c._vssN)*dParam[4+i*2]).norm(),
                     (bNI[i].dIdPhi(c._vssN)*dParam[4+i*2]-(bNI2[i].interp(c2._vssN)-bNI[i].interp(c._vssN))/DELTA).norm())
      DEBUG_GRADIENT("tNIGrad",(tNI[i].dIdPhi(c._vssN)*dParam[4+i*2]).norm(),
                     (tNI[i].dIdPhi(c._vssN)*dParam[4+i*2]-(tNI2[i].interp(c2._vssN)-tNI[i].interp(c._vssN))/DELTA).norm())
      DEBUG_GRADIENT("bNNIGrad",(bNNI[i].dIdPhi(c._vssNN)*dParam[4+i*2]).norm(),
                     (bNNI[i].dIdPhi(c._vssNN)*dParam[4+i*2]-(bNNI2[i].interp(c2._vssNN)-bNNI[i].interp(c._vssNN))/DELTA).norm())
      DEBUG_GRADIENT("tNNIGrad",(tNNI[i].dIdPhi(c._vssNN)*dParam[4+i*2]).norm(),
                     (tNNI[i].dIdPhi(c._vssNN)*dParam[4+i*2]-(tNNI2[i].interp(c2._vssNN)-tNNI[i].interp(c._vssNN))/DELTA).norm())
    }
  }
}
scalarD KinematicRibbonEnergy::dt() const
{
  Mat4d H0,DHCoef;
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
  H(ribbon->width(),ribbon->segLen(),H0,DHCoef);
  scalarD coef=_H03(0,0)/kronecker(H0,3)(0,0);
  return std::sqrt(1/coef);
}
void KinematicRibbonEnergy::H(scalarD width,scalarD segLen,Mat4d& H,Mat4d& DHCoef)
{
  //input
  //scalarD segLen;
  //scalarD width;

  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;
  scalarD tt7;
  scalarD tt8;

  tt1=(2.0*width*segLen)/9.0;
  tt2=(width*segLen)/9.0;
  tt3=(width*segLen)/18.0;
  tt4=std::pow(width,2);
  tt5=tt4/9.0;
  tt6=tt4/18.0;
  tt7=-tt4/9.0;
  tt8=-tt4/18.0;
  H(0,0)=tt1;
  H(0,1)=tt2;
  H(0,2)=tt2;
  H(0,3)=tt3;
  H(1,0)=tt2;
  H(1,1)=tt1;
  H(1,2)=tt3;
  H(1,3)=tt2;
  H(2,0)=tt2;
  H(2,1)=tt3;
  H(2,2)=tt1;
  H(2,3)=tt2;
  H(3,0)=tt3;
  H(3,1)=tt2;
  H(3,2)=tt2;
  H(3,3)=tt1;
  DHCoef(0,0)=tt5;
  DHCoef(0,1)=tt6;
  DHCoef(0,2)=0;
  DHCoef(0,3)=0;
  DHCoef(1,0)=tt6;
  DHCoef(1,1)=tt5;
  DHCoef(1,2)=0;
  DHCoef(1,3)=0;
  DHCoef(2,0)=0;
  DHCoef(2,1)=0;
  DHCoef(2,2)=tt7;
  DHCoef(2,3)=tt8;
  DHCoef(3,0)=0;
  DHCoef(3,1)=0;
  DHCoef(3,2)=tt8;
  DHCoef(3,3)=tt7;
}
void KinematicRibbonEnergy::searchForInterpInfo(const MeshCache& cache,std::vector<InterpInfo>& bNI,std::vector<InterpInfo>& tNI,std::vector<InterpInfo>& bNNI,std::vector<InterpInfo>& tNNI) const
{
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
  sizeType nrSegment=ribbon->nrSegment();
  std::vector<scalarD> targets(nrSegment+1);
  bNI.resize(nrSegment+1);
  tNI.resize(nrSegment+1);
  bNNI.resize(nrSegment+1);
  tNNI.resize(nrSegment+1);
  //search for interp param
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<=nrSegment; i++) {
    targets[i]=i*ribbon->segLen();
    if(i>0 && i<nrSegment)
      targets[i]+=cache._param[4+i*2]*ribbon->width();
  }
  buildInterpInfo(targets,true,cache._paramN,bNI);
  buildInterpInfo(targets,true,cache._paramNN,bNNI);
  //search for interp param
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<=nrSegment; i++) {
    targets[i]=i*ribbon->segLen();
    if(i>0 && i<nrSegment)
      targets[i]-=cache._param[4+i*2]*ribbon->width();
  }
  buildInterpInfo(targets,false,cache._paramN,tNI);
  buildInterpInfo(targets,false,cache._paramNN,tNNI);
}
void KinematicRibbonEnergy::buildInterpInfo(const std::vector<scalarD>& targets,bool bottom,const Vec& param,std::vector<InterpInfo>& infos) const
{
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
  sizeType nrSegment=ribbon->nrSegment();
  scalarD sgn=bottom?1:-1;
  std::vector<scalarD> dists(nrSegment+1);
  for(sizeType i=0; i<=nrSegment; i++) {
    dists[i]=i*ribbon->segLen();
    if(i>0 && i<nrSegment)
      dists[i]+=sgn*param[4+i*2]*ribbon->width();
  }
  for(sizeType i=0; i<=nrSegment; i++) {
    infos[i]._bottom=bottom;
    if(i==0) {
      infos[i]._i=0;
      infos[i]._alpha=0;
    } else if(i==nrSegment) {
      infos[i]._i=nrSegment-1;
      infos[i]._alpha=1;
    } else {
      scalarD target=targets[i];
      infos[i]._i=infos[i-1]._i;
      infos[i]._alpha=infos[i-1]._alpha;
      while(true)
        if(dists[infos[i]._i+1]>target) {
          infos[i]._alpha=(target-dists[infos[i]._i])/(dists[infos[i]._i+1]-dists[infos[i]._i]);
          break;
        } else {
          infos[i]._i++;
          infos[i]._alpha=0;
        }
    }
    infos[i]._dAlphadPhi=sgn*ribbon->width()/(dists[infos[i]._i+1]-dists[infos[i]._i]);
  }
}
//ConstantForceEnergy
ConstantForceEnergy::ConstantForceEnergy() {}
ConstantForceEnergy::ConstantForceEnergy(boost::shared_ptr<Mesh> mesh,const Vec3d& f,scalarD rho):KinematicEnergy(mesh,rho)
{
  _f=_mesh->param0();
  for(sizeType i=0; i<_f.size(); i+=3)
    _f.segment<3>(i)=-f;
}
bool ConstantForceEnergy::read(std::istream& is,IOData* dat)
{
  Energy::read(is,dat);
  readBinaryData(_f,is);
  return is.good();
}
bool ConstantForceEnergy::write(std::ostream& os,IOData* dat) const
{
  Energy::write(os,dat);
  writeBinaryData(_f,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> ConstantForceEnergy::copy() const
{
  return boost::shared_ptr<SerializableBase>(new ConstantForceEnergy);
}
std::string ConstantForceEnergy::type() const
{
  return typeid(ConstantForceEnergy).name();
}
void ConstantForceEnergy::operator()(MeshCache& cache) const {
  Vec g=_mass*_f;
  cache._dEdVss+=g;
  cache._E+=cache._vss.dot(g);
}
void ConstantForceEnergy::debugGradient(scalarD scale,sizeType nrIter) const
{
  Energy::debugGradient("ConstantForce",scale,nrIter);
}
//ConstantForceRibbonEnergy
ConstantForceRibbonEnergy::ConstantForceRibbonEnergy() {}
ConstantForceRibbonEnergy::ConstantForceRibbonEnergy(boost::shared_ptr<Mesh> mesh,const Vec3d& f,scalarD rho):KinematicRibbonEnergy(mesh,rho) {
  _f.segment<3>(0)=_f.segment<3>(3)=_f.segment<3>(6)=_f.segment<3>(9)=-f;
}
bool ConstantForceRibbonEnergy::read(std::istream& is,IOData* dat)
{
  Energy::read(is,dat);
  readBinaryData(_f,is);
  return is.good();
}
bool ConstantForceRibbonEnergy::write(std::ostream& os,IOData* dat) const
{
  Energy::write(os,dat);
  writeBinaryData(_f,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> ConstantForceRibbonEnergy::copy() const
{
  return boost::shared_ptr<SerializableBase>(new ConstantForceRibbonEnergy);
}
std::string ConstantForceRibbonEnergy::type() const
{
  return typeid(ConstantForceRibbonEnergy).name();
}
void ConstantForceRibbonEnergy::operator()(MeshCache& cache) const
{
  boost::shared_ptr<MeshRibbon> ribbon=boost::dynamic_pointer_cast<MeshRibbon>(_mesh);
  sizeType nrSegment=ribbon->nrSegment();
  //build energy
  OMP_PARALLEL_FOR_
  for(sizeType i=0; i<nrSegment; i++) {
    scalarD phil=i==0?0:cache._param[4+i*2];
    scalarD phir=i==nrSegment-1?0:cache._param[6+i*2];
    //[xbl,xbr,xtl,xtr]
    Mat12d H=_H03+_DHCoef3*(phir-phil);
    Vec12d g=H*_f,x;
    x.segment<3>(0)=cache._vss.segment<3>(MeshRibbon::ID0(i)*3);
    x.segment<3>(3)=cache._vss.segment<3>(MeshRibbon::ID0(i+1)*3);
    x.segment<3>(6)=cache._vss.segment<3>(MeshRibbon::ID1(i)*3);
    x.segment<3>(9)=cache._vss.segment<3>(MeshRibbon::ID1(i+1)*3);
    scalarD E=x.dot(g);
    OMP_ATOMIC_
    cache._E+=E;
    if(i>0) {
      scalarD dEdPhil=-x.dot(_DHCoef3*_f);
      OMP_ATOMIC_
      cache._dEdParam[4+i*2]+=dEdPhil;
    }
    if(i<ribbon->nrSegment()-1) {
      scalarD dEdPhir= x.dot(_DHCoef3*_f);
      OMP_ATOMIC_
      cache._dEdParam[6+i*2]+=dEdPhir;
    }
    atomicAdd(cache._dEdVss,MeshRibbon::ID0(i)*3,g.segment<3>(0));
    atomicAdd(cache._dEdVss,MeshRibbon::ID0(i+1)*3,g.segment<3>(3));
    atomicAdd(cache._dEdVss,MeshRibbon::ID1(i)*3,g.segment<3>(6));
    atomicAdd(cache._dEdVss,MeshRibbon::ID1(i+1)*3,g.segment<3>(9));
  }
}
void ConstantForceRibbonEnergy::debugGradient(scalarD scale,sizeType nrIter) const
{
  Energy::debugGradient("ConstantForceRibbon",scale,nrIter);
}
//SoftPointConstraintEnergy
SoftPointConstraintEnergy::SoftPointConstraintEnergy() {}
SoftPointConstraintEnergy::SoftPointConstraintEnergy(boost::shared_ptr<Mesh> mesh,sizeType id,const Vec3d& b,scalarD coef):Energy(mesh),_coef(coef) {
  _H.resize(3,_mesh->nrVertex()*3);
  for(sizeType d=0; d<3; d++)
    _H.coeffRef(d,id*3+d)=1;
  _b=b;
}
bool SoftPointConstraintEnergy::read(std::istream& is,IOData* dat)
{
  Energy::read(is,dat);
  readBinaryData(_coef,is);
  readBinaryData(_H,is);
  readBinaryData(_b,is);
  return is.good();
}
bool SoftPointConstraintEnergy::write(std::ostream& os,IOData* dat) const
{
  Energy::write(os,dat);
  writeBinaryData(_coef,os);
  writeBinaryData(_H,os);
  writeBinaryData(_b,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> SoftPointConstraintEnergy::copy() const
{
  return boost::shared_ptr<SerializableBase>(new SoftPointConstraintEnergy);
}
std::string SoftPointConstraintEnergy::type() const
{
  return typeid(SoftPointConstraintEnergy).name();
}
void SoftPointConstraintEnergy::operator()(MeshCache& cache) const
{
  Vec3d A=_H*cache._vss-_b;
  cache._dEdVss+=_H.transpose()*A*_coef;
  cache._E+=A.squaredNorm()*_coef/2;
}
void SoftPointConstraintEnergy::debugGradient(scalarD scale,sizeType nrIter) const
{
  Energy::debugGradient("SoftPointConstraint",scale,nrIter);
}
//SoftNormalConstraintEnergy
SoftNormalConstraintEnergy::SoftNormalConstraintEnergy() {}
SoftNormalConstraintEnergy::SoftNormalConstraintEnergy(boost::shared_ptr<Mesh> mesh,sizeType id,const Vec3d& b,scalarD coef):Energy(mesh),_coef(coef) {
  _H.resize(9,_mesh->nrVertex()*3);
  for(sizeType v=0; v<3; v++)
    for(sizeType d=0; d<3; d++)
      _H.coeffRef(d+v*3,_mesh->tid(id)[v]*3+d)=1;
  _b=b;
}
bool SoftNormalConstraintEnergy::read(std::istream& is,IOData* dat)
{
  Energy::read(is,dat);
  readBinaryData(_coef,is);
  readBinaryData(_H,is);
  readBinaryData(_b,is);
  return is.good();
}
bool SoftNormalConstraintEnergy::write(std::ostream& os,IOData* dat) const
{
  Energy::write(os,dat);
  writeBinaryData(_coef,os);
  writeBinaryData(_H,os);
  writeBinaryData(_b,os);
  return os.good();
}
boost::shared_ptr<SerializableBase> SoftNormalConstraintEnergy::copy() const
{
  return boost::shared_ptr<SerializableBase>(new SoftNormalConstraintEnergy);
}
std::string SoftNormalConstraintEnergy::type() const
{
  return typeid(SoftNormalConstraintEnergy).name();
}
void SoftNormalConstraintEnergy::operator()(MeshCache& cache) const
{
  Mat3X9d g;
  Vec9d abc=_H*cache._vss;
  Vec3d A=crossDiff(abc.segment<3>(0),abc.segment<3>(3),abc.segment<3>(6),g)-_b;
  cache._dEdVss+=_H.transpose()*(g.transpose()*A)*_coef;
  cache._E+=A.squaredNorm()*_coef/2;
}
void SoftNormalConstraintEnergy::debugGradient(scalarD scale,sizeType nrIter) const
{
  Energy::debugGradient("SoftNormalConstraint",scale,nrIter);
}
Vec3d SoftNormalConstraintEnergy::crossDiff(const Vec3d& a,const Vec3d& b,const Vec3d& c,Mat3X9d& J,scalarD eps)
{
  //input
  //Vec3d a;
  //Vec3d b;
  //Vec3d c;
  //scalarD eps;
  Vec3d nn;

  //temp
  scalarD tt1;
  scalarD tt2;
  scalarD tt3;
  scalarD tt4;
  scalarD tt5;
  scalarD tt6;
  scalarD tt7;
  scalarD tt8;
  scalarD tt9;
  scalarD tt10;
  scalarD tt11;
  scalarD tt12;
  scalarD tt13;
  scalarD tt14;
  scalarD tt15;
  scalarD tt16;
  scalarD tt17;
  scalarD tt18;
  scalarD tt19;
  scalarD tt20;
  scalarD tt21;
  scalarD tt22;
  scalarD tt23;
  scalarD tt24;
  scalarD tt25;
  scalarD tt26;
  scalarD tt27;
  scalarD tt28;
  scalarD tt29;
  scalarD tt30;
  scalarD tt31;
  scalarD tt32;
  scalarD tt33;
  scalarD tt34;
  scalarD tt35;
  scalarD tt36;
  scalarD tt37;
  scalarD tt38;
  scalarD tt39;
  scalarD tt40;
  scalarD tt41;
  scalarD tt42;

  tt1=-a[1];
  tt2=c[1]+tt1;
  tt3=-a[2];
  tt4=b[2]+tt3;
  tt5=b[1]+tt1;
  tt6=c[2]+tt3;
  tt7=tt5*tt6-tt2*tt4;
  tt8=-a[0];
  tt9=c[0]+tt8;
  tt10=b[0]+tt8;
  tt11=tt10*tt2-tt9*tt5;
  tt12=tt9*tt4-tt10*tt6;
  tt13=std::sqrt(eps+std::pow(tt7,2)+std::pow(tt12,2)+std::pow(tt11,2));
  tt14=1/tt13;
  tt15=-c[1];
  tt16=tt15+b[1];
  tt17=-b[2];
  tt18=c[2]+tt17;
  tt19=2*tt18*tt12+2*tt16*tt11;
  tt20=1/std::pow(tt13,3);
  tt21=-b[0];
  tt22=c[0]+tt21;
  tt23=-c[2];
  tt24=tt23+b[2];
  tt25=2*tt24*tt7+2*tt22*tt11;
  tt26=-c[0];
  tt27=tt26+b[0];
  tt28=-b[1];
  tt29=c[1]+tt28;
  tt30=2*tt29*tt7+2*tt27*tt12;
  tt31=tt23+a[2];
  tt32=2*tt31*tt12+2*tt2*tt11;
  tt33=tt26+a[0];
  tt34=2*tt6*tt7+2*tt33*tt11;
  tt35=tt15+a[1];
  tt36=2*tt35*tt7+2*tt9*tt12;
  tt37=tt28+a[1];
  tt38=2*tt4*tt12+2*tt37*tt11;
  tt39=tt17+a[2];
  tt40=2*tt39*tt7+2*tt10*tt11;
  tt41=tt21+a[0];
  tt42=2*tt5*tt7+2*tt41*tt12;
  nn(0,0)=tt7*tt14;
  nn(1,0)=tt12*tt14;
  nn(2,0)=tt11*tt14;
  J(0,0)=-(tt7*tt19*tt20)/2.0;
  J(0,1)=tt24*tt14-(tt7*tt25*tt20)/2.0;
  J(0,2)=tt29*tt14-(tt7*tt30*tt20)/2.0;
  J(0,3)=-(tt7*tt32*tt20)/2.0;
  J(0,4)=tt6*tt14-(tt7*tt34*tt20)/2.0;
  J(0,5)=tt35*tt14-(tt7*tt36*tt20)/2.0;
  J(0,6)=-(tt7*tt38*tt20)/2.0;
  J(0,7)=tt39*tt14-(tt7*tt40*tt20)/2.0;
  J(0,8)=tt5*tt14-(tt7*tt42*tt20)/2.0;
  J(1,0)=tt18*tt14-(tt12*tt19*tt20)/2.0;
  J(1,1)=-(tt12*tt25*tt20)/2.0;
  J(1,2)=tt27*tt14-(tt12*tt30*tt20)/2.0;
  J(1,3)=tt31*tt14-(tt12*tt32*tt20)/2.0;
  J(1,4)=-(tt12*tt34*tt20)/2.0;
  J(1,5)=tt9*tt14-(tt12*tt36*tt20)/2.0;
  J(1,6)=tt4*tt14-(tt12*tt38*tt20)/2.0;
  J(1,7)=-(tt12*tt40*tt20)/2.0;
  J(1,8)=tt41*tt14-(tt12*tt42*tt20)/2.0;
  J(2,0)=tt16*tt14-(tt11*tt19*tt20)/2.0;
  J(2,1)=tt22*tt14-(tt11*tt25*tt20)/2.0;
  J(2,2)=-(tt11*tt30*tt20)/2.0;
  J(2,3)=tt2*tt14-(tt11*tt32*tt20)/2.0;
  J(2,4)=tt33*tt14-(tt11*tt34*tt20)/2.0;
  J(2,5)=-(tt11*tt36*tt20)/2.0;
  J(2,6)=tt37*tt14-(tt11*tt38*tt20)/2.0;
  J(2,7)=tt10*tt14-(tt11*tt40*tt20)/2.0;
  J(2,8)=-(tt11*tt42*tt20)/2.0;
  return nn;
}
