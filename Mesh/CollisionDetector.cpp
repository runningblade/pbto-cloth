#include "CollisionDetector.h"
#include "CollisionHandler.h"
#include "Simulator.h"
#include "CommonFile/geom/BVHBuilder.h"

USE_PRJ_NAMESPACE

static FORCE_INLINE void adjustNormalSign(const Vec3d v[4],const Vec4d& omg,Vec3d& n)
{
  if(n.dot(v[0]*omg[0]+v[1]*omg[1]+v[2]*omg[2]+v[3]*omg[3]) < 0.0f)
    n*=-1.0f;
}
static FORCE_INLINE void calcCoef(const Vec3d pt[4],const Vec3d v[4],scalarD c[4])
{
  Vec3d x21=pt[1]-pt[0];
  Vec3d x31=pt[2]-pt[0];
  Vec3d x41=pt[3]-pt[0];

  Vec3d v21=v[1]-v[0];
  Vec3d v31=v[2]-v[0];
  Vec3d v41=v[3]-v[0];

  Vec3d x21Xx31=x21.cross(x31);
  Vec3d v21Xv31=v21.cross(v31);
  Vec3d v21Cx31_x21Cv31=v21.cross(x31)+x21.cross(v31);

  c[0]=x21Xx31.dot(x41);
  c[1]=x21Xx31.dot(v41)+v21Cx31_x21Cv31.dot(x41);
  c[2]=v21Xv31.dot(x41)+v21Cx31_x21Cv31.dot(v41);
  c[3]=v21Xv31.dot(v41);
}
static FORCE_INLINE scalarD eval(const scalarD c[4],scalarD s)
{
  scalarD ret=c[3]*s+c[2];
  ret=ret*s+c[1];
  ret=ret*s+c[0];
  return ret;
}
static FORCE_INLINE bool secant(scalarD l,scalarD r,const scalarD c[4],scalarD& s, scalarD tol)
{
  //out of range
  if(l>r)
    return false;
  //already close to zero
  scalarD el=eval(c,l);
  if(std::abs(el)<tol) {
    s=l;
    return true;
  }
  //already close to zero
  scalarD er=eval(c,r);
  if(std::abs(er)<tol) {
    s=r;
    return true;
  }
  //no zero in interval
  if(el*er>0.0f)
    return false;
  //secant search
  scalarD m,em;
  for(int i=0; i<1000; i++) {
    m=(el*r-er*l)/(el-er);
    em=eval(c,m);
    if(std::abs(em)<tol || std::abs(r-l)<CollisionDetector::_timeRes) {
      s=m;
      return true;
    }
    if(el*em<0.0f) {
      r=m;
      er=em;
    } else {
      l=m;
      el=em;
    }
  }
  return false;
}
static FORCE_INLINE int solveCubicAllSecant(scalarD l,scalarD r,const scalarD c[4],scalarD s[4])
{
  scalarD tol=1E-7f*(std::abs(c[0])+std::abs(c[1])+std::abs(c[2])+std::abs(c[3]));
  //determine the minimum and maximum
  //c[1] + (2*c[2]) * x + (3*c[3]) * x^2
  //sol=(-c[2] (+/-) sqrt(c[2]*c[2]-3*c[3]*c[1]) )/(3*c[3])
  scalarD sol0,sol1;
  scalarD deter=c[2]*c[2]-3.0f*c[3]*c[1];
  if(deter<=0.0f) {
    return secant(l,r,c,s[0],tol)?1:0;
  } else {
    deter=std::sqrt(deter);
    sol0=(-c[2]-deter)/(3.0f*c[3]);
    sol1=(-c[2]+deter)/(3.0f*c[3]);
    if(sol0>sol1)std::swap(sol0,sol1);
    //scant search in each segment
    int nrSol=0;
    if(secant(l,
              std::min<scalarD>(sol0,r),
              c,s[nrSol],tol))nrSol++;
    if(secant(std::max<scalarD>(l,sol0),
              std::min<scalarD>(r,sol1),
              c,s[nrSol],tol))nrSol++;
    if(secant(std::max<scalarD>(l,sol1),
              r,
              c,s[nrSol],tol))nrSol++;
    return nrSol;
  }
}
static FORCE_INLINE bool testVT(const Vec3d& v,const Vec3d& t0,const Vec3d& t1,const Vec3d& t2,Vec4d& omega,Vec3d& n)
{
  Vec3d t13=t0-t2;
  Vec3d t23=t1-t2;
  n=t13.cross(t23);

  scalarD area=n.norm();
  n/=area;

  if(std::abs((v-t0).dot(n))>CollisionDetector::_thickness)
    return false;
  else {
    scalarD delta=CollisionDetector::_thickness/std::max(std::sqrt(area),CollisionDetector::_rounding);
    Vec3d t43=v-t2;

    Vec2d RHS(t13.dot(t43),t23.dot(t43));
    Mat2d LHS;
    LHS(0,0)=t13.dot(t13);
    LHS(1,0)=LHS(0,1)=t13.dot(t23);
    LHS(1,1)=t23.dot(t23);

    omega.block<2,1>(0,0)=LHS.inverse()*RHS;
    omega[2]=1.0f-omega[0]-omega[1];
    return omega[0]>=-delta &&
           omega[1]>=-delta &&
           omega[2]>=-delta &&
           omega[0]<=1.0f+delta &&
           omega[1]<=1.0f+delta &&
           omega[2]<=1.0f+delta;
  }
}
static FORCE_INLINE bool testEE(const Vec3d& eA0,const Vec3d& eA1,const Vec3d& eB0,const Vec3d& eB1,Vec4d& omega,Vec3d& n)
{
  Vec3d t21=eA1-eA0;
  Vec3d t43=eB1-eB0;

  scalarD delta=CollisionDetector::_thickness/std::max(std::max(t21.norm(),t43.norm()),CollisionDetector::_rounding);
  Vec3d nt21=t21/std::max(t21.norm(),CollisionDetector::_rounding);
  Vec3d nt43=t43/std::max(t43.norm(),CollisionDetector::_rounding);
  n=(nt21).cross(nt43);

  scalarD nNorm=n.norm();
  if(nNorm < CollisionDetector::_rounding) {
    //ignore parallel edges
    //David Harmon pointed out that parallel edge constraint
    //can be detected by other point triangle test
    /*Vec3d dA=eA1-eA0;
    dA/=std::max(dA.norm(),CollisionDetector::_rounding);

    Vec3d n=eA0-eB0;
    n-=n.dot(dA)*dA;
    if(n.norm()>CollisionDetector::_thickness)
      return false;

    scalarD IA[2]={eA0.dot(dA),eA1.dot(dA)};
    scalarD IB[2]={eB0.dot(dA),eB1.dot(dA)};

    scalarD SIB[2]={std::min(IB[0],IB[1]),std::max(IB[0],IB[1])};
    if(IA[0]>=SIB[0] && IA[0]<=SIB[1]) {
      omega[0]=1.0f;
      omega[1]=0.0f;
      omega[2]=(IA[0]-IB[1])/(IB[0]-IB[1]);
      omega[3]=1.0f-omega[2];
      return true;
    } else if(IA[1]>=SIB[0] && IA[1]<=SIB[1]) {
      omega[0]=0.0f;
      omega[1]=1.0f;
      omega[2]=(IA[1]-IB[1])/(IB[0]-IB[1]);
      omega[3]=1.0f-omega[2];
      return true;
    }

    scalarD SIA[2]={std::min(IA[0],IA[1]),std::max(IA[0],IA[1])};
    if(IB[0]>=SIA[0] && IB[0]<=SIA[1]) {
      omega[2]=1.0f;
      omega[3]=0.0f;
      omega[0]=(IB[0]-IA[1])/(IA[0]-IA[1]);
      omega[1]=1.0f-omega[0];
      return true;
    } else if(IB[1]>=SIA[0] && IB[1]<=SIA[1]) {
      omega[2]=0.0f;
      omega[3]=1.0f;
      omega[0]=(IB[1]-IA[1])/(IA[0]-IA[1]);
      omega[1]=1.0f-omega[0];
      return true;
    }*/
    return false;
  } else {
    n/=nNorm;
    Vec3d t31=eB0-eA0;

    Vec2d RHS(t21.dot(t31),-t43.dot(t31));
    Mat2d LHS;
    LHS(0,0)=t21.dot(t21);
    LHS(0,1)=-t21.dot(t43);
    LHS(1,0)=-t21.dot(t43);
    LHS(1,1)=t43.dot(t43);
    RHS=LHS.inverse()*RHS;
    if(RHS[0]>=-delta && RHS[1]>=-delta && RHS[0]<=1.0f+delta && RHS[1]<=1.0f+delta) {
      t21=eA0+t21*RHS[0];
      t43=eB0+t43*RHS[1];
      if((t21-t43).norm() < CollisionDetector::_thickness) {
        omega[0]=1.0f-RHS[0];
        omega[1]=RHS[0];
        omega[2]=1.0f-RHS[1];
        omega[3]=RHS[1];
        return true;
      }
    }
    return false;
  }
}
static FORCE_INLINE bool testVT(Cache<CollisionDetector::VertexRef,CollisionDetector::TriangleRef> VT,CollisionHandler& handler)
{
  //geometry info
  Vec3d pt[4]= {VT._A.posN()      ,VT._B.posN(0)      ,VT._B.posN(1)      ,VT._B.posN(2)      };
  Vec3d v [4]= {VT._A.pos ()-pt[0],VT._B.pos (0)-pt[1],VT._B.pos (1)-pt[2],VT._B.pos (2)-pt[3]};

  //CCD test
  scalarD c[4];
  calcCoef(pt,v,c);

  scalarD s[4];
  scalarD cofl=0.0f, cofr=1.0f;
  int nr=solveCubicAllSecant(cofl,cofr,c,s);

  //check result
  Vec3d pos[4];
  Vec4d omega;
  for(int i=0; i<nr; ++i) {
    for(int j=0; j<4; ++j)
      pos[j]=pt[j]+v[j]*s[i];
    if(testVT(pos[0],pos[1],pos[2],pos[3],omega,VT._n)) {
      VT._t=s[i];
      VT._omg=Vec4d(1.0f,-omega[0],-omega[1],-omega[2]);
      adjustNormalSign(v,-VT._omg,VT._n);
      handler.handle(VT);
      return true;
    }
  }
  return false;
}
static FORCE_INLINE bool testEE(Cache<CollisionDetector::EdgeRef,CollisionDetector::EdgeRef> EE,CollisionHandler& handler)
{
  //geometry info
  Vec3d pt[4]= {EE._A.posN(0)      ,EE._A.posN(1)      ,EE._B.posN(0)      ,EE._B.posN(1)      };
  Vec3d v [4]= {EE._A.pos (0)-pt[0],EE._A.pos (1)-pt[1],EE._B.pos (0)-pt[2],EE._B.pos (1)-pt[3]};

  //CCD test
  scalarD c[4];
  calcCoef(pt,v,c);

  scalarD s[4];
  scalarD cofl=0.0f, cofr=1.0f;
  int nr=solveCubicAllSecant(cofl,cofr,c,s);

  //check result
  Vec3d pos[4];
  Vec4d omega;
  for(int i=0; i<nr; ++i) {
    for(int j=0; j<4; ++j)
      pos[j]=pt[j]+v[j]*s[i];
    if(testEE(pos[0],pos[1],pos[2],pos[3],omega,EE._n)) {
      EE._t=s[i];
      EE._omg=Vec4d(omega[0],omega[1],-omega[2],-omega[3]);
      adjustNormalSign(v,-EE._omg,EE._n);
      handler.handle(EE);
      return true;
    }
  }
  return false;
}
static FORCE_INLINE bool testTT(Cache<CollisionDetector::TriangleRef,CollisionDetector::TriangleRef> TT,CollisionHandler& handler)
{
  TriangleTpl<scalarD> t1(TT._A.pos(0),TT._A.pos(1),TT._A.pos(2));
  TriangleTpl<scalarD> t2(TT._B.pos(0),TT._B.pos(1),TT._B.pos(2));
  Vec3d b1,b2;
  scalarD sqrDist;
  t1.calcTriangleDist(t2,sqrDist,b1,b2);
  if(sqrDist<CollisionDetector::_staticDist*CollisionDetector::_staticDist) {
    handler.handle(TT);
    return true;
  } else return false;
}
//Cache<TA,TB>
template <typename TA,typename TB>
Cache<TA,TB>::Cache() {}
template <typename TA,typename TB>
Cache<TA,TB>::Cache(const TA& A,const TB& B):_A(A),_B(B) {}
template <typename TA,typename TB>
bool Cache<TA,TB>::read(std::istream& is,IOData* dat)
{
  readBinaryData(_A,is,dat);
  readBinaryData(_B,is,dat);
  return is.good();
}
template <typename TA,typename TB>
bool Cache<TA,TB>::write(std::ostream& os,IOData* dat) const
{
  writeBinaryData(_A,os,dat);
  writeBinaryData(_B,os,dat);
  return os.good();
}
template <typename TA,typename TB>
boost::shared_ptr<SerializableBase> Cache<TA,TB>::copy() const
{
  return boost::shared_ptr<SerializableBase>(new Cache<TA,TB>);
}
template <typename TA,typename TB>
std::string Cache<TA,TB>::type() const
{
  return typeid(Cache<TA,TB>).name();
}
template <typename TA,typename TB>
bool Cache<TA,TB>::operator!=(const Cache<TA,TB>& other) const
{
  return _A!=other._A || _B!=other._B;
}
template <typename TA,typename TB>
bool Cache<TA,TB>::operator==(const Cache<TA,TB>& other) const
{
  return _A==other._A && _B==other._B;
}
template <typename TA,typename TB>
bool Cache<TA,TB>::operator<(const Cache<TA,TB>& other) const
{
  if(_A<other._A)
    return true;
  else if(other._A<_A)
    return false;
  if(_B<other._B)
    return true;
  else if(other._B<_B)
    return false;
  return false;
}
//Cache<T>
template <typename T>
Cache<T,T>::Cache() {}
template <typename T>
Cache<T,T>::Cache(const T& A,const T& B):_A(A),_B(B) {
  if(_B<_A)
    std::swap(_A,_B);
}
template <typename T>
bool Cache<T,T>::read(std::istream& is,IOData* dat)
{
  readBinaryData(_A,is,dat);
  readBinaryData(_B,is,dat);
  return is.good();
}
template <typename T>
bool Cache<T,T>::write(std::ostream& os,IOData* dat) const
{
  writeBinaryData(_A,os,dat);
  writeBinaryData(_B,os,dat);
  return os.good();
}
template <typename T>
boost::shared_ptr<SerializableBase> Cache<T,T>::copy() const
{
  return boost::shared_ptr<SerializableBase>(new Cache<T,T>);
}
template <typename T>
std::string Cache<T,T>::type() const
{
  return typeid(Cache<T,T>).name();
}
template <typename T>
bool Cache<T,T>::operator!=(const Cache<T,T>& other) const
{
  return _A!=other._A || _B!=other._B;
}
template <typename T>
bool Cache<T,T>::operator==(const Cache<T,T>& other) const
{
  return _A==other._A && _B==other._B;
}
template <typename T>
bool Cache<T,T>::operator<(const Cache<T,T>& other) const
{
  if(_A<other._A)
    return true;
  else if(other._A<_A)
    return false;
  if(_B<other._B)
    return true;
  else if(other._B<_B)
    return false;
  return false;
}
//CacheHash
template <typename TA,typename TB>
size_t CacheHash<TA,TB>::operator()(const Cache<TA,TB>& c) const
{
  return operator()(c._A)+operator()(c._B);
}
template <typename TA,typename TB>
size_t CacheHash<TA,TB>::operator()(const boost::shared_ptr<NarrowNode>& p) const
{
  return boost::hash<boost::shared_ptr<NarrowNode>>()(p);
}
template <typename TA,typename TB>
size_t CacheHash<TA,TB>::operator()(const VertexRef& ref) const
{
  return boost::hash<boost::shared_ptr<NarrowNode>>()(ref._node)+boost::hash<sizeType>()(ref._id);
}
template <typename TA,typename TB>
size_t CacheHash<TA,TB>::operator()(const TriangleRef& ref) const
{
  return boost::hash<boost::shared_ptr<NarrowNode>>()(ref._node)+boost::hash<sizeType>()(ref._id);
}
template <typename TA,typename TB>
size_t CacheHash<TA,TB>::operator()(const EdgeRef& ref) const
{
  return boost::hash<boost::shared_ptr<NarrowNode>>()(ref._node)+boost::hash<sizeType>()(ref._id);
}
template <typename TA,typename TB>
size_t CacheHash<TA,TB>::operator()(sizeType c) const
{
  return boost::hash<sizeType>()(c);
}
//CollisionDetector
bool CollisionDetector::read(std::istream& is,IOData* dat)
{
  registerTypes(dat);
  readBinaryData(_bvh,is,dat);
  readBinaryData(_cacheVT,is,dat);
  readBinaryData(_cacheEE,is,dat);
  readBinaryData(_cache,is,dat);
  readBinaryData(_activeCache,is,dat);
  return is.good();
}
bool CollisionDetector::write(std::ostream& os,IOData* dat) const
{
  registerTypes(dat);
  writeBinaryData(_bvh,os,dat);
  writeBinaryData(_cacheVT,os,dat);
  writeBinaryData(_cacheEE,os,dat);
  writeBinaryData(_cache,os,dat);
  writeBinaryData(_activeCache,os,dat);
  return os.good();
}
boost::shared_ptr<SerializableBase> CollisionDetector::copy() const
{
  return boost::shared_ptr<SerializableBase>(new CollisionDetector);
}
std::string CollisionDetector::type() const
{
  return typeid(CollisionDetector).name();
}
sizeType CollisionDetector::nrMesh() const
{
  return ((sizeType)_bvh.size()+1)/2;
}
boost::shared_ptr<Mesh> CollisionDetector::getMesh(sizeType i) const
{
  return _bvh[i]._cell->_mesh;
}
boost::shared_ptr<NarrowNode> CollisionDetector::getNode(sizeType i) const
{
  return _bvh[i]._cell;
}
void CollisionDetector::updateMesh(boost::shared_ptr<Mesh> mesh)
{
  for(sizeType i=0; i<(sizeType)_bvh.size(); i++)
    if(_bvh[i]._cell && _bvh[i]._cell->_mesh == mesh)
      _bvh[i]._cell->buildBVH();
}
void CollisionDetector::addMesh(boost::shared_ptr<Mesh> mesh,boost::shared_ptr<MeshCache> cache)
{
  //check if already exists
  for(sizeType i=0; i<(sizeType)_bvh.size(); i++)
    if(_bvh[i]._cell && _bvh[i]._cell->_mesh == mesh)
      return;
  //insert
  for(sizeType i=0; i<(sizeType)_bvh.size();)
    if(_bvh[i]._cell) {
      _bvh[i]._nrCell=1;
      _bvh[i]._parent=-1;
      _bvh[i]._bb=_bvh[i]._cell->refit();
      i++;
    } else {
      _bvh[i]=_bvh.back();
      _bvh.pop_back();
    }
  ASSERT_MSG(mesh->isFreeze()==!_bvh.empty(),"We only allow one ClothMesh be inserted first!")
  _bvh.push_back(Node<boost::shared_ptr<NarrowNode>,BBOX>());
  _bvh.back()._nrCell=1;
  _bvh.back()._parent=-1;
  _bvh.back()._cell.reset(new NarrowNode);
  _bvh.back()._cell->_mesh=mesh;
  _bvh.back()._cell->_cache=cache;
  _bvh.back()._cell->buildBVH();
  _bvh.back()._bb=_bvh.back()._cell->refit();
  //rebuild BVH
  BVHBuilder<Node<boost::shared_ptr<NarrowNode>,BBOX>,3>().buildBVH(_bvh);
}
void CollisionDetector::delMesh(boost::shared_ptr<Mesh> mesh)
{
  while(!_bvh.empty() && !_bvh.back()._cell)
    _bvh.pop_back();
  for(sizeType i=0; i<(sizeType)_bvh.size();)
    if(_bvh[i]._cell->_mesh == mesh) {
      _bvh[i]=_bvh.back();
      _bvh.pop_back();
    } else {
      _bvh[i]._nrCell=1;
      _bvh[i]._parent=-1;
      _bvh[i]._cell->refit();
      i++;
    }
  BVHBuilder<Node<boost::shared_ptr<NarrowNode>,BBOX>,3>().buildBVH(_bvh);
}
void CollisionDetector::collide(CollisionHandler& handler,bool continuous,bool useActive,bool bruteForce,bool stopFirst)
{
  //refit all
  _continuous=continuous;
  for(sizeType i=0; i<(sizeType)_bvh.size(); i++)
    if(_bvh[i]._cell)
      _bvh[i]._bb=_bvh[i]._cell->refit(continuous);
    else {
      _bvh[i]._bb.reset();
      _bvh[i]._bb.setUnion(_bvh[_bvh[i]._l]._bb);
      _bvh[i]._bb.setUnion(_bvh[_bvh[i]._r]._bb);
    }
  //BroadPhase
  _cache.clear();
  _cacheTT.clear();
  BVHQuery<boost::shared_ptr<NarrowNode>,BBOX> q(_bvh,3,boost::shared_ptr<NarrowNode>());
  q.interBodyQuery(q,*this);
  //NarrowPhase
  _cacheVT.clear();
  _cacheEE.clear();
  for(CACHEs::const_iterator beg=_cache.begin(),end=_cache.end(); beg!=end; beg++) {
    _activeCache=*beg;
    if(bruteForce) {
      for(sizeType r=0; r<(sizeType)_activeCache._A->_mesh->nrTriangle(); r++) {
        BBOX bbR;
        TriangleRef ARef(_activeCache._A,r);
        bbR.setUnion(ARef.pos(0).cast<scalar>());
        bbR.setUnion(ARef.pos(1).cast<scalar>());
        bbR.setUnion(ARef.pos(2).cast<scalar>());
        if(continuous) {
          bbR.setUnion(ARef.posN(0).cast<scalar>());
          bbR.setUnion(ARef.posN(1).cast<scalar>());
          bbR.setUnion(ARef.posN(2).cast<scalar>());
        }
        bbR.enlarged((scalar)CollisionDetector::_thickness);
        for(sizeType c=0; c<(sizeType)_activeCache._B->_mesh->nrTriangle(); c++) {
          BBOX bbC;
          TriangleRef BRef(_activeCache._B,c);
          bbC.setUnion(BRef.pos(0).cast<scalar>());
          bbC.setUnion(BRef.pos(1).cast<scalar>());
          bbC.setUnion(BRef.pos(2).cast<scalar>());
          if(continuous) {
            bbC.setUnion(BRef.posN(0).cast<scalar>());
            bbC.setUnion(BRef.posN(1).cast<scalar>());
            bbC.setUnion(BRef.posN(2).cast<scalar>());
          }
          bbC.enlarged((scalar)CollisionDetector::_thickness);
          if(bbR.intersect(bbC))
            onCell(_activeCache._A->_bvh[r],_activeCache._B->_bvh[c]);
        }
      }
    } else {
      BVHQuery<sizeType,BBOX> q1(_activeCache._A->_bvh,3,-1);
      BVHQuery<sizeType,BBOX> q2(_activeCache._B->_bvh,3,-1);
      if(useActive && !_activeCache._A->_active.empty())
        q1._active=&(_activeCache._A->_active);
      if(useActive && !_activeCache._B->_active.empty())
        q2._active=&(_activeCache._B->_active);
      q1.interBodyQuery(q2,*this);
    }
  }
  collideFineGrained(handler,stopFirst);
}
void CollisionDetector::collideBruteForce(CollisionHandler& handler,bool stopFirst)
{
  //refit all
  _continuous=true;
  for(sizeType i=0; i<(sizeType)_bvh.size(); i++)
    if(_bvh[i]._cell)
      _bvh[i]._bb=_bvh[i]._cell->refit(true);
    else {
      _bvh[i]._bb.reset();
      _bvh[i]._bb.setUnion(_bvh[_bvh[i]._l]._bb);
      _bvh[i]._bb.setUnion(_bvh[_bvh[i]._r]._bb);
    }
  //NarrowPhase
  _cacheVT.clear();
  _cacheEE.clear();
  //meshI
  for(sizeType i=0; i<nrMesh(); i++)
    for(sizeType vi=0; vi<_bvh[i]._cell->_mesh->nrVertex(); vi++) {
      VertexRef IRef(_bvh[i]._cell,vi);
      const BBOX& IBB=_bvh[i]._cell->_vbb[vi];
      //meshJ
      for(sizeType j=0; j<nrMesh(); j++)
        for(sizeType tj=0; tj<_bvh[j]._cell->_mesh->nrTriangle(); tj++) {
          TriangleRef JRef(_bvh[j]._cell,tj);
          const BBOX& JBB=_bvh[j]._cell->_bvh[tj]._bb;
          if(IBB.intersect(JBB))
            onCell(IRef,JRef);
        }
    }
  //meshI
  for(sizeType i=0; i<nrMesh(); i++)
    for(sizeType ei=0; ei<_bvh[i]._cell->_mesh->nrEdge(); ei++) {
      EdgeRef IRef(_bvh[i]._cell,ei);
      const BBOX& IBB=_bvh[i]._cell->_ebb[ei];
      //meshJ
      for(sizeType j=0; j<nrMesh(); j++)
        for(sizeType ej=0; ej<_bvh[j]._cell->_mesh->nrEdge(); ej++) {
          EdgeRef JRef(_bvh[j]._cell,ej);
          const BBOX& JBB=_bvh[j]._cell->_ebb[ej];
          if(IBB.intersect(JBB))
            onCell(IRef,JRef);
        }
    }
  collideFineGrained(handler,stopFirst);
}
void CollisionDetector::collideFineGrained(CollisionHandler& handler,bool stopFirst)
{
  sizeType nrFound,nrReal;
  if(_continuous) {
    //Fine-Grained TV Test
    nrFound=_cacheVT.size();
    nrReal=0;
    for(CACHE_VTs::const_iterator beg=_cacheVT.begin(),end=_cacheVT.end(); beg!=end; beg++)
      if(testVT(*beg,handler)) {
        if(stopFirst)
          return;
        nrReal++;
      }
    INFOV("VT Collision: (%d,%d)",nrFound,nrReal);
    //Fine-Grained EE Test
    nrFound=_cacheEE.size();
    nrReal=0;
    for(CACHE_EEs::const_iterator beg=_cacheEE.begin(),end=_cacheEE.end(); beg!=end; beg++)
      if(testEE(*beg,handler)) {
        if(stopFirst)
          return;
        nrReal++;
      }
    INFOV("EE Collision: (%d,%d)",nrFound,nrReal);
  } else {
    nrFound=_cacheTT.size();
    nrReal=0;
    for(CACHE_TTs::const_iterator beg=_cacheTT.begin(),end=_cacheTT.end(); beg!=end; beg++)
      if(testTT(*beg,handler)) {
        if(stopFirst)
          return;
        nrReal++;
      }
    INFOV("TT Collision: (%d,%d)",nrFound,nrReal);
  }
}
void CollisionDetector::activate(const VertexRef& v)
{
  boost::unordered_map<sizeType,boost::unordered_set<sizeType>>::const_iterator it=v._node->_oneRing.find(v._id);
  ASSERT(it!=v._node->_oneRing.end())
  const boost::unordered_set<sizeType>& oneRing=it->second;
  for(boost::unordered_set<sizeType>::const_iterator beg=oneRing.begin(),end=oneRing.end(); beg!=end; beg++)
    v._node->_active[*beg]=true;
}
void CollisionDetector::restartActive()
{
  for(sizeType i=0; i<(sizeType)_bvh.size(); i++)
    if(_bvh[i]._cell) {
      NarrowNode& n=*(_bvh[i]._cell);
      n._active.assign(((sizeType)n._bvh.size()+1)/2,false);
    }
}
void CollisionDetector::clearActive()
{
  for(sizeType i=0; i<(sizeType)_bvh.size(); i++)
    if(_bvh[i]._cell)
      _bvh[i]._cell->_active.clear();
}
void CollisionDetector::onCell(const Node<boost::shared_ptr<NarrowNode>,BBOX>& A,const Node<boost::shared_ptr<NarrowNode>,BBOX>& B)
{
  _cache.insert(CACHE(A._cell,B._cell));
}
void CollisionDetector::onCell(const Node<sizeType,BBOX>& nA,const Node<sizeType,BBOX>& nB)
{
  if(_continuous) {
    const boost::unordered_set<sizeType>& eA=_activeCache._A->_edgeOfTri.find(nA._cell)->second;
    const boost::unordered_set<sizeType>& eB=_activeCache._B->_edgeOfTri.find(nB._cell)->second;
    for(boost::unordered_set<sizeType>::const_iterator beg=eA.begin(),end=eA.end(); beg!=end; beg++) {
      EdgeRef EARef(_activeCache._A,*beg);
      for(boost::unordered_set<sizeType>::const_iterator beg2=eB.begin(),end2=eB.end(); beg2!=end2; beg2++) {
        EdgeRef EBRef(_activeCache._B,*beg2);
        if(EARef.BBox().intersect(EBRef.BBox()))onCell(EARef,EBRef);
      }
    }
    TriangleRef TARef(_activeCache._A,nA._cell);
    TriangleRef TBRef(_activeCache._B,nB._cell);
    for(int i=0; i<3; i++) {
      VertexRef VARef(_activeCache._A,_activeCache._A->_mesh->tid(nA._cell)[i]);
      VertexRef VBRef(_activeCache._B,_activeCache._B->_mesh->tid(nB._cell)[i]);
      if(TBRef.BBox().intersect(VARef.BBox()))onCell(VARef,TBRef);
      if(TARef.BBox().intersect(VBRef.BBox()))onCell(VBRef,TARef);
    }
  } else {
    TriangleRef TARef(_activeCache._A,nA._cell);
    TriangleRef TBRef(_activeCache._B,nB._cell);
    if(TARef.id(0)!=TBRef.id(0) && TARef.id(0)!=TBRef.id(1) && TARef.id(0)!=TBRef.id(2) &&
        TARef.id(1)!=TBRef.id(0) && TARef.id(1)!=TBRef.id(1) && TARef.id(1)!=TBRef.id(2) &&
        TARef.id(2)!=TBRef.id(0) && TARef.id(2)!=TBRef.id(1) && TARef.id(2)!=TBRef.id(2))
      _cacheTT.insert(CACHE_TT(TARef,TBRef));
  }
}
void CollisionDetector::onCell(const EdgeRef& A,const EdgeRef& B)
{
  if(A._node!=B._node || (A.id(0)!=B.id(0) && A.id(0)!=B.id(1) && A.id(1)!=B.id(0) && A.id(1)!=B.id(1)))
    _cacheEE.insert(CACHE_EE(A,B));
}
void CollisionDetector::onCell(const VertexRef& A,const TriangleRef& B)
{
  if(A._node!=B._node || (A!=B.id(0) && A!=B.id(1) && A!=B.id(2)))
    _cacheVT.insert(CACHE_VT(A,B));
}
void CollisionDetector::registerTypes(IOData* dat)
{
  Simulator::registerTypes(dat);
  registerType<NarrowNode>(dat);
  registerType<CollisionDetector>(dat);
  registerType<CACHE_VT>(dat);
  registerType<CACHE_EE>(dat);
  registerType<CACHE>(dat);
  registerType<MeshRef<VERTEX>>(dat);
  registerType<MeshRef<TRIANGLE>>(dat);
  registerType<MeshRef<EDGE>>(dat);
}
//debug code
void CollisionDetector::debugBroadphase(std::vector<boost::shared_ptr<Mesh>> meshes,scalarD scale,bool continuous)
{
  _bvh.clear();
  std::vector<boost::shared_ptr<MeshCache>> caches(meshes.size());
  for(sizeType i=0; i<(sizeType)meshes.size(); i++) {
    caches[i].reset(new MeshCache);
    caches[i]->_param=meshes[i]->param0()+Mesh::Vec::Random(meshes[i]->nrParam())*scale;
    meshes[i]->paramToVertex(*caches[i]);
    caches[i]->saveN();
    caches[i]->_param=meshes[i]->param0()+Mesh::Vec::Random(meshes[i]->nrParam())*scale;
    meshes[i]->paramToVertex(*caches[i]);
    addMesh(meshes[i],caches[i]);
  }

  CollisionHandler handler1;
  collide(handler1,continuous,false,true);
  handler1.writeVTK("handler1"+std::string(continuous?"C":"D"));
  CACHE_TTs tmpTT=_cacheTT;
  CACHE_VTs tmpVT=_cacheVT;
  CACHE_EEs tmpEE=_cacheEE;

  CollisionHandler handler2;
  collide(handler2,continuous,false,false);
  handler2.writeVTK("handler2"+std::string(continuous?"C":"D"));
  ASSERT(tmpTT==_cacheTT && tmpVT==_cacheVT && tmpEE==_cacheEE)

  if(continuous) {
    CollisionHandler handler3;
    collideBruteForce(handler3);
    handler3.writeVTK("handler3"+std::string(continuous?"C":"D"));
    ASSERT(tmpVT==_cacheVT && tmpEE==_cacheEE)
    ASSERT(handler1==handler3)
  }
  ASSERT(handler1==handler2)
}
void CollisionDetector::debugBroadphaseActive(std::vector<boost::shared_ptr<Mesh>> meshes,scalarD scale,sizeType nrIter)
{
  _bvh.clear();
  CollisionDetector cdRef;
  ASSERT((sizeType)meshes.size()==2)
  std::vector<boost::shared_ptr<MeshCache>> caches(meshes.size());
  for(sizeType i=0; i<(sizeType)meshes.size(); i++) {
    caches[i].reset(new MeshCache);
    caches[i]->_param=meshes[i]->param0();
    meshes[i]->paramToVertex(*caches[i]);
    caches[i]->saveN();
    caches[i]->_param=meshes[i]->param0();
    meshes[i]->paramToVertex(*caches[i]);
    if(i==1)
      for(sizeType i=0; i<caches[i]->_vss.size(); i+=3) {
        caches[i]->_vss.segment<3>(i)+=Vec3d::Constant(scale);
        caches[i]->_vssN.segment<3>(i)+=Vec3d::Constant(scale);
      }
    addMesh(meshes[i],caches[i]);
    cdRef.addMesh(meshes[i],caches[i]);
  }
  restartActive();
  for(sizeType i=0; i<nrIter; i++) {
    CollisionHandler handler1;
    collide(handler1,true,true,false);
    CollisionHandler handler2;
    cdRef.collide(handler2,true,false,false);
    ASSERT(handler1==handler2)

    sizeType vid0=RandEngine::randI(0,meshes[0]->nrVertex()-1);
    sizeType vid1=RandEngine::randI(0,meshes[1]->nrVertex()-1);
    caches[0]->_vss.segment<3>(vid0*3)+=Vec3d::Random()*scale;
    caches[1]->_vss.segment<3>(vid1*3)+=Vec3d::Random()*scale;
    activate(VertexRef(_bvh[0]._cell,vid0));
    activate(VertexRef(_bvh[1]._cell,vid1));
  }
}
scalarD CollisionDetector::_staticDist=1E-3f;
scalarD CollisionDetector::_thickness=1E-3f;
scalarD CollisionDetector::_rounding=1E-6f;
scalarD CollisionDetector::_timeRes=1E-5f;
//instance
PRJ_BEGIN
template class Cache<CollisionDetector::TriangleRef,CollisionDetector::TriangleRef>;
template class Cache<CollisionDetector::VertexRef,CollisionDetector::TriangleRef>;
template class Cache<CollisionDetector::VertexRef,CollisionDetector::VertexRef>;
template class Cache<CollisionDetector::EdgeRef,CollisionDetector::EdgeRef>;
template class Cache<boost::shared_ptr<NarrowNode>,boost::shared_ptr<NarrowNode>>;

template class CacheHash<CollisionDetector::TriangleRef,CollisionDetector::TriangleRef>;
template class CacheHash<CollisionDetector::VertexRef,CollisionDetector::TriangleRef>;
template class CacheHash<CollisionDetector::VertexRef,CollisionDetector::VertexRef>;
template class CacheHash<CollisionDetector::EdgeRef,CollisionDetector::EdgeRef>;
template class CacheHash<boost::shared_ptr<NarrowNode>,boost::shared_ptr<NarrowNode>>;
PRJ_END
