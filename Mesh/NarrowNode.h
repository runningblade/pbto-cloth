#ifndef NARROW_NODE_H
#define NARROW_NODE_H

#include "Mesh.h"
#include "CommonFile/geom/BVHNode.h"
#include "CommonFile/CollisionDetection.h"

PRJ_BEGIN

struct NarrowNode : public SerializableBase
{
  //typedef BBox<scalar> BBOX;
  typedef KDOP18<scalar> BBOX;
  NarrowNode();
  void buildBVH();
  BBOX refit(bool useLastPos=true);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  bool operator<(const NarrowNode& other) const;
  //data
  std::vector<Node<sizeType,BBOX>> _bvh;
  std::vector<BBOX> _vbb,_ebb;
  std::vector<char> _active;
  boost::shared_ptr<Mesh> _mesh;
  boost::shared_ptr<MeshCache> _cache;
  //info of index
  boost::unordered_map<sizeType,boost::unordered_set<sizeType>> _oneRing,_edgeOfTri;
};
enum MESH_REF_TYPE {
  VERTEX,
  TRIANGLE,
  EDGE,
};
template <int TYPE>
struct MeshRef;
template <>
struct MeshRef<VERTEX> : public SerializableBase
{
  MeshRef();
  MeshRef(boost::shared_ptr<NarrowNode> node,sizeType id);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  Eigen::Map<const Vec3d> posN() const;
  Eigen::Map<const Vec3d> pos() const;
  const NarrowNode::BBOX& BBox() const;
  bool operator!=(const MeshRef& other) const;
  bool operator==(const MeshRef& other) const;
  bool operator<(const MeshRef& other) const;
  boost::shared_ptr<NarrowNode> _node;
  sizeType _id;
};
template <>
struct MeshRef<TRIANGLE> : public SerializableBase
{
  MeshRef();
  MeshRef(boost::shared_ptr<NarrowNode> node,sizeType id);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  Eigen::Map<const Vec3d> posN(sizeType vid) const;
  Eigen::Map<const Vec3d> pos(sizeType vid) const;
  MeshRef<VERTEX> id(sizeType vid) const;
  const NarrowNode::BBOX& BBox() const;
  bool operator!=(const MeshRef& other) const;
  bool operator==(const MeshRef& other) const;
  bool operator<(const MeshRef& other) const;
  boost::shared_ptr<NarrowNode> _node;
  sizeType _id;
};
template <>
struct MeshRef<EDGE> : public SerializableBase
{
  MeshRef();
  MeshRef(boost::shared_ptr<NarrowNode> node,sizeType id);
  virtual bool read(std::istream& is,IOData* dat) override;
  virtual bool write(std::ostream& os,IOData* dat) const override;
  virtual boost::shared_ptr<SerializableBase> copy() const override;
  virtual std::string type() const override;
  Eigen::Map<const Vec3d> posN(sizeType vid) const;
  Eigen::Map<const Vec3d> pos(sizeType vid) const;
  MeshRef<VERTEX> id(sizeType vid) const;
  const NarrowNode::BBOX& BBox() const;
  bool operator!=(const MeshRef& other) const;
  bool operator==(const MeshRef& other) const;
  bool operator<(const MeshRef& other) const;
  boost::shared_ptr<NarrowNode> _node;
  sizeType _id;
};

PRJ_END

#endif
