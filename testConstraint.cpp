#include "Mesh/Mesh.h"
#include "Mesh/Constraint.h"

USE_PRJ_NAMESPACE

int main()
{
  {
    boost::shared_ptr<Mesh> mesh(new Mesh(Mesh::createSheet(Vec2i(10,20),0.1f)));
    SpringConstraint spring(mesh,Vec2d(0.9f,1.1f));
    spring.debugGradient(0.01f);
    BendConstraint bend(mesh,0.1);
    bend.debugGradient(0.01f);
  }
  {
    boost::shared_ptr<Mesh> mesh(new MeshRibbon(0.1,0.05,10));
    RuleRibbonConstraint rule(mesh,0.05f);
    rule.debugGradient(0.01f);
    BendRibbonConstraint bend(mesh,0.05f);
    bend.debugGradient(0.01f);
  }
  return 0;
}
