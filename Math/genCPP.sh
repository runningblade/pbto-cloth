./MaximaToCpp inputR.txt outputR.txt -O Mat3d,R,3,3
./MaximaToCpp inputD.txt outputD.txt -O Mat3d,DRDPhi,3,3 -O Mat3d,DRDTheta,3,3
./MaximaToCpp inputStretch.txt outputStretch.txt -I Vec3d,a,3 -I Vec3d,b,3 -I Vec3d,c,3 -I Mat3d,mbc,3,3 -I Mat3d,mac,3,3 -I Mat3d,mab,3,3
./MaximaToCpp inputStretchE.txt outputStretchE.txt -I Vec3d,a,3 -I Vec3d,b,3 -I Vec3d,c,3 -I Mat3d,mbc,3,3 -I Mat3d,mac,3,3 -I Mat3d,mab,3,3 -O Mat3d,E,3,3
./MaximaToCpp inputBend.txt outputBend.txt -I Vec3d,a,3 -I Vec3d,b,3 -I Vec3d,c,3 -I Vec3d,d,3
./MaximaToCpp inputKinematicRibbon.txt outputKinematicRibbon.txt -O Mat4d,H,4,4 -O Mat4d,DHCoef,4,4
./MaximaToCpp inputCross.txt outputCross.txt -I Vec3d,a,3 -I Vec3d,b,3 -I Vec3d,c,3 -O Vec3d,nn,3,1 -O Mat3X9d,J,3,9
