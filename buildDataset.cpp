#include "Dataset/Dataset.h"
#include "Optimizer/Utils.h"

USE_PRJ_NAMESPACE

int main(int argc,char** argv)
{
  ASSERT_MSG(argc==2,"Usage: buildDataset nrToBuild/Add")
  int nrDataTermToAdd=std::atoi(argv[1]);
  ASSERT_MSG(nrDataTermToAdd>=0,"nrToBuild/Add>=0")
  boost::shared_ptr<Mesh> mesh(new MeshRibbon(0.1f,0.05f,100));
  boost::shared_ptr<Simulator> sim(new Simulator(mesh,Simulator::NRZ_QP));
  sim->initRibbon(1/600.0f,1,Vec3d(0,-9.81f,0),M_PI/4,0.05f);
  boost::shared_ptr<Dataset> ds(new Dataset(sim,4));
  if(exists("Dataset.dat"))
    ds->SerializableBase::read("Dataset.dat");
  ds->buildDataset(nrDataTermToAdd);
  ds->SerializableBase::write("Dataset.dat");
  return 0;
}
